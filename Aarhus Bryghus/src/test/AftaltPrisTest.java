package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.AftaltPris;
import model.Rabat;

public class AftaltPrisTest {

	Rabat aftaltPris;

	@Before
	public void setUp() throws Exception {
		aftaltPris = new AftaltPris();
	}

	@Test
	public void testGivRabat() {
		assertEquals(10, aftaltPris.givRabat(100, 10), 0.001);
		assertEquals(5000, aftaltPris.givRabat(300, 5000), 0.001);
		assertEquals(102.5, aftaltPris.givRabat(300, 102.5), 0.001);
		assertEquals(50, aftaltPris.givRabat(200.2, 50), 0.001);
		assertEquals(100.8, aftaltPris.givRabat(200.3, 100.8), 0.001);
	}
}