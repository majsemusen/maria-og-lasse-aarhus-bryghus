package test;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

import model.Anlæg;
import model.Produkt;
import model.Produktgruppe;

public class ProduktTest {

	// Da produkt er en abstrakt klasse, har jeg valgt at tage Anlæg, der ikke selv
	// har yderligere felter, at teste med
	Produkt anlæg;
	Produkt anlæg2;

	@Before
	public void setUp() throws Exception {
		anlæg = new Anlæg("Navn");
		anlæg2 = new Anlæg("Nyt navn");
	}

	@Test
	public void testProdukt() {
		assertNotNull(anlæg);
		assertEquals("Navn", anlæg.getNavn());
	}

	@Test
	public void testGetProduktgruppe() {
		assertEquals(Produktgruppe.ANLÆG, anlæg.getProduktgruppe());
	}

	@Test
	public void testGetNavn() {
		assertEquals("Navn", anlæg.getNavn());
	}

	@Test
	public void testSetProduktgruppe() {
		anlæg.setProduktgruppe(Produktgruppe.GLAS);
		assertEquals(Produktgruppe.GLAS, anlæg.getProduktgruppe());
	}

	@Test
	public void testSetNavn() {
		anlæg.setNavn("Nyt navn");
		assertEquals("Nyt navn", anlæg.getNavn());
	}

	@Test
	public void testCompareTo() {
		assertEquals(0, anlæg.compareTo(anlæg));
		assertEquals(anlæg, anlæg);
		assertNotEquals(0, anlæg.compareTo(anlæg2));
		assertNotEquals(anlæg2, anlæg);
	}
}