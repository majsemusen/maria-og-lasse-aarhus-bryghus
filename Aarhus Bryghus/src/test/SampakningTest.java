package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.Produktgruppe;
import model.Sampakning;

public class SampakningTest {

	Sampakning sampak;

	@Before
	public void setUp() throws Exception {
		sampak = new Sampakning("Gaveæske", 3, 2);
	}

	@Test
	public void testSampakning() {
		assertNotNull(sampak);
	}

	@Test
	public void testGetProduktgruppe() {
		assertEquals(Produktgruppe.SAMPAKNING, sampak.getProduktgruppe());
	}

	@Test
	public void testGetBryg() {
		assertEquals(3, sampak.getBryg());
	}

	@Test
	public void testGetGlas() {
		assertEquals(2, sampak.getGlas());
	}

	@Test
	public void testSetBryg() {
		sampak.setBryg(6);
		assertEquals(6, sampak.getBryg());
	}

	@Test
	public void testSetGlas() {
		sampak.setGlas(1);
		assertEquals(1, sampak.getGlas());
	}
}