package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.Malt;
import model.Produktgruppe;

public class MaltTest {

	Malt malt;

	@Before
	public void setUp() throws Exception {
		malt = new Malt("Malt", 20);
	}

	@Test
	public void testMalt() {
		assertNotNull(malt);
	}

	@Test
	public void testGetProduktgruppe() {
		assertEquals(Produktgruppe.MALT, malt.getProduktgruppe());
	}

	@Test
	public void testGetMængde() {
		assertEquals(20, malt.getMængde());
	}

	@Test
	public void testSetMængde() {
		malt.setMængde(35);
		assertEquals(35, malt.getMængde());
	}
}