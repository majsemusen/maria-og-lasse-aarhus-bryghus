package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.IngenRabat;
import model.Rabat;

public class IngenRabatTest {

	Rabat ingenRabat;

	@Before
	public void setUp() throws Exception {
		ingenRabat = new IngenRabat();
	}

	@Test
	public void testGivRabat() {
		assertEquals(100, ingenRabat.givRabat(100, 10), 0.001);
		assertEquals(300, ingenRabat.givRabat(300, 5000), 0.001);
		assertEquals(300, ingenRabat.givRabat(300, 102.5), 0.001);
		assertEquals(200.2, ingenRabat.givRabat(200.2, 50), 0.001);
		assertEquals(200.3, ingenRabat.givRabat(200.3, 100.8), 0.001);
	}
}