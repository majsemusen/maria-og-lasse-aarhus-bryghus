package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.Bryg;
import model.Produktgruppe;

public class BrygTest {

	Bryg fadøl;
	Bryg flaske;
	Bryg fustage;
	Bryg spiritus;

	@Before
	public void setUp() throws Exception {
		fadøl = new Bryg(Produktgruppe.FADØL, "Forårsbryg", 40, "cl", 7.0);
		flaske = new Bryg(Produktgruppe.FLASKE, "Forårsbryg", 60, "cl", 7.0);
		fustage = new Bryg(Produktgruppe.FUSTAGE, "Forårsbryg", 20, "l", 7.0);
		spiritus = new Bryg(Produktgruppe.SPIRITUS, "Whisky", 50, "cl", 43.0);
	}

	@Test
	public void testBryg() {
		assertNotNull(fadøl);
		assertNotNull(flaske);
		assertNotNull(fustage);
		assertNotNull(spiritus);
	}

	@Test
	public void testGetProduktgruppe() {
		assertEquals(Produktgruppe.FADØL, fadøl.getProduktgruppe());
		assertEquals(Produktgruppe.FLASKE, flaske.getProduktgruppe());
		assertEquals(Produktgruppe.FUSTAGE, fustage.getProduktgruppe());
		assertEquals(Produktgruppe.SPIRITUS, spiritus.getProduktgruppe());
	}

	@Test
	public void testGetMængde() {
		assertEquals(40, fadøl.getMængde());
	}

	@Test
	public void testGetEnhed() {
		assertEquals("cl", fadøl.getEnhed());
	}

	@Test
	public void testGetProcent() {
		assertEquals(7.0, fadøl.getProcent(), 0.001);
	}

	@Test
	public void testSetMængde() {
		fadøl.setMængde(53);
		assertEquals(53, fadøl.getMængde());
	}

	@Test
	public void testSetEnhed() {
		fadøl.setEnhed("l");
		assertEquals("l", fadøl.getEnhed());
	}

	@Test
	public void testSetProcent() {
		fadøl.setProcent(10.3);
		assertEquals(10.3, fadøl.getProcent(), 0.001);
	}
}