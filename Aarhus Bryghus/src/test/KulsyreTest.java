package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.Kulsyre;
import model.Produktgruppe;

public class KulsyreTest {

	Kulsyre kulsyre;

	@Before
	public void setUp() throws Exception {
		kulsyre = new Kulsyre("Kulsyre", 50);
	}

	@Test
	public void testKulsyre() {
		assertNotNull(kulsyre);
	}

	@Test
	public void testSetProduktgruppe() {
		assertEquals(Produktgruppe.KULSYRE, kulsyre.getProduktgruppe());
	}

	@Test
	public void testGetMængde() {
		assertEquals(50, kulsyre.getMængde());
	}

	@Test
	public void testSetMængde() {
		kulsyre.setMængde(100);
		assertEquals(100, kulsyre.getMængde());
	}
}