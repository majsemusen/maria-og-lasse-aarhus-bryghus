package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.Produktgruppe;
import model.Rundvisning;

public class RundvisningTest {

	Rundvisning rundvisning;

	@Before
	public void setUp() throws Exception {
//		rundvisning = new Rundvisning("Rundvisning", 15,
//				LocalDateTime.of(LocalDate.of(2020, 4, 1), LocalTime.of(15, 30)));
		rundvisning = new Rundvisning("Rundvisning");
	}

	@Test
	public void testRundvisning() {
		assertNotNull(rundvisning);
	}

	@Test
	public void testGetProduktgruppe() {
		assertEquals(Produktgruppe.RUNDVISNING, rundvisning.getProduktgruppe());
	}

//	@Test
//	public void testGetAntal() {
//		assertEquals(15, rundvisning.getAntal());
//	}

//	@Test
//	public void testGetTidspunkt() {
//		assertEquals(LocalDateTime.of(LocalDate.of(2020, 4, 1), LocalTime.of(15, 30)), rundvisning.getTidspunkt());
//	}
}