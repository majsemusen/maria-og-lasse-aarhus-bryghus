package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.Klippekort;
import model.Produktgruppe;

public class KlippekortTest {

	Klippekort klippekort;

	@Before
	public void setUp() throws Exception {
		klippekort = new Klippekort("Klippekort", 4);
	}

	@Test
	public void testKlippekort() {
		assertNotNull(klippekort);
	}

	@Test
	public void testGetProduktgruppe() {
		assertEquals(Produktgruppe.KLIPPEKORT, klippekort.getProduktgruppe());
	}

	@Test
	public void testGetKlip() {
		assertEquals(4, klippekort.getKlip());
	}
}