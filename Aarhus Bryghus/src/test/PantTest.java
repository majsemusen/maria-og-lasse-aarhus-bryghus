package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.Pant;
import model.Produktgruppe;

public class PantTest {

	Pant pant;

	@Before
	public void setUp() throws Exception {
		pant = new Pant("Pant");
	}

	@Test
	public void testPant() {
		assertNotNull(pant);
	}

	@Test
	public void testGetProduktgruppe() {
		assertEquals(Produktgruppe.PANT, pant.getProduktgruppe());
	}
}