package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.Levering;
import model.Produktgruppe;

public class LeveringTest {

	Levering levering;

	@Before
	public void setUp() throws Exception {
		levering = new Levering("Levering", 50);
	}

	@Test
	public void testLevering() {
		assertNotNull(levering);
	}

	@Test
	public void testGetProduktgruppe() {
		assertEquals(Produktgruppe.LEVERING, levering.getProduktgruppe());
	}

	@Test
	public void testGetAfstand() {
		assertEquals(50, levering.getAfstand(), 0.001);
	}

	@Test
	public void testSetAfstand() {
		levering.setAfstand(101.3);
		assertEquals(101.3, levering.getAfstand(), 0.001);
	}
}