package test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDate;
import java.util.TreeMap;

import org.junit.Before;
import org.junit.Test;

import model.AftaltPris;
import model.Anlæg;
import model.Betalingsmetode;
import model.Bryg;
import model.Glas;
import model.IngenRabat;
import model.Kunde;
import model.Listepris;
import model.Ordre;
import model.Ordrelinje;
import model.OrdrelinjeSampak;
import model.Prisliste;
import model.ProcentRabat;
import model.Produktgruppe;
import model.Rabat;
import model.Sampakning;

public class OrdreTest {

	Ordre ordre1;
	Ordre ordre2IK;
	Rabat rabatIngen;
	Kunde kunde1;
	TreeMap<Betalingsmetode, Double> betalingsmetoder;
	Betalingsmetode btmt;
	Anlæg anlæg;
	Prisliste prisliste;
	Listepris listepris;

	@Before
	public void setUp() throws Exception {
		rabatIngen = new IngenRabat();
		kunde1 = new Kunde("Ale Øllegård", "12121212", "Ipavej 12, 2812 Kongens Ølby",
				"Æblebrus Allé 12, 2812 Kongens Ølby");
//		ordre1 = new Ordre(kunde1, LocalDate.of(2020, 04, 8), rabatIngen, 0, true);
//		ordre2IK = new Ordre(LocalDate.of(2020, 04, 8), rabatIngen, 0, true);
		// Nedenstående opdateret i iteration 5, ovenstående er fra iteration 4
		ordre1 = new Ordre(kunde1, LocalDate.of(2020, 04, 8), rabatIngen, 0);
		ordre2IK = new Ordre(LocalDate.of(2020, 04, 8), rabatIngen, 0);
		betalingsmetoder = new TreeMap<Betalingsmetode, Double>();
		btmt = new Betalingsmetode("Dankort");
		prisliste = new Prisliste("Prisliste", false);
		anlæg = new Anlæg("Anlæg");
		listepris = new Listepris(anlæg, 200);
	}

	@Test
	public void testOrdre() {
//		Ordre ordre2Ugyldig = new Ordre(kunde1, null, null, 0, true);
		// Nedenstående opdateret i iteration 5, ovenstående er fra iteration 4
		Ordre ordre2Ugyldig = new Ordre(kunde1, null, null, 0);
		Kunde kunde2 = new Kunde("Ale Øllegård", "12121212", "Ipavej 12, 2812 Kongens Ølby",
				"Æblebrus Allé 12, 2812 Kongens Ølby");
		Rabat rabat2 = new IngenRabat();
		assertNotNull(ordre1);
		assertEquals(kunde2.getNavn(), kunde1.getNavn()); // tester om kundeobjekternes navne er ens
		assertEquals(LocalDate.of(2020, 04, 8), ordre1.getDatoStart());
		assertEquals(rabat2.getClass(), ordre1.getRabat().getClass());
		assertEquals(0, ordre1.getRabatMængde(), 0.01);
		assertEquals(true, ordre1.isBetalt());

		assertEquals(kunde1.getNavn(), kunde2.getNavn());
		assertEquals(null, ordre2Ugyldig.getDatoSlut());
		assertEquals(null, ordre2Ugyldig.getRabat());
		assertEquals(0, ordre2Ugyldig.getRabatMængde(), 0.01);
		assertEquals(true, ordre2Ugyldig.isBetalt());
	}

	@Test
	public void testOrdreIngenKunde() {
//		Ordre ordre4Ugyldig = new Ordre(null, null, 0, true);
		// Nedenstående opdateret i iteration 5, ovenstående er fra iteration 4
		Ordre ordre4Ugyldig = new Ordre(null, null, 0);
		Rabat rabat2 = new IngenRabat();
		assertNotNull(ordre2IK);
		assertEquals(LocalDate.of(2020, 04, 8), ordre2IK.getDatoStart());
		assertEquals(rabat2.getClass(), ordre2IK.getRabat().getClass());
		assertEquals(0, ordre2IK.getRabatMængde(), 0.01);
		assertEquals(true, ordre2IK.isBetalt());

		assertEquals(null, ordre4Ugyldig.getDatoSlut());
		assertEquals(null, ordre4Ugyldig.getRabat());
		assertEquals(0, ordre4Ugyldig.getRabatMængde(), 0.01);
		assertEquals(true, ordre4Ugyldig.isBetalt());

	}

	@Test
	public void testSetDatoSlut() {
		assertNotEquals(LocalDate.of(2019, 06, 12), ordre1.getDatoSlut());
		ordre1.setDatoSlut((LocalDate.of(2020, 04, 8)));
		assertEquals(LocalDate.of(2020, 04, 8), ordre1.getDatoSlut());
		ordre1.setDatoSlut(LocalDate.of(2020, 04, 12));
		assertEquals(LocalDate.of(2020, 04, 12), ordre1.getDatoSlut());
		ordre1.setDatoSlut(LocalDate.of(2020, 04, 4));
		assertEquals(LocalDate.of(2020, 04, 4), ordre1.getDatoSlut());
	}

	@Test
	public void testSetRabat() {
		Rabat rabat2 = new IngenRabat();
		Rabat rabat3 = new ProcentRabat();
		Rabat rabat4 = new AftaltPris();
		Rabat rabatNull = null;
		assertEquals(rabat2.getClass(), ordre1.getRabat().getClass());
		ordre1.setRabat(rabat3);
		assertNotEquals(rabat2.getClass(), ordre1.getRabat().getClass());

		assertEquals(rabat3.getClass(), ordre1.getRabat().getClass());
		ordre1.setRabat(rabat4);
		assertEquals(rabat4.getClass(), ordre1.getRabat().getClass());
		ordre1.setRabat(rabatNull);
		assertEquals(null, ordre1.getRabat());
	}

	@Test
	public void testSetRabatMængde() {
		ordre1.setRabatMængde(50);
		assertEquals(50, ordre1.getRabatMængde(), 0.01);
		ordre1.setRabatMængde(-10);
		assertEquals(-10, ordre1.getRabatMængde(), 0.01);
	}

	@Test
	public void testSetBetalingsmetoder() {
		assertEquals(0, ordre1.getBetalingsmetoder().size());
		TreeMap<Betalingsmetode, Double> betalingsmetoderNyTom = new TreeMap<Betalingsmetode, Double>();
		TreeMap<Betalingsmetode, Double> betalingsmetoderNy = new TreeMap<Betalingsmetode, Double>();
		betalingsmetoderNy.put(btmt, (double) 500);
		ordre1.setBetalingsmetoder(betalingsmetoderNy);
		assertNotEquals(0, ordre1.getBetalingsmetoder().size());
		assertEquals(Double.valueOf(500), ordre1.getBetalingsmetoder().get(btmt));
		ordre1.setBetalingsmetoder(betalingsmetoderNyTom);
		assertEquals(0, ordre1.getBetalingsmetoder().size());
	}

	@Test
	public void testAddBetalingsmetode() {
		assertEquals(0, ordre1.getBetalingsmetoder().size());
		ordre1.addBetalingsmetode(btmt, (double) 500);
		assertEquals(1, ordre1.getBetalingsmetoder().size());
		assertEquals(Double.valueOf(500), ordre1.getBetalingsmetoder().get(btmt));
		assertEquals("Dankort", ordre1.getBetalingsmetoder().firstKey().getType());

		ordre1.addBetalingsmetode(btmt, -300);
		assertEquals(1, ordre1.getBetalingsmetoder().size());
		assertEquals(Double.valueOf(-300), ordre1.getBetalingsmetoder().get(btmt));
		assertEquals("Dankort", ordre1.getBetalingsmetoder().firstKey().getType());
		assertNotEquals(Double.valueOf(500), ordre1.getBetalingsmetoder().get(btmt));
	}

	@Test
	public void testCreateOrdrelinje() {
		assertEquals(0, ordre1.getOrdrelinjer().size());
//		ordre1.createOrdrelinje(listepris, 1);
		// Nedenstående opdateret i iteration 5, ovenstående er fra iteration 4
		ordre1.createOrdrelinje(listepris, 1, true);
		assertEquals(1, ordre1.getOrdrelinjer().size());
		assertEquals(Double.valueOf(200), Double.valueOf(ordre1.getOrdrelinjer().get(0).getListepris().getPris()));
		assertEquals(Integer.valueOf(1), Integer.valueOf(ordre1.getOrdrelinjer().get(0).getAntal()));

//		ordre1.createOrdrelinje(listepris, -1);
		// Nedenstående opdateret i iteration 5, ovenstående er fra iteration 4
		ordre1.createOrdrelinje(listepris, -1, true);
		assertEquals(2, ordre1.getOrdrelinjer().size());
		assertEquals(Integer.valueOf(-1), Integer.valueOf(ordre1.getOrdrelinjer().get(1).getAntal()));
	}

	@Test
	public void testRemoveOrdrelinje() {
		assertEquals(0, ordre1.getOrdrelinjer().size());
//		Ordrelinje ol = ordre1.createOrdrelinje(listepris, 1);
		// Nedenstående opdateret i iteration 5, ovenstående er fra iteration 4
		Ordrelinje ol = ordre1.createOrdrelinje(listepris, 1, true);
		assertEquals(1, ordre1.getOrdrelinjer().size());
		ordre1.removeOrdrelinje(ol);
		assertEquals(0, ordre1.getOrdrelinjer().size());
	}

	@Test
	public void testCreateOrdrelinjeSampak() {
		Sampakning sp = new Sampakning("1 Glas, 1 Øl", 1, 1);
		Listepris lp = new Listepris(sp, 200);
		Bryg øl = new Bryg(Produktgruppe.FLASKE, "Julebryg", 60, "cl", 8.0);
		Glas glas = new Glas("Glas", 40, "cl");
//		OrdrelinjeSampak ol = (OrdrelinjeSampak) ordre1.createOrdrelinjeSampak(lp, 1);
		// Nedenstående opdateret i iteration 5, ovenstående er fra iteration 4
		OrdrelinjeSampak ol = (OrdrelinjeSampak) ordre1.createOrdrelinjeSampak(lp, 1, true);
		Bryg[] brygArray = new Bryg[sp.getBryg()];
		brygArray[0] = øl;
		Glas[] glasArray = new Glas[sp.getGlas()];
		glasArray[0] = glas;
		ol.setBrygArray(brygArray);
		ol.setGlasArray(glasArray);

		assertEquals(1, ordre1.getOrdrelinjer().size());
		assertEquals(Double.valueOf(200), Double.valueOf(ordre1.getOrdrelinjer().get(0).getListepris().getPris()));
		assertEquals(Integer.valueOf(1), Integer.valueOf(ordre1.getOrdrelinjer().get(0).getAntal()));
		assertArrayEquals(brygArray, ol.getBrygArray());
		assertArrayEquals(glasArray, ol.getGlasArray());

//		ordre1.createOrdrelinje(listepris, -1);
		// Nedenstående opdateret i iteration 5, ovenstående er fra iteration 4
		ordre1.createOrdrelinje(listepris, -1, true);
		assertEquals(2, ordre1.getOrdrelinjer().size());
		assertEquals(Integer.valueOf(-1), Integer.valueOf(ordre1.getOrdrelinjer().get(1).getAntal()));
	}
}