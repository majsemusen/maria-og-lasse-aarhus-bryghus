package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.Glas;
import model.Produktgruppe;

public class GlasTest {

	Glas glas;

	@Before
	public void setUp() throws Exception {
		glas = new Glas("Glas", 50, "cl");
	}

	@Test
	public void testGlas() {
		assertNotNull(glas);
	}

	@Test
	public void testGetProduktgruppe() {
		assertEquals(Produktgruppe.GLAS, glas.getProduktgruppe());
	}

	@Test
	public void testGetStørrelse() {
		assertEquals(50, glas.getStørrelse());
	}

	@Test
	public void testGetEnhed() {
		assertEquals("cl", glas.getEnhed());
	}

	@Test
	public void testSetStørrelse() {
		glas.setStørrelse(103);
		assertEquals(103, glas.getStørrelse());
	}

	@Test
	public void testSetEnhed() {
		glas.setEnhed("ml");
		assertEquals("ml", glas.getEnhed());
	}
}