package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.Anlæg;
import model.Produktgruppe;

public class AnlægTest {

	Anlæg anlæg;

	@Before
	public void setUp() throws Exception {
		anlæg = new Anlæg("Anlæg");
	}

	@Test
	public void testAnlæg() {
		assertNotNull(anlæg);
		assertEquals("Anlæg", anlæg.getNavn());
	}

	@Test
	public void testGetProduktgruppe() {
		assertEquals(Produktgruppe.ANLÆG, anlæg.getProduktgruppe());
	}
}