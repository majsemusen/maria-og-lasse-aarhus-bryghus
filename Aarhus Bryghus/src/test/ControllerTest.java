package test;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import controller.Controller;
import model.Anlæg;
import model.Listepris;
import model.Ordre;
import model.Prisliste;
import model.Rundvisning;

public class ControllerTest {

	Listepris listepris;

	@Before
	public void setUp() throws Exception {
		Prisliste prisliste = new Prisliste("Testprisliste", false);

		Anlæg anlæg = new Anlæg("Anlæg");
		listepris = prisliste.setListepris(anlæg, 30);
	}

	@Test
	public void testPrisIKlip() {
		// TC22
		listepris.setPris(30);
		assertEquals(1, Controller.prisIKlip(listepris));

		// TC23
		listepris.setPris(32.50);
		assertEquals(1, Controller.prisIKlip(listepris));

		// TC24
		listepris.setPris(35);
		assertEquals(1, Controller.prisIKlip(listepris));

		// TC25
		listepris.setPris(39);
		assertEquals(1, Controller.prisIKlip(listepris));

		// TC26
		listepris.setPris(40);
		assertEquals(2, Controller.prisIKlip(listepris));

		// TC27
		listepris.setPris(80);
		assertEquals(3, Controller.prisIKlip(listepris));

		// TC28
		listepris.setPris(0);
		assertEquals(0, Controller.prisIKlip(listepris));

		// TC29
		listepris.setPris(-1);
		assertEquals(0, Controller.prisIKlip(listepris));
	}

	@Test
	public void testResterendePrisEfterKlip() {
		// TC30
		listepris.setPris(30);
		assertEquals(30, Controller.resterendePrisEfterKlip(listepris, 1, 0), 0.001);

		// TC31
		assertEquals(0, Controller.resterendePrisEfterKlip(listepris, 1, 1), 0.001);

		// TC32
		listepris.setPris(39);
		assertEquals(0, Controller.resterendePrisEfterKlip(listepris, 1, 1), 0.001);

		// TC33
		listepris.setPris(40);
		assertEquals(7.50, Controller.resterendePrisEfterKlip(listepris, 1, 1), 0.001);

		// TC34
		assertEquals(0, Controller.resterendePrisEfterKlip(listepris, 1, 2), 0.001);

		// TC35
		listepris.setPris(80);
		assertEquals(15, Controller.resterendePrisEfterKlip(listepris, 1, 2), 0.001);

		// TC36
		listepris.setPris(39);
		assertEquals(39, Controller.resterendePrisEfterKlip(listepris, 2, 1), 0.001);

		// TC37
		listepris.setPris(40);
		assertEquals(47.50, Controller.resterendePrisEfterKlip(listepris, 3, 3), 0.001);

		// TC38
		listepris.setPris(-1);
		assertEquals(0, Controller.resterendePrisEfterKlip(listepris, 1, 2), 0.001);
	}

	@Test
	public void testErTidspunktLedigt() {
		// Oprettelsen af disse objekter findes i denne metode, da kaldet af
		// Controller.init() ellers giver fejl.

		Controller.init();

		Prisliste prisliste = new Prisliste("Testprisliste", false);
		Rundvisning rundvisning = new Rundvisning("Test");
		Listepris listeprisRundvisning = prisliste.setListepris(rundvisning, 30);
		Ordre ordre = Controller.createOrdre(LocalDate.now(), Controller.getRabatter().get(0), 0);
		Controller.createOrdrelinjeRundvisning(ordre, listeprisRundvisning, 15, false, "Testrundvisning",
				LocalDateTime.of(LocalDate.of(2021, 3, 8), LocalTime.of(8, 00)));

		// TC 39
		assertTrue(Controller.erTidspunktLedigt(LocalDate.of(2021, 3, 1), LocalTime.of(8, 00)));

		// TC 40
		assertTrue(Controller.erTidspunktLedigt(LocalDate.of(2021, 3, 1), LocalTime.of(15, 00)));

		// TC 41
		assertTrue(Controller.erTidspunktLedigt(LocalDate.of(2021, 3, 1), LocalTime.of(16, 00)));

		// TC 42
		assertTrue(Controller.erTidspunktLedigt(LocalDate.of(2021, 3, 5), LocalTime.of(13, 00)));

		// TC 43
		assertFalse(Controller.erTidspunktLedigt(LocalDate.of(2021, 3, 5), LocalTime.of(14, 00)));

		// TC 44
		assertFalse(Controller.erTidspunktLedigt(LocalDate.of(2021, 3, 6), LocalTime.of(8, 00)));

		// TC 45
		assertFalse(Controller.erTidspunktLedigt(LocalDate.of(2021, 3, 8), LocalTime.of(9, 00)));
	}
}