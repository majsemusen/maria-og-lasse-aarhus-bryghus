package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.Diverse;
import model.Produktgruppe;

public class DiverseTest {

	Diverse snacks;
	Diverse vand;

	@Before
	public void setUp() throws Exception {
		snacks = new Diverse(Produktgruppe.SNACKS, "Snacks", 50, "g");
		vand = new Diverse(Produktgruppe.VAND, "Vand", 100, "ml");
	}

	@Test
	public void testDiverse() {
		assertNotNull(snacks);
		assertNotNull(vand);
	}

	@Test
	public void testGetProduktgruppe() {
		assertEquals(Produktgruppe.SNACKS, snacks.getProduktgruppe());
		assertEquals(Produktgruppe.VAND, vand.getProduktgruppe());
	}

	@Test
	public void testGetMængde() {
		assertEquals(50, snacks.getMængde());
	}

	@Test
	public void testSetMængde() {
		vand.setMængde(100);
		assertEquals(100, vand.getMængde());
	}

	@Test
	public void testGetEnhed() {
		assertEquals("g", snacks.getEnhed());
	}

	@Test
	public void testSetEnhed() {
		vand.setEnhed("cl");
		assertEquals("cl", vand.getEnhed());
	}
}