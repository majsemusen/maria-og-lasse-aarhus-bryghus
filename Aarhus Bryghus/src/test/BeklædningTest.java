package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.Beklædning;
import model.Produktgruppe;

public class BeklædningTest {

	Beklædning tøj;

	@Before
	public void setUp() throws Exception {
		tøj = new Beklædning("Tøj", "Medium", "Lyserød", "Silke");
	}

	@Test
	public void testBeklædning() {
		assertNotNull(tøj);
	}

	@Test
	public void testGetProduktgruppe() {

		assertEquals(Produktgruppe.BEKLÆDNING, tøj.getProduktgruppe());
	}

	@Test
	public void testGetStørrelse() {
		assertEquals("Medium", tøj.getStørrelse());
	}

	@Test
	public void testGetFarve() {
		assertEquals("Lyserød", tøj.getFarve());
	}

	@Test
	public void testGetMateriale() {
		assertEquals("Silke", tøj.getMateriale());
	}

	@Test
	public void testSetStørrelse() {
		tøj.setStørrelse("XS");
		assertEquals("XS", tøj.getStørrelse());
	}

	@Test
	public void testSetFarve() {
		tøj.setFarve("Hvid");
		assertEquals("Hvid", tøj.getFarve());
	}

	@Test
	public void testSetMateriale() {
		tøj.setMateriale("Bomuld");
		assertEquals("Bomuld", tøj.getMateriale());
	}
}