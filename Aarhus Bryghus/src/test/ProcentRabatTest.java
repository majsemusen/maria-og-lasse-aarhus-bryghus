package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.ProcentRabat;
import model.Rabat;

public class ProcentRabatTest {

	Rabat procentRabat;

	@Before
	public void setUp() throws Exception {
		procentRabat = new ProcentRabat();
	}

	@Test
	public void testGivRabat() {
		assertEquals(90, procentRabat.givRabat(100, 10), 0.001);
		assertEquals(232.50, procentRabat.givRabat(300, 22.5), 0.001);
		assertEquals(100.1, procentRabat.givRabat(200.2, 50), 0.001);
		assertEquals(20.43, procentRabat.givRabat(200.3, 89.8), 0.001);
		// Går imod præbetingelsen
		assertEquals(-14700, procentRabat.givRabat(300, 5000), 0.001);
	}
}