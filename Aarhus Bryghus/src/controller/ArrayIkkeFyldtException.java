package controller;

public class ArrayIkkeFyldtException extends RuntimeException {

	public ArrayIkkeFyldtException(String message) {
		super(message);
	}

	public ArrayIkkeFyldtException(String message, Throwable cause) {
		super(message, cause);
	}
}