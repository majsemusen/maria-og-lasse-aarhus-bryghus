package controller;

public class SalgssituationTagetException extends RuntimeException {

	public SalgssituationTagetException(String message) {
		super(message);
	}

	public SalgssituationTagetException(String message, Throwable cause) {
		super(message, cause);
	}
}