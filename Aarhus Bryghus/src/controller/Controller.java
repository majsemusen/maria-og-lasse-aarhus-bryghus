package controller;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.Map.Entry;

import model.AftaltPris;
import model.Anlæg;
import model.Beklædning;
import model.Betalingsmetode;
import model.Bruger;
import model.Bryg;
import model.Diverse;
import model.Glas;
import model.IngenRabat;
import model.Klippekort;
import model.Kulsyre;
import model.Kunde;
import model.Levering;
import model.Listepris;
import model.Malt;
import model.Ordre;
import model.Ordrelinje;
import model.OrdrelinjeRundvisning;
import model.OrdrelinjeSampak;
import model.Pant;
import model.Prisliste;
import model.ProcentRabat;
import model.Produkt;
import model.Produktgruppe;
import model.Rabat;
import model.Rundvisning;
import model.Sampakning;
import storage.Storage;

public class Controller {

	private static Storage storage;

	// -------------------------------------------------------------------------

	/**
	 * Gemmer storage. Kaster exception, hvis filen ikke kan findes og ved IO fejl.
	 */
	public static void saveStorage() {
		try (FileOutputStream fileOut = new FileOutputStream("storage.ser");
				ObjectOutputStream out = new ObjectOutputStream(fileOut)) {
			out.writeObject(storage);
			System.out.println("Storage saved in file storage.ser.");
		} catch (FileNotFoundException ex) {
			System.out.println(ex.getMessage());
			System.out.println("File not found. Error saving storage object.");
		} catch (IOException ex) {
			System.out.println(ex.getMessage());
			System.out.println("IOException. Error saving storage object.");
		}
	}

	/**
	 * Henter gemt storage. Kaster exception, hvis klassen ikke kan findes og ved IO
	 * fejl.
	 */
	public static void loadStorage() {
		try (FileInputStream fileIn = new FileInputStream("storage.ser");
				ObjectInputStream in = new ObjectInputStream(fileIn)) {
			storage = (Storage) in.readObject();
			System.out.println("Storage loaded from file storage.ser.");
		} catch (ClassNotFoundException ex) {
			System.out.println("Class not found. Error loading storage object.");
			throw new RuntimeException(ex);
		} catch (IOException ex) {
			System.out.println("IO Exception. Error loading storage object.");
			throw new RuntimeException(ex);
		}
	}

	// -------------------------------------------------------------------------

	public static ArrayList<Produkt> getProdukter() {
		return storage.getProdukter();
	}

	/**
	 * Henter alle produkter i storage fra den specificerede produktgruppe.
	 */
	public static ArrayList<Produkt> getProdukterFraProduktgruppe(Produktgruppe produktgruppe) {
		ArrayList<Produkt> produkter = new ArrayList<>();
		for (Produkt produkt : storage.getProdukter()) {
			if (produkt.getProduktgruppe().equals(produktgruppe)) {
				produkter.add(produkt);
			}
		}
		return produkter;
	}

	public static void deleteProdukt(Produkt produkt) {
		storage.removeProdukt(produkt);
	}

	public static Anlæg createAnlæg(String navn) {
		Anlæg anlæg = new Anlæg(navn);
		storage.addProdukt(anlæg);
		return anlæg;
	}

	public static void updateAnlæg(Anlæg anlæg, String navn) {
		anlæg.setNavn(navn);
	}

	public static Beklædning createBeklædning(String navn, String størrelse, String farve, String materiale) {
		Beklædning beklædning = new Beklædning(navn, størrelse, farve, materiale);
		storage.addProdukt(beklædning);
		return beklædning;
	}

	public static void updateBeklædning(Beklædning bk, String navn, String størrelse, String farve, String materiale) {
		bk.setNavn(navn);
		bk.setStørrelse(størrelse);
		bk.setFarve(farve);
		bk.setMateriale(materiale);
	}

	public static Bryg createBryg(Produktgruppe produktgruppe, String navn, int mængde, String enhed, double procent) {
		Bryg bryg = new Bryg(produktgruppe, navn, mængde, enhed, procent);
		storage.addProdukt(bryg);
		return bryg;
	}

	public static void updateBryg(Bryg bryg, String navn, int mængde, String enhed, double procent) {
		bryg.setNavn(navn);
		bryg.setMængde(mængde);
		bryg.setEnhed(enhed);
		bryg.setProcent(procent);
	}

	public static Diverse createDiverse(Produktgruppe produktgruppe, String navn, int mængde, String enhed) {
		Diverse diverse = new Diverse(produktgruppe, navn, mængde, enhed);
		storage.addProdukt(diverse);
		return diverse;
	}

	public static void updateDiverse(Produktgruppe produktgruppe, Diverse diverse, String navn, int mængde,
			String enhed) {
		diverse.setNavn(navn);
		diverse.setMængde(mængde);
		diverse.setEnhed(enhed);
	}

	public static Glas createGlas(String navn, int størrelse, String enhed) {
		Glas glas = new Glas(navn, størrelse, enhed);
		storage.addProdukt(glas);
		return glas;
	}

	public static void updateGlas(Glas glas, String navn, int størrelse, String enhed) {
		glas.setNavn(navn);
		glas.setStørrelse(størrelse);
		glas.setEnhed(enhed);
	}

	public static Klippekort createKlippekort(String navn, int klip) {
		Klippekort klippekort = new Klippekort(navn, klip);
		storage.addProdukt(klippekort);
		return klippekort;
	}

	public static void updateKlippekort(Klippekort klippekort, String navn, int klip) {
		klippekort.setNavn(navn);
		klippekort.setKlip(klip);
	}

	public static Kulsyre createKulsyre(String navn, int mængde) {
		Kulsyre kulsyre = new Kulsyre(navn, mængde);
		storage.addProdukt(kulsyre);
		return kulsyre;
	}

	public static void updateKulsyre(Kulsyre ks, String navn, int mængde) {
		ks.setNavn(navn);
		ks.setMængde(mængde);
	}

	public static Levering createLevering(String navn, double afstand) {
		Levering levering = new Levering(navn, afstand);
		storage.addProdukt(levering);
		return levering;
	}

	public static void updateLevering(Levering lv, String navn, double afstand) {
		lv.setNavn(navn);
		lv.setAfstand(afstand);
	}

	public static Malt createMalt(String navn, int mængde) {
		Malt malt = new Malt(navn, mængde);
		storage.addProdukt(malt);
		return malt;
	}

	public static void updateMalt(Malt malt, String navn, int mængde) {
		malt.setNavn(navn);
		malt.setMængde(mængde);
	}

	public static Pant createPant(String navn) {
		Pant pant = new Pant(navn);
		storage.addProdukt(pant);
		return pant;
	}

	public static void updatePant(Pant pant, String navn) {
		pant.setNavn(navn);
	}

	public static Rundvisning createRundvisning(String navn) {
		Rundvisning rundvisning = new Rundvisning(navn);
		storage.addProdukt(rundvisning);
		return rundvisning;
	}

	public static void updateRundvisning(Rundvisning rundvisning, String navn) {
		rundvisning.setNavn(navn);
	}

	public static Sampakning createSampakning(String navn, int bryg, int glas) {
		Sampakning sampakning = new Sampakning(navn, bryg, glas);
		storage.addProdukt(sampakning);
		return sampakning;
	}

	public static void updateSampakning(Sampakning sp, String navn, int bryg, int glas) {
		sp.setNavn(navn);
		sp.setBryg(bryg);
		sp.setGlas(glas);
	}

	// -------------------------------------------------------------------------

	public static ArrayList<Betalingsmetode> getBetalingsmetoder() {
		return storage.getBetalingsmetoder();
	}

	public static Betalingsmetode createBetalingsmetode(String type) {
		Betalingsmetode bt = new Betalingsmetode(type);
		storage.addBetalingsmetode(bt);
		return bt;
	}

	// -------------------------------------------------------------------------

	public static ArrayList<Ordre> getOrdrer() {
		return storage.getOrdrer();
	}

	/**
	 * Opretter en ordre med en kunde tilknyttet.
	 */
	public static Ordre createOrdre(Kunde kunde, LocalDate datoStart, Rabat rabat, double rabatMængde) {
		Ordre ordre = new Ordre(kunde, datoStart, rabat, rabatMængde);
		storage.addOrdre(ordre);
		return ordre;
	}

	/**
	 * Opretter en ordre uden en kunde tilknyttet.
	 */
	public static Ordre createOrdre(LocalDate datoStart, Rabat rabat, double rabatMængde) {
		Ordre ordre = new Ordre(datoStart, rabat, rabatMængde);
		storage.addOrdre(ordre);
		return ordre;
	}

	public static void deleteOrdre(Ordre ordre) {
		for (Ordrelinje ordrelinje : ordre.getOrdrelinjer()) {
			ordre.removeOrdrelinje(ordrelinje);
		}
		storage.removeOrdre(ordre);
	}

	/**
	 * Sætter ordrens slutdato.
	 */
	public static void setOrdreDatoSlut(Ordre ordre, LocalDate datoSlut) {
		ordre.setDatoSlut(datoSlut);
	}

	/**
	 * Opdaterer ordrens rabat.
	 */
	public static void setOrdreRabat(Ordre ordre, Rabat rabat) {
		ordre.setRabat(rabat);
	}

	/**
	 * Sætter ordrens rabat.
	 */
	public static void setOrdreRabatMængde(Ordre ordre, double rabatMængde) {
		ordre.setRabatMængde(rabatMængde);
	}

	/**
	 * Sætter ordrens betalingsmetoder til et TreeMap af betalingsmetoder og beløb.
	 */
	public static void setBetalingsmetoder(Ordre ordre, TreeMap<Betalingsmetode, Double> betalingsmetoder) {
		ordre.setBetalingsmetoder(betalingsmetoder);
	}

	/**
	 * Tilføjer en betalingsmetode og et beløb til ordrens betalingsmetoder.<br/>
	 * Pre: Ordrens total med rabat må ikke være mindre end de samlede beløb af
	 * betalingsmetoderne.
	 */
	public static void addBetalingsmetode(Ordre ordre, Betalingsmetode betalingsmetode, double beløb) {
		ordre.addBetalingsmetode(betalingsmetode, beløb);
	}

	/**
	 * Opretter en ordrelinje med en pris bestående af listeprisens pris gange
	 * antal.
	 */
	public static Ordrelinje createOrdrelinje(Ordre ordre, Listepris listepris, int antal, boolean betalt) {
		Ordrelinje ordrelinje;
		if (listepris.getProdukt() instanceof Sampakning) {
			ordrelinje = ordre.createOrdrelinjeSampak(listepris, antal, betalt);
		} else {
			ordrelinje = ordre.createOrdrelinje(listepris, antal, betalt);
		}
		return ordrelinje;
	}

	/**
	 * Opretter en ordrelinje med en fastsat pris.<br/>
	 * Pre: nyPris er positiv.
	 */
	public static Ordrelinje createOrdrelinje(Ordre ordre, Listepris listepris, double nyPris, int antal,
			boolean betalt) {
		Ordrelinje ordrelinje;
		if (listepris.getProdukt() instanceof Sampakning) {
			ordrelinje = ordre.createOrdrelinjeSampak(listepris, antal, betalt);
		} else {
			ordrelinje = ordre.createOrdrelinje(listepris, antal, betalt);
		}
		ordrelinje.setPris(nyPris);
		return ordrelinje;
	}

	/**
	 * Opretter en ordrelinje for rundvisninger med en pris bestående af
	 * listeprisens pris gange antal.
	 */
	public static OrdrelinjeRundvisning createOrdrelinjeRundvisning(Ordre ordre, Listepris listepris, int antal,
			boolean betalt, String beskrivelse, LocalDateTime tidspunkt) {
		OrdrelinjeRundvisning olr = ordre.createOrdrelinjeRundvisning(listepris, beskrivelse, tidspunkt, antal, betalt);
		return olr;
	}

	/**
	 * Opretter en ordrelinje for rundvisninger med en fastsat pris.
	 */
	public static OrdrelinjeRundvisning createOrdrelinjeRundvisning(Ordre ordre, Listepris listepris, double nyPris,
			int antal, boolean betalt, String beskrivelse, LocalDateTime tidspunkt) {
		OrdrelinjeRundvisning olr = ordre.createOrdrelinjeRundvisning(listepris, beskrivelse, tidspunkt, antal, betalt);
		olr.setPris(nyPris);
		return olr;
	}

	/**
	 * Sætter en OrdrelinjeSampaks brygArray.
	 */
	public static void setOrdrelinjeSampakBrygArray(OrdrelinjeSampak ordrelinje, Bryg[] nyArray) {
		for (Bryg bryg : nyArray) {
			if (bryg == null) {
				throw new ArrayIkkeFyldtException("Array skal være fyldt med bryg.");
			}
		}
		if (ordrelinje.getBrygArray().length == nyArray.length) {
			ordrelinje.setBrygArray(nyArray);
		}
	}

	/**
	 * Sætter en OrdrelinjeSampaks glasArray.
	 */
	public static void setOrdrelinjeSampakGlasArray(OrdrelinjeSampak ordrelinje, Glas[] nyArray) {
		for (Glas glas : nyArray) {
			if (glas == null) {
				throw new ArrayIkkeFyldtException("Array skal være fyldt med bryg.");
			}
		}
		if (ordrelinje.getGlasArray().length == nyArray.length) {
			ordrelinje.setGlasArray(nyArray);
		}
	}

	/**
	 * Sætter en OrdrelinjeRundvisnings beskrivelse og tidspunkt.
	 */
	public static void editOrdrelinjeRundvisning(OrdrelinjeRundvisning olr, String beskrivelse,
			LocalDateTime tidspunkt) {
		olr.setBeskrivelse(beskrivelse);
		olr.setTidspunkt(tidspunkt);
	}

	/**
	 * Sætter en ordrelinjes antal.
	 */
	public static void setOrdrelinjeAntal(Ordrelinje ordrelinje, int antal) {
		ordrelinje.setAntal(antal);
	}

	/**
	 * Sætter en eksisterende ordrelinjes pris til ny pris.<br/>
	 * Pre: Pris er positiv.
	 */
	public static void setOrdrelinjePris(Ordrelinje ordrelinje, double pris) {
		ordrelinje.setPris(pris);
	}

	/**
	 * Sætter en eksisterende ordrelinjes pris til listeprisens pris gange antal.
	 */
	public static void resetOrdrelinjePris(Ordrelinje ordrelinje) {
		ordrelinje.resetPris();
	}

	/**
	 * Sætter en eksisterende ordrelinje til betalt (hvis true) eller ikke betalt
	 * (hvis false).
	 */
	public static void setOrdrelinjeBetalt(Ordrelinje ordrelinje, boolean betalt) {
		ordrelinje.setBetalt(betalt);
	}

	// -------------------------------------------------------------------------

	public static ArrayList<Prisliste> getPrislister() {
		return storage.getPrislister();
	}

	/**
	 * Opretter en ny prisliste. Kaster en exception, hvis salgssituationen allerede
	 * er i brug.
	 */
	public static Prisliste createPrisliste(String salgssituation, boolean simpel) {
		for (Prisliste pl : Controller.getPrislister()) {
			if (pl.getSalgssituation().toLowerCase().equals(salgssituation.toLowerCase())) {
				throw new SalgssituationTagetException("Salgssituation er allerede taget");
			}
		}
		Prisliste prisliste = new Prisliste(salgssituation, simpel);
		storage.addPrisliste(prisliste);
		return prisliste;
	}

	/**
	 * Redigerer en eksisterende prisliste. Kaster en exception, hvis
	 * salgssituationen allerede er i brug.
	 */
	public static void editPrisliste(Prisliste prisliste, String salgssituation, boolean simpel) {
		for (Prisliste pl : Controller.getPrislister()) {
			if (pl.getSalgssituation().toLowerCase().equals(salgssituation.toLowerCase()) && pl != prisliste) {
				throw new SalgssituationTagetException("Salgssituation er allerede taget");
			}
		}
		prisliste.setSalgssituation(salgssituation);
		prisliste.setSimpel(simpel);
	}

	public static void deletePrisliste(Prisliste prisliste) {
		storage.removePrisliste(prisliste);
	}

	// -------------------------------------------------------------------------

	/**
	 * Angiver ny pris for produktet på prislinjen, hvis en listepris allerede
	 * eksisterer. Opretter ellers en ny listepris.<br/>
	 * Pre: Pris er positiv.
	 */
	public static Listepris setListepris(Prisliste prisliste, Produkt produkt, double pris) {
		Listepris listepris = prisliste.setListepris(produkt, pris);
		return listepris;
	}

	public static void deleteListepris(Prisliste prisliste, Listepris listepris) {
		prisliste.removeListepris(listepris);
	}

	// -------------------------------------------------------------------------

	public static ArrayList<Kunde> getKunder() {
		return storage.getKunder();
	}

	public static Kunde createKunde(String navn, String telefonnummer, String adresse, String leveringsadresse) {
		Kunde kunde = new Kunde(navn, telefonnummer, adresse, leveringsadresse);
		storage.addKunde(kunde);
		return kunde;
	}

	public static void editKunde(Kunde kunde, String navn, String telefonnummer, String adresse,
			String leveringsadresse) {
		kunde.setNavn(navn);
		kunde.setTelefonnummer(telefonnummer);
		kunde.setAdresse(leveringsadresse);
		kunde.setLeveringsadresse(leveringsadresse);
	}

	public static void deleteKunde(Kunde kunde) {
		storage.removeKunde(kunde);
	}

	// -------------------------------------------------------------------------

	public static ArrayList<Bruger> getBrugere() {
		return storage.getBrugere();
	}

	/**
	 * Opretter en ny bruger. Kaster en exception, hvis brugernavn allerede er
	 * taget.
	 */
	public static Bruger createBruger(String navn, String brugernavn, String password, boolean isAdmin) {
		for (Bruger b : getBrugere()) {
			if (b.getBrugernavn().equals(brugernavn)) {
				throw new BrugernavnTagetException("Brugernavn er allerede taget.");
			}
		}
		Bruger bruger = new Bruger(navn, brugernavn, password, isAdmin);
		storage.addBruger(bruger);
		return bruger;
	}

	public static void deleteBruger(Bruger bruger) {
		storage.removeBruger(bruger);
	}

	// -------------------------------------------------------------------------

	public static ArrayList<Rabat> getRabatter() {
		return storage.getRabatter();
	}

	public static Rabat createIngenRabat() {
		Rabat ingenRabat = new IngenRabat();
		storage.addRabat(ingenRabat);
		return ingenRabat;
	}

	public static Rabat createProcentRabat() {
		Rabat procentRabat = new ProcentRabat();
		storage.addRabat(procentRabat);
		return procentRabat;
	}

	public static Rabat createFastRabat() {
		Rabat aftaltPris = new AftaltPris();
		storage.addRabat(aftaltPris);
		return aftaltPris;
	}

	// -------------------------------------------------------------------------

	/**
	 * Returnerer true, hvis rundvisningen kan reserveres.<br/>
	 * Returnerer false, hvis rundvisningen vil overlappe med en anden rundvisning,
	 * vil slutte efter kl. 15 en fredag, eller ligger i weekenden.<br/>
	 * Pre: dato er ikke null.<br/>
	 * Pre: tid er ikke null.
	 */
	public static boolean erTidspunktLedigt(LocalDate dato, LocalTime tid) {
		LocalDateTime tidspunkt = LocalDateTime.of(dato, tid);
		if (tidspunkt.isAfter(LocalDateTime.now())) {
			if (tidspunkt.getDayOfWeek().equals(DayOfWeek.FRIDAY)
					&& tid.plusMinutes(90).isAfter(LocalTime.of(15, 00))) {
				return false;
			}
			if (dato.getDayOfWeek().equals(DayOfWeek.SATURDAY) || dato.getDayOfWeek().equals(DayOfWeek.SUNDAY)) {
				return false;
			}
			for (Ordre ordre : Controller.getOrdrer()) {
				for (Ordrelinje ol : ordre.getOrdrelinjer()) {
					if (ol instanceof OrdrelinjeRundvisning) {
						OrdrelinjeRundvisning olr = (OrdrelinjeRundvisning) ol;
						if (tidspunkt.plusMinutes(90).isBefore(olr.getTidspunkt())
								|| olr.getTidspunkt().plusMinutes(90).isBefore(tidspunkt)) {
							// fortsæt loop
						} else {
							return false;
						}
					}
				}
			}
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Returnerer true, hvis produktet i ordrelinjen kan købes med klip. Ellers
	 * returneres false.<br/>
	 * Produktgrupper, der kan købes med klip: Fadøl, Flaske, Snacks, Vand.
	 */
	public static boolean kanKlipBruges(Ordrelinje ordrelinje) {
		if (ordrelinje != null) {
			Produktgruppe produktgruppe = ordrelinje.getListepris().getProdukt().getProduktgruppe();
			if (produktgruppe.equals(Produktgruppe.FADØL) || produktgruppe.equals(Produktgruppe.FLASKE)
					|| produktgruppe.equals(Produktgruppe.SNACKS) || produktgruppe.equals(Produktgruppe.VAND)) {
				return true;
			} else {
				return false;
			}
		}
		return false;
	}

	/**
	 * Returnerer hvor mange klip, den pågældende listepris vil koste.
	 */
	public static int prisIKlip(Listepris listepris) {
		double prisPrKlip = 130.0 / 4.0; // Et klippekort med 4 klip koster 130 kr.
		double ekstraVærdiSomKlip = 1.20; // Et klip er 20% ekstra værd, når det bruges til fuld betaling
		int prisIKlip = (int) Math.ceil(listepris.getPris() / (prisPrKlip * ekstraVærdiSomKlip));
		return prisIKlip;
	}

	/**
	 * Returnerer hvor meget, der skal betales kontant, når klippene er brugt på
	 * listeprisen gange antal. Hvis et eller flere produkter kan betales fuldt med
	 * klip, gøres dette først. Udregningen bliver dermed billigst for kunden.
	 */
	public static double resterendePrisEfterKlip(Listepris listepris, int antal, int klip) {
		if (klip < (prisIKlip(listepris) * antal)) {
			double prisPrKlip = 130.0 / 4.0; // Et klippekort med 4 klip koster 130 kr.
			double blandetPris = 0;
			while (klip >= prisIKlip(listepris)) {
				klip -= prisIKlip(listepris);
				antal--;
			}
			if (klip > 0) {
				blandetPris = listepris.getPris() - klip * prisPrKlip;
				antal--;
			}
			double prisEfterKlip = (listepris.getPris() * antal) + blandetPris;
			return prisEfterKlip;
		} else {
			return 0;
		}
	}

	// -------------------------------------------------------------------------

	/**
	 * Returnerer dagens ordrer.
	 */
	public static ArrayList<Ordre> statistikOrdrer() {
		ArrayList<Ordre> periodeOrdrer = new ArrayList<>();
		for (Ordre ordre : Controller.getOrdrer()) {
			if (ordre.getDatoStart().isEqual(LocalDate.now())) {
				periodeOrdrer.add(ordre);
			}
		}
		return periodeOrdrer;
	}

	/**
	 * Returnerer ordrerne fra den angivne periode.<br/>
	 * Pre: datoStart er før datoSlut.<br/>
	 * Pre: Ingen af datoerne er null.
	 */
	public static ArrayList<Ordre> statistikOrdrer(LocalDate datoStart, LocalDate datoSlut) {
		ArrayList<Ordre> periodeOrdrer = new ArrayList<>();
		for (Ordre ordre : Controller.getOrdrer()) {
			if (!ordre.getDatoStart().isBefore(datoStart) && !ordre.getDatoStart().isAfter(datoSlut)) {
				periodeOrdrer.add(ordre);
			}
		}
		return periodeOrdrer;
	}

	/**
	 * Returnerer det totale beløb betalt med Dankort fra ordrerne.
	 */
	public static double statistikDankort(ArrayList<Ordre> ordrer) {
		double dankort = 0;
		for (Ordre ordre : ordrer) {
			if (!ordre.getOrdrelinjer().isEmpty()) {
				for (Entry<Betalingsmetode, Double> entry : ordre.getBetalingsmetoder().entrySet()) {
					if (entry.getKey().getType().equals("Kort")) {
						dankort += entry.getValue();
					}
				}
			}
		}
		return dankort;
	}

	/**
	 * Returnerer det totale beløb betalt med kontanter fra ordrerne.
	 */
	public static double statistikKontant(ArrayList<Ordre> ordrer) {
		double kontant = 0;
		for (Ordre ordre : ordrer) {
			if (!ordre.getOrdrelinjer().isEmpty()) {
				for (Entry<Betalingsmetode, Double> entry : ordre.getBetalingsmetoder().entrySet()) {
					if (entry.getKey().getType().equals("Kontant")) {
						kontant += entry.getValue();
					}
				}
			}
		}
		return kontant;
	}

	/**
	 * Returnerer det totale beløb betalt med MobilePay fra ordrerne.
	 */
	public static double statistikMobilePay(ArrayList<Ordre> ordrer) {
		double mobilePay = 0;
		for (Ordre ordre : ordrer) {
			if (!ordre.getOrdrelinjer().isEmpty()) {
				for (Entry<Betalingsmetode, Double> entry : ordre.getBetalingsmetoder().entrySet()) {
					if (entry.getKey().getType().equals("MobilePay")) {
						mobilePay += entry.getValue();
					}
				}
			}
		}
		return mobilePay;
	}

	/**
	 * Returnerer det totale beløb betalt med klip fra ordrerne.
	 */
	public static double statistikKlipBrugt(ArrayList<Ordre> ordrer) {
		double klip = 0;
		for (Ordre ordre : ordrer) {
			if (!ordre.getOrdrelinjer().isEmpty()) {
				for (Entry<Betalingsmetode, Double> entry : ordre.getBetalingsmetoder().entrySet()) {
					if (entry.getKey().getType().equals("Klippekort")) {
						klip += entry.getValue();
					}
				}
			}
		}
		return klip;
	}

	/**
	 * Returnerer det totale antal af klip købt fra ordrerne.
	 */
	public static int statistikKlipKøbt(ArrayList<Ordre> ordrer) {
		int klipKøbt = 0;
		for (Ordre ordre : ordrer) {
			if (!ordre.getOrdrelinjer().isEmpty()) {
				for (Ordrelinje ol : ordre.getOrdrelinjer()) {
					Produkt produkt = ol.getListepris().getProdukt();
					if (produkt.getProduktgruppe().equals(Produktgruppe.KLIPPEKORT)) {
						Klippekort klippekort = (Klippekort) produkt;
						klipKøbt += klippekort.getKlip();
					}
				}
			}
		}
		return klipKøbt;
	}

	/**
	 * Returnerer det totale beløb uden rabat fra ordrerne.
	 */
	public static double statistikTotalUdenRabat(ArrayList<Ordre> ordrer) {
		double totalUdenRabat = 0;
		for (Ordre ordre : ordrer) {
			if (!ordre.getOrdrelinjer().isEmpty()) {
				totalUdenRabat += ordre.getTotalUdenRabat();
			}
		}
		return totalUdenRabat;
	}

	/**
	 * Returnerer det totale beløb med rabat fra ordrerne.
	 */
	public static double statistikTotalMedRabat(ArrayList<Ordre> ordrer) {
		double totalMedRabat = 0;
		for (Ordre ordre : ordrer) {
			if (!ordre.getOrdrelinjer().isEmpty()) {
				totalMedRabat += ordre.getTotalMedRabat();
			}
		}
		return totalMedRabat;
	}

	/**
	 * Returnerer den gennemsnitlige rabat per ordre fra ordrerne.
	 */
	public static double statistikGnsRabat(ArrayList<Ordre> ordrer) {
		double totalUdenRabat = 0;
		double totalMedRabat = 0;
		double antal = 0;
		for (Ordre ordre : ordrer) {
			if (!ordre.getOrdrelinjer().isEmpty()) {
				totalUdenRabat += ordre.getTotalUdenRabat();
				totalMedRabat += ordre.getTotalMedRabat();
				antal++;
			}
		}
		if (antal > 0) {
			return (totalUdenRabat - totalMedRabat) / antal;
		} else {
			return 0;
		}
	}

	/**
	 * Returnerer den gennemsnitlige pris efter rabat per ordre fra ordrerne.
	 */
	public static double statistikGnsPris(ArrayList<Ordre> ordrer) {
		double totalMedRabat = 0;
		double antal = 0;
		for (Ordre ordre : ordrer) {
			if (!ordre.getOrdrelinjer().isEmpty()) {
				totalMedRabat += ordre.getTotalMedRabat();
				antal++;
			}
		}
		if (antal > 0) {
			return totalMedRabat / antal;
		} else {
			return 0;
		}
	}

	// -------------------------------------------------------------------------

	public static void init() {
		try {
			loadStorage();
		} catch (RuntimeException ex) {
			storage = Storage.getStorage();
			initStorage();
		}
	}

	/**
	 * Initialiserer storage med nogle objekter.
	 */
	private static void initStorage() {
		createBruger("BeerChamp", "Admin", "admin", true);
		createBruger("Bent Børge", "Bruger", "bruger", false);

		createBetalingsmetode("Kort");
		createBetalingsmetode("MobilePay");
		createBetalingsmetode("Kontant");
		createBetalingsmetode("Klippekort");

		createIngenRabat();
		createProcentRabat();
		createFastRabat();

		// ---

		Anlæg haneen = createAnlæg("1-hane");
		Anlæg haneto = createAnlæg("2-haner");
		Anlæg bar = createAnlæg("Bar med flere haner");
		Anlæg krus = createAnlæg("Krus");

		Beklædning tshirt = createBeklædning("T-shirt med logo", "Large", "Mørkegrøn", "Bomuld");
		Beklædning polo = createBeklædning("Polo med logo", "Medium", "Hvid", "Bomuld");
		Beklædning cap = createBeklædning("Cap med logo", "One-size", "Mørkegrøn", "Bomuld");

		ArrayList<Bryg> fadøl = new ArrayList<>();
		fadøl.add(createBryg(Produktgruppe.FADØL, "Klosterbryg", 40, "cl", 6.0));
		fadøl.add(createBryg(Produktgruppe.FADØL, "Jazz Classic", 40, "cl", 5.0));
		fadøl.add(createBryg(Produktgruppe.FADØL, "Extra Pilsner", 40, "cl", 7.0));
		fadøl.add(createBryg(Produktgruppe.FADØL, "Celebration", 40, "cl", 6.5));
		fadøl.add(createBryg(Produktgruppe.FADØL, "Blondie", 40, "cl", 5.0));
		fadøl.add(createBryg(Produktgruppe.FADØL, "Forårsbryg", 40, "cl", 7.0));
		fadøl.add(createBryg(Produktgruppe.FADØL, "India Pale Ale", 40, "cl", 6.0));
		fadøl.add(createBryg(Produktgruppe.FADØL, "Julebryg", 40, "cl", 6.0));
		fadøl.add(createBryg(Produktgruppe.FADØL, "Imperial Stout", 40, "cl", 8.0));
		fadøl.add(createBryg(Produktgruppe.FADØL, "Special", 40, "cl", 6.5));
		Bryg æblebrus = createBryg(Produktgruppe.FADØL, "Æblebrus", 40, "cl", 0.0);

		ArrayList<Bryg> flasker = new ArrayList<>();
		flasker.add(createBryg(Produktgruppe.FLASKE, "Klosterbryg", 60, "cl", 6.0));
		flasker.add(createBryg(Produktgruppe.FLASKE, "Sweet Georgia Brown", 60, "cl", 5.5));
		flasker.add(createBryg(Produktgruppe.FLASKE, "Extra Pilsner", 60, "cl", 7.0));
		flasker.add(createBryg(Produktgruppe.FLASKE, "Celebration", 60, "cl", 6.5));
		flasker.add(createBryg(Produktgruppe.FLASKE, "Blondie", 60, "cl", 5.0));
		flasker.add(createBryg(Produktgruppe.FLASKE, "Forårsbryg", 60, "cl", 7.0));
		flasker.add(createBryg(Produktgruppe.FLASKE, "India Pale Ale", 60, "cl", 6.0));
		flasker.add(createBryg(Produktgruppe.FLASKE, "Julebryg", 60, "cl", 6.0));
		flasker.add(createBryg(Produktgruppe.FLASKE, "Juletønden", 60, "cl", 8.0));
		flasker.add(createBryg(Produktgruppe.FLASKE, "Fregatten Jylland", 60, "cl", 8.0));
		flasker.add(createBryg(Produktgruppe.FLASKE, "Imperial Stout", 60, "cl", 8.0));
		flasker.add(createBryg(Produktgruppe.FLASKE, "Aarhus Tribute", 60, "cl", 9.0));
		Bryg blackMonster = createBryg(Produktgruppe.FLASKE, "Black Monster", 60, "cl", 10.0);

		Produkt klosterbryg = createBryg(Produktgruppe.FUSTAGE, "Klosterbryg", 20, "L", 6.0);
		Produkt jazzClassic = createBryg(Produktgruppe.FUSTAGE, "Jazz Classic", 25, "L", 5.0);
		Produkt extraPilsner = createBryg(Produktgruppe.FUSTAGE, "Extra Pilsner", 25, "L", 7.0);
		Produkt celebration = createBryg(Produktgruppe.FUSTAGE, "Celebration", 20, "L", 6.5);
		Produkt blondie = createBryg(Produktgruppe.FUSTAGE, "Blondie", 25, "L", 5.0);
		Produkt forårsbryg = createBryg(Produktgruppe.FUSTAGE, "Forårsbryg", 20, "L", 7.0);
		Produkt indiaPaleAle = createBryg(Produktgruppe.FUSTAGE, "India Pale Ale", 20, "L", 6.0);
		Produkt julebryg = createBryg(Produktgruppe.FUSTAGE, "Julebryg", 20, "L", 6.0);
		Produkt imperialStout = createBryg(Produktgruppe.FUSTAGE, "Imperial Stout", 20, "L", 8.0);

		Bryg soa = createBryg(Produktgruppe.SPIRITUS, "Spirit of Aarhus", 50, "cl", 40.0);
		Bryg soapind = createBryg(Produktgruppe.SPIRITUS, "Spirit of Aarhus med egepind", 50, "cl", 40.0);
		Bryg whisky = createBryg(Produktgruppe.SPIRITUS, "Whisky", 50, "cl", 43.0);
		Bryg loa = createBryg(Produktgruppe.SPIRITUS, "Liquor of Aarhus", 35, "cl", 30.0);

		Diverse chips = createDiverse(Produktgruppe.SNACKS, "Chips", 50, "g");
		Diverse peanuts = createDiverse(Produktgruppe.SNACKS, "Peanuts", 100, "g");
		Diverse cola = createDiverse(Produktgruppe.VAND, "Cola", 25, "cl");
		Diverse nikoline = createDiverse(Produktgruppe.VAND, "Nikoline", 25, "cl");
		Diverse sevenup = createDiverse(Produktgruppe.VAND, "7UP", 25, "cl");
		Diverse vand = createDiverse(Produktgruppe.VAND, "Vand", 50, "cl");

		Glas glas = createGlas("Ølglas", 55, "cl");
		Klippekort klippekort = createKlippekort("Klippekort", 4);

		Kulsyre kulsyre = createKulsyre("Flaske", 6);
		createKulsyre("Flaske", 4);
		createKulsyre("Flaske", 10);

		createLevering("Levering til nærområdet", 10);
		createLevering("Levering i Jylland", 200);
		Levering leveringAnlæg = createLevering("Levering af anlæg i nærområdet", 10);

		Malt malt = createMalt("Sæk", 25);

		Pant pantFustage = createPant("Pant for fustage");
		Pant pantKulsyre = createPant("Pant for kulsyre");

		Rundvisning rundvisningTidlig = createRundvisning("Rundvisning før 16");
		Rundvisning rundvisningSen = createRundvisning("Rundvisning efter 16");

		Sampakning sampak22 = createSampakning("Gaveæske", 2, 2);
		Sampakning sampak4 = createSampakning("Gaveæske", 4, 0);
		Sampakning sampak6 = createSampakning("Trækasse", 6, 0);
		Sampakning sampak62 = createSampakning("Gavekurv", 6, 2);
		Sampakning sampak66 = createSampakning("Trækasse", 6, 6);
		Sampakning sampak12træ = createSampakning("Trækasse", 12, 0);
		Sampakning sampak12pap = createSampakning("Papkasse", 12, 0);

		// ---

		// fredagsbar
		Prisliste fredagsbar = createPrisliste("Fredagsbar", true);
		for (Bryg bryg : fadøl) {
			setListepris(fredagsbar, bryg, 38);
		}
		for (Bryg bryg : flasker) {
			setListepris(fredagsbar, bryg, 70);
		}
		setListepris(fredagsbar, blackMonster, 100);
		setListepris(fredagsbar, æblebrus, 15);
		setListepris(fredagsbar, chips, 10);
		setListepris(fredagsbar, peanuts, 15);
		setListepris(fredagsbar, cola, 15);
		setListepris(fredagsbar, nikoline, 15);
		setListepris(fredagsbar, sevenup, 15);
		setListepris(fredagsbar, vand, 10);
		setListepris(fredagsbar, klippekort, 130);
		setListepris(fredagsbar, soa, 300);
		setListepris(fredagsbar, soapind, 350);
		setListepris(fredagsbar, whisky, 500);
		setListepris(fredagsbar, loa, 175);
		setListepris(fredagsbar, kulsyre, 400);
		setListepris(fredagsbar, pantKulsyre, 1000);
		setListepris(fredagsbar, sampak22, 110);
		setListepris(fredagsbar, sampak4, 140);
		setListepris(fredagsbar, sampak6, 260);
		setListepris(fredagsbar, sampak62, 260);
		setListepris(fredagsbar, sampak66, 350);
		setListepris(fredagsbar, sampak12træ, 410);
		setListepris(fredagsbar, sampak12pap, 370);
		setListepris(fredagsbar, tshirt, 70);
		setListepris(fredagsbar, polo, 100);
		setListepris(fredagsbar, cap, 30);

		// butik
		Prisliste butik = createPrisliste("Butik", false);
		for (Bryg bryg : flasker) {
			setListepris(butik, bryg, 36);
		}
		setListepris(butik, blackMonster, 60);
		setListepris(butik, klosterbryg, 775);
		setListepris(butik, jazzClassic, 625);
		setListepris(butik, extraPilsner, 575);
		setListepris(butik, celebration, 775);
		setListepris(butik, blondie, 700);
		setListepris(butik, forårsbryg, 775);
		setListepris(butik, indiaPaleAle, 775);
		setListepris(butik, julebryg, 775);
		setListepris(butik, imperialStout, 775);
		setListepris(butik, pantFustage, 200);
		setListepris(butik, klippekort, 130);
		setListepris(butik, soa, 300);
		setListepris(butik, soapind, 350);
		setListepris(butik, whisky, 500);
		setListepris(butik, loa, 175);
		setListepris(butik, glas, 15);
		setListepris(butik, kulsyre, 400);
		setListepris(butik, pantKulsyre, 1000);
		setListepris(butik, malt, 300);
		setListepris(butik, haneen, 250);
		setListepris(butik, haneto, 400);
		setListepris(butik, bar, 500);
		setListepris(butik, krus, 60);
		setListepris(butik, leveringAnlæg, 500);
		setListepris(butik, sampak22, 110);
		setListepris(butik, sampak4, 140);
		setListepris(butik, sampak6, 260);
		setListepris(butik, sampak62, 260);
		setListepris(butik, sampak66, 350);
		setListepris(butik, sampak12træ, 410);
		setListepris(butik, sampak12pap, 370);
		setListepris(butik, rundvisningTidlig, 100);
		setListepris(butik, rundvisningSen, 120);
		setListepris(butik, tshirt, 70);
		setListepris(butik, polo, 100);
		setListepris(butik, cap, 30);

		// ---

		Kunde roy = createKunde("Roy Hurtigkoder", "88888888", "Hurtigvej 2, 9999 Java Landsby",
				"Hurtigvej 2, 9999 Java Landsby");
		Ordre ordre = createOrdre(roy, LocalDate.of(2020, 1, 1), new IngenRabat(), 0);
		createOrdrelinje(ordre, butik.getListepriser().get(15), 2, false);
		createOrdrelinje(ordre, butik.getListepriser().get(22), 2, true);

		Ordre ordre2 = createOrdre(roy, LocalDate.of(2020, 3, 21), new IngenRabat(), 0);
		createOrdrelinje(ordre2, fredagsbar.getListepriser().get(1), 3, false);
		createOrdrelinje(ordre2, fredagsbar.getListepriser().get(45), 3, false);

		Ordre ordre3 = createOrdre(roy, LocalDate.now(), new IngenRabat(), 0);
		createOrdrelinje(ordre3, fredagsbar.getListepriser().get(2), 4, false);
		createOrdrelinje(ordre3, fredagsbar.getListepriser().get(45), 100, false);
	}
}