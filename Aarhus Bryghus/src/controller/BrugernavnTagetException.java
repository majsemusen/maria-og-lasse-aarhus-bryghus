package controller;

public class BrugernavnTagetException extends RuntimeException {

	public BrugernavnTagetException(String message) {
		super(message);
	}

	public BrugernavnTagetException(String message, Throwable cause) {
		super(message, cause);
	}
}