package storage;

import java.io.Serializable;
import java.util.ArrayList;

import model.Betalingsmetode;
import model.Bruger;
import model.Kunde;
import model.Ordre;
import model.Prisliste;
import model.Produkt;
import model.Rabat;

public class Storage implements Serializable {

	private static Storage uniqueStorage;

	private ArrayList<Prisliste> prislister = new ArrayList<>();
	private ArrayList<Produkt> produkter = new ArrayList<>();
	private ArrayList<Ordre> ordrer = new ArrayList<>();
	private ArrayList<Betalingsmetode> betalingsmetoder = new ArrayList<>();
	private ArrayList<Kunde> kunder = new ArrayList<>();
	private ArrayList<Bruger> brugere = new ArrayList<>();
	private ArrayList<Rabat> rabatter = new ArrayList<>();

	private Storage() {
	}

	public static Storage getStorage() {
		if (uniqueStorage == null) {
			uniqueStorage = new Storage();
		}
		return uniqueStorage;
	}

	// -------------------------------------------------------------------------

	public ArrayList<Prisliste> getPrislister() {
		return new ArrayList<>(prislister);
	}

	public void addPrisliste(Prisliste prisliste) {
		prislister.add(prisliste);
	}

	public void removePrisliste(Prisliste prisliste) {
		prislister.remove(prisliste);
	}

	// -------------------------------------------------------------------------

	public ArrayList<Produkt> getProdukter() {
		return new ArrayList<>(produkter);
	}

	public void addProdukt(Produkt produkt) {
		produkter.add(produkt);
	}

	public void removeProdukt(Produkt produkt) {
		produkter.remove(produkt);
	}

	// -------------------------------------------------------------------------

	public ArrayList<Ordre> getOrdrer() {
		return new ArrayList<>(ordrer);
	}

	public void addOrdre(Ordre ordre) {
		ordrer.add(ordre);
	}

	public void removeOrdre(Ordre ordre) {
		ordrer.remove(ordre);
	}
	// -------------------------------------------------------------------------

	public ArrayList<Betalingsmetode> getBetalingsmetoder() {
		return new ArrayList<>(betalingsmetoder);
	}

	public void addBetalingsmetode(Betalingsmetode betalingsmetode) {
		betalingsmetoder.add(betalingsmetode);
	}

	public void removeBetalingsmetode(Betalingsmetode betalingsmetode) {
		betalingsmetoder.remove(betalingsmetode);
	}
	// -------------------------------------------------------------------------

	public ArrayList<Kunde> getKunder() {
		return new ArrayList<>(kunder);
	}

	public void addKunde(Kunde kunde) {
		kunder.add(kunde);
	}

	public void removeKunde(Kunde kunde) {
		kunder.remove(kunde);
	}

	// -------------------------------------------------------------------------

	public ArrayList<Bruger> getBrugere() {
		return new ArrayList<>(brugere);
	}

	public void addBruger(Bruger bruger) {
		brugere.add(bruger);
	}

	public void removeBruger(Bruger bruger) {
		brugere.remove(bruger);
	}

	// --------------------------------------------------------------------------
	public ArrayList<Rabat> getRabatter() {
		return new ArrayList<>(rabatter);
	}

	public void addRabat(Rabat rabat) {
		rabatter.add(rabat);
	}

	public void removeRabat(Rabat rabat) {
		rabatter.remove(rabat);
	}
}