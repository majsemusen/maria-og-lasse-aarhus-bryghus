package model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class OrdrelinjeRundvisning extends Ordrelinje {

	private String beskrivelse;
	private LocalDateTime tidspunkt;

	public OrdrelinjeRundvisning(Listepris listepris, String beskrivelse, LocalDateTime tidspunkt, int antal,
			boolean betalt) {
		super(listepris, antal, betalt);
		this.beskrivelse = beskrivelse;
		this.tidspunkt = tidspunkt;
	}

	public String getBeskrivelse() {
		return beskrivelse;
	}

	public void setBeskrivelse(String beskrivelse) {
		this.beskrivelse = beskrivelse;
	}

	public LocalDateTime getTidspunkt() {
		return tidspunkt;
	}

	public void setTidspunkt(LocalDateTime tidspunkt) {
		this.tidspunkt = tidspunkt;
	}

	public String toString() {
		return String.format("%s\n%s\n%s", super.toString(), beskrivelse,
				tidspunkt.format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm")));
	}

	@Override
	public int compareTo(Ordrelinje ordrelinje) {
		if (ordrelinje instanceof OrdrelinjeRundvisning) {
			OrdrelinjeRundvisning olr = (OrdrelinjeRundvisning) ordrelinje;
			return getTidspunkt().compareTo(olr.getTidspunkt());
		} else {
			return getListepris().compareTo(ordrelinje.getListepris());
		}
	}
}