package model;

import java.io.Serializable;

public class Bryg extends Produkt implements Serializable {

	private int mængde;
	private String enhed; // cl eller l
	private double procent;

	/**
	 * Pre: Mængde er positiv.<br/>
	 * Pre: Procent er positiv eller lig 0.
	 */
	public Bryg(Produktgruppe produktgruppe, String navn, int mængde, String enhed, double procent) {
		super(navn);
		this.setProduktgruppe(produktgruppe);
		this.mængde = mængde;
		this.enhed = enhed;
		this.procent = procent;
	}

	public int getMængde() {
		return mængde;
	}

	public String getEnhed() {
		return enhed;
	}

	public double getProcent() {
		return procent;
	}

	/**
	 * Pre: Mængde er positiv.
	 */
	public void setMængde(int mængde) {
		this.mængde = mængde;
	}

	public void setEnhed(String enhed) {
		this.enhed = enhed;
	}

	/**
	 * Pre: Procent er positiv eller lig 0.
	 */
	public void setProcent(double procent) {
		this.procent = procent;
	}

	@Override
	public String produktBeskrivelse() {
		return String.format("%s %d %s %.1f%% (%s)", this.getNavn(), mængde, enhed, procent,
				this.getProduktgruppe().toString().toLowerCase());
	}
}