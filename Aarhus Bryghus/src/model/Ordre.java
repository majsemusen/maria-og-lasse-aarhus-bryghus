package model;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

public class Ordre implements Serializable {

	private List<Ordrelinje> ordrelinjer = new ArrayList<>();
	private Kunde kunde; // nullable
	private LocalDate datoStart;
	private LocalDate datoSlut;
	private Rabat rabat;
	private double rabatMængde;
	private TreeMap<Betalingsmetode, Double> betalingsmetoder = new TreeMap<Betalingsmetode, Double>();

	public Ordre(Kunde kunde, LocalDate datoStart, Rabat rabat, double rabatMængde) {
		this.kunde = kunde;
		this.datoStart = datoStart;
		this.rabat = rabat;
		this.rabatMængde = rabatMængde;
		kunde.addOrdre(this);
	}

	public Ordre(LocalDate datoStart, Rabat rabat, double rabatMængde) {
		this.datoStart = datoStart;
		this.rabat = rabat;
		this.rabatMængde = rabatMængde;
	}

	public List<Ordrelinje> getOrdrelinjer() {
		return ordrelinjer;
	}

	public Kunde getKunde() {
		return kunde;
	}

	public LocalDate getDatoStart() {
		return datoStart;
	}

	public LocalDate getDatoSlut() {
		return datoSlut;
	}

	/**
	 * Pre: datoSlut skal være samme dag som eller efter datoStart
	 */
	public void setDatoSlut(LocalDate datoSlut) {
		this.datoSlut = datoSlut;
	}

	public Rabat getRabat() {
		return rabat;
	}

	public void setRabat(Rabat rabat) {
		this.rabat = rabat;
	}

	public double getRabatMængde() {
		return rabatMængde;
	}

	/**
	 * Pre: Rabatmængden skal være et positivt tal.
	 */
	public void setRabatMængde(double rabatMængde) {
		this.rabatMængde = rabatMængde;
	}

	/**
	 * Returnerer true, hvis alle ordrelinjer er betalt. Hvis der ingen ordrelinjer
	 * er, eller mindst én ordrelinje ikke er betalt, returneres false.
	 */
	public boolean isBetalt() {
		if (ordrelinjer.size() == 0) {
			return false;
		}
		for (Ordrelinje ordrelinje : ordrelinjer) {
			if (!ordrelinje.isBetalt())
				return false;
		}
		return true;
	}

	public TreeMap<Betalingsmetode, Double> getBetalingsmetoder() {
		return betalingsmetoder;
	}

	public void setBetalingsmetoder(TreeMap<Betalingsmetode, Double> betalingsmetoder) {
		this.betalingsmetoder = betalingsmetoder;
	}

	/**
	 * Tilføjer en betalingsmetode og det beløb, der er betalt med den, til
	 * ordren.<br/>
	 * Pre: Beløbet skal være positivt.
	 */
	public void addBetalingsmetode(Betalingsmetode betalingsmetode, double beløb) {
		betalingsmetoder.put(betalingsmetode, beløb);
	}

	public double getTotalMedRabat() {
		return rabat.givRabat(getTotalUdenRabat(), rabatMængde);
	}

	public double getTotalUdenRabat() {
		double total = 0;
		for (Ordrelinje ordrelinje : ordrelinjer) {
			total += ordrelinje.getPris();
		}
		return total;
	}

	/**
	 * Opretter en ordrelinje og tilføjer den til ordrens ordrelinjer.<br/>
	 * Pre: Antal skal være et positivt tal.
	 */
	public Ordrelinje createOrdrelinje(Listepris pris, int antal, boolean betalt) {
		Ordrelinje ol = new Ordrelinje(pris, antal, betalt);
		ordrelinjer.add(ol);
		return ol;
	}

	/**
	 * Opretter en OrdrelinjeSampak og tilføjer den til ordrens ordrelinjer.<br/>
	 * Pre: Antal skal være et positivt tal.
	 */
	public OrdrelinjeSampak createOrdrelinjeSampak(Listepris pris, int antal, boolean betalt) {
		OrdrelinjeSampak ols = new OrdrelinjeSampak(pris, antal, betalt);
		ordrelinjer.add(ols);
		return ols;
	}

	/**
	 * Opretter en OrdrelinjeRundvisning og tilføjer den til ordrens
	 * ordrelinjer.<br/>
	 * Pre: Antal skal være et positivt tal.
	 */
	public OrdrelinjeRundvisning createOrdrelinjeRundvisning(Listepris pris, String beskrivelse,
			LocalDateTime tidspunkt, int antal, boolean betalt) {
		OrdrelinjeRundvisning olr = new OrdrelinjeRundvisning(pris, beskrivelse, tidspunkt, antal, betalt);
		ordrelinjer.add(olr);
		return olr;
	}

	public void removeOrdrelinje(Ordrelinje ol) {
		if (ordrelinjer.contains(ol)) {
			ordrelinjer.remove(ol);
		}
	}

	public String toString() {
		String ordreString = String.format("Ordre startet %s\nPris: %.2f kr.",
				datoStart.format(DateTimeFormatter.ISO_LOCAL_DATE), getTotalMedRabat());
		if (!isBetalt()) {
			ordreString += " (ikke betalt)";
		} else {
			ordreString += " (betalt)";
		}
		if (kunde != null) {
			ordreString += String.format("\n- %s", kunde.getNavn());
		}
		return ordreString;
	}
}