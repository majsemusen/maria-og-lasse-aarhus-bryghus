package model;

import java.io.Serializable;

public class Sampakning extends Produkt implements Serializable {

	private int bryg;
	private int glas;

	/**
	 * Pre: Bryg og glas er positive.
	 */
	public Sampakning(String navn, int bryg, int glas) {
		super(navn);
		this.setProduktgruppe(Produktgruppe.SAMPAKNING);
		this.bryg = bryg;
		this.glas = glas;
	}

	public int getBryg() {
		return bryg;
	}

	public int getGlas() {
		return glas;
	}

	/**
	 * Pre: Bryg er positiv.
	 */
	public void setBryg(int bryg) {
		this.bryg = bryg;
	}

	/**
	 * Pre: Glas er positiv.
	 */
	public void setGlas(int glas) {
		this.glas = glas;
	}

	@Override
	public String produktBeskrivelse() {
		return String.format("%s med %d bryg og %d glas (%s)", this.getNavn(), bryg, glas,
				this.getProduktgruppe().toString().toLowerCase());
	}
}