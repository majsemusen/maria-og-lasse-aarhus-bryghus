package model;

import java.io.Serializable;

public class Rundvisning extends Produkt implements Serializable {

	/**
	 * Pre: Antal er positiv.
	 */
	public Rundvisning(String navn) {
		super(navn);
		this.setProduktgruppe(Produktgruppe.RUNDVISNING);
	}

	@Override
	public String produktBeskrivelse() {
		return String.format("%s (%s)", this.getNavn(), this.getProduktgruppe().toString().toLowerCase());
	}
}