package model;

import java.io.Serializable;

public class Malt extends Produkt implements Serializable {

	private int mængde; // i kg

	/**
	 * Pre: Mængde er positiv.
	 */
	public Malt(String navn, int mængde) {
		super(navn);
		this.setProduktgruppe(Produktgruppe.MALT);
		this.mængde = mængde;
	}

	public int getMængde() {
		return mængde;
	}

	/**
	 * Pre: Mængde er positiv.
	 */
	public void setMængde(int mængde) {
		this.mængde = mængde;
	}

	@Override
	public String produktBeskrivelse() {
		return String.format("%s %d kg (%s)", this.getNavn(), mængde, this.getProduktgruppe().toString().toLowerCase());
	}
}