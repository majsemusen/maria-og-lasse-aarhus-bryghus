package model;

import java.io.Serializable;

public class Betalingsmetode implements Comparable<Betalingsmetode>, Serializable {

	private String type;

	public Betalingsmetode(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}

	public String toString() {
		return type;
	}

	@Override
	public int compareTo(Betalingsmetode bm) {
		return getType().compareTo(bm.getType());
	}
}