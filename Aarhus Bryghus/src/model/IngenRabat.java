package model;

import java.io.Serializable;

public class IngenRabat implements Rabat, Serializable {

	/**
	 * Giver ingen rabat.
	 * 
	 * @return den fulde pris
	 */
	@Override
	public double givRabat(double prisIalt, double rabat) {
		return prisIalt;
	}

	public String toString() {
		return "Ingen rabat";
	}
}