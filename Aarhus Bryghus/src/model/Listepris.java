package model;

import java.io.Serializable;

public class Listepris implements Comparable<Listepris>, Serializable {

	private Produkt produkt;
	private double pris;

	/**
	 * Pre: Pris er positiv eller 0.
	 */
	public Listepris(Produkt produkt, double pris) {
		this.produkt = produkt;
		this.pris = pris;
	}

	public Produkt getProdukt() {
		return produkt;
	}

	public double getPris() {
		return pris;
	}

	/**
	 * Pre: Pris er positiv eller 0.
	 */
	public void setPris(double pris) {
		this.pris = pris;
	}

	public String toString() {
		return String.format("%-50s %7.2f kr.", produkt, pris);
	}

	@Override
	public int compareTo(Listepris listepris) {
		return getProdukt().compareTo(listepris.getProdukt());
	}
}