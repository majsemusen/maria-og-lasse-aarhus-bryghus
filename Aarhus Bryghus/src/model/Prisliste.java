package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Prisliste implements Serializable {

	private String salgssituation;
	private boolean simpel; // er simpel, hvis simpel er true
	private final List<Listepris> listepriser = new ArrayList<>();

	public Prisliste(String salgssituation, boolean simpel) {
		this.salgssituation = salgssituation;
		this.simpel = simpel;
	}

	public String getSalgssituation() {
		return salgssituation;
	}

	public void setSalgssituation(String salgssituation) {
		this.salgssituation = salgssituation;
	}

	public boolean isSimpel() {
		return simpel;
	}

	public void setSimpel(boolean simpel) {
		this.simpel = simpel;
	}

	public List<Listepris> getListepriser() {
		return listepriser;
	}

	/**
	 * Returnerer alle listepriser på prislisten fra den specificerede
	 * produktgruppe.
	 */
	public List<Listepris> getListePriserFraProduktgruppe(Produktgruppe produktgruppe) {
		List<Listepris> lppg = new ArrayList<>();
		for (Listepris listepris : listepriser) {
			if (listepris.getProdukt().getProduktgruppe().equals(produktgruppe)) {
				lppg.add(listepris);
			}
		}
		return lppg;
	}

	/**
	 * Returnerer true, hvis der er produkter fra produktgruppen i prislisten.
	 * Ellers returneres false.
	 */
	public boolean erProduktgruppenAnvendt(Produktgruppe produktgruppe) {
		for (Listepris listepris : listepriser) {
			if (listepris.getProdukt().getProduktgruppe().equals(produktgruppe)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Opretter en ny listepris for et produkt.<br/>
	 * Kaldes via setListepris.
	 */
	private Listepris createListepris(Produkt produkt, double pris) {
		Listepris listepris = new Listepris(produkt, pris);
		listepriser.add(listepris);
		return listepris;
	}

	/**
	 * Sætter en ny pris for et produkt, der allerede har en pris i denne prisliste.
	 * Hvis produktet ikke allerede har en pris, oprettes en pris for produktet.
	 */
	public Listepris setListepris(Produkt produkt, double pris) {
		for (Listepris listepris : listepriser) {
			if (listepris.getProdukt().equals(produkt)) {
				listepris.setPris(pris);
				return listepris;
			}
		}
		return createListepris(produkt, pris);
	}

	public void removeListepris(Listepris listepris) {
		if (listepriser.contains(listepris)) {
			listepriser.remove(listepris);
		}
	}

	public String toString() {
		return salgssituation;
	}
}