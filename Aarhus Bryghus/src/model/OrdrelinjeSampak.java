package model;

import java.io.Serializable;

public class OrdrelinjeSampak extends Ordrelinje implements Serializable {

	private Bryg[] brygArray;
	private Glas[] glasArray;

	/*
	 * Constructor benyttes i GUI. Oprettes normalt gennem Prisliste.
	 */
	public OrdrelinjeSampak(Listepris listepris, int antal, boolean betalt) {
		super(listepris, antal, betalt);
		if (listepris.getProdukt() instanceof Sampakning) {
			Sampakning sampakning = (Sampakning) listepris.getProdukt();
			brygArray = new Bryg[sampakning.getBryg()];
			glasArray = new Glas[sampakning.getGlas()];
		}
	}

	public Bryg[] getBrygArray() {
		return brygArray;
	}

	/**
	 * Pre: brygArray er samme længde som den oprindelige brygArray.
	 */
	public void setBrygArray(Bryg[] brygArray) {
		this.brygArray = brygArray;
	}

	public Glas[] getGlasArray() {
		return glasArray;
	}

	/**
	 * Pre: glasArray er samme længde som den oprindelige glasArray.
	 */
	public void setGlasArray(Glas[] glasArray) {
		this.glasArray = glasArray;
	}

	public String toString() {
		String string = super.toString();
		for (Bryg bryg : brygArray) {
			string = string + String.format("\n%5s %s", "-", bryg);
		}
		for (Glas glas : glasArray) {
			string = string + String.format("\n%5s %s", "-", glas);
		}
		return string;
	}
}