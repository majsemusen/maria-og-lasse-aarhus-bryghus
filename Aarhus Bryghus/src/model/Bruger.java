package model;

import java.io.Serializable;

public class Bruger implements Serializable {

	private String navn;
	private String brugernavn;
	private String password;
	private boolean isAdmin;

	public Bruger(String navn, String brugernavn, String password, boolean isAdmin) {
		this.navn = navn;
		this.brugernavn = brugernavn;
		this.password = password;
		this.isAdmin = isAdmin;
	}

	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public String getBrugernavn() {
		return brugernavn;
	}

	public void setBrugernavn(String brugernavn) {
		this.brugernavn = brugernavn;
	}

	/**
	 * Returnerer true, hvis input svarer til brugerens password. Ellers returneres
	 * false.
	 */
	public boolean confirmPassword(String input) {
		return password.equals(input);
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isAdmin() {
		return isAdmin;
	}

	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public String toString() {
		return navn;
	}
}