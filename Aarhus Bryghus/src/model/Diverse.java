package model;

import java.io.Serializable;

public class Diverse extends Produkt implements Serializable {

	private int mængde;
	private String enhed;

	/**
	 * Pre: Mængde er positiv.
	 */
	public Diverse(Produktgruppe produktgruppe, String navn, int mængde, String enhed) {
		super(navn);
		this.setProduktgruppe(produktgruppe);
		this.mængde = mængde;
		this.enhed = enhed;
	}

	public int getMængde() {
		return mængde;
	}

	/**
	 * Pre: Mængde er positiv.
	 */
	public void setMængde(int mængde) {
		this.mængde = mængde;
	}

	public String getEnhed() {
		return enhed;
	}

	public void setEnhed(String enhed) {
		this.enhed = enhed;
	}

	@Override
	public String produktBeskrivelse() {
		return String.format("%s - %d %s (%s)", this.getNavn(), mængde, enhed,
				this.getProduktgruppe().toString().toLowerCase());
	}
}