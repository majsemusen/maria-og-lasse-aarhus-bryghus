package model;

import java.io.Serializable;

public class AftaltPris implements Rabat, Serializable {

	/**
	 * Den oprindelige pris indtastes sammen med den aftalte pris.
	 * 
	 * @return den aftalte pris
	 */
	@Override
	public double givRabat(double prisIalt, double rabat) {
		return rabat;
	}

	public String toString() {
		return "Aftalt pris";
	}
}