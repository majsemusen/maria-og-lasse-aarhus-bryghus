package model;

import java.io.Serializable;

public class ProcentRabat implements Rabat, Serializable {

	/**
	 * Rabat indtastes i procent, f.eks. 15. <br>
	 * Pre: rabat >= 0 && rabat <= 100
	 * 
	 * @return prisen med rabat
	 */
	@Override
	public double givRabat(double prisIalt, double rabat) {
		return prisIalt * (1 - rabat / 100);
	}

	public String toString() {
		return "Procentrabat";
	}
}