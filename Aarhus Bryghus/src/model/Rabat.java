package model;

public interface Rabat {

	/**
	 * Returnerer prisen med rabat.
	 * 
	 * @param prisIalt
	 * @param rabat
	 * @return
	 */
	public double givRabat(double prisIalt, double rabat);
}