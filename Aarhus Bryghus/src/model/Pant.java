package model;

import java.io.Serializable;

public class Pant extends Produkt implements Serializable {

	public Pant(String navn) {
		super(navn);
		this.setProduktgruppe(Produktgruppe.PANT);
	}

	@Override
	public String produktBeskrivelse() {
		return String.format("%s (%s)", this.getNavn(), this.getProduktgruppe().toString().toLowerCase());
	}
}