package model;

import java.io.Serializable;

public class Klippekort extends Produkt implements Serializable {

	private int klip;

	/**
	 * Pre: Klip er positiv.
	 */
	public Klippekort(String navn, int klip) {
		super(navn);
		this.setProduktgruppe(Produktgruppe.KLIPPEKORT);
		this.klip = klip;
	}

	public int getKlip() {
		return klip;
	}

	public void setKlip(int klip) {
		this.klip = klip;
	}

	@Override
	public String produktBeskrivelse() {
		return String.format("%s med %d klip (%s)", this.getNavn(), klip,
				this.getProduktgruppe().toString().toLowerCase());
	}
}