package model;

import java.io.Serializable;

public class Beklædning extends Produkt implements Serializable {

	private String størrelse;
	private String farve;
	private String materiale;

	public Beklædning(String navn, String størrelse, String farve, String materiale) {
		super(navn);
		this.setProduktgruppe(Produktgruppe.BEKLÆDNING);
		this.størrelse = størrelse;
		this.farve = farve;
		this.materiale = materiale;
	}

	public String getStørrelse() {
		return størrelse;
	}

	public String getFarve() {
		return farve;
	}

	public String getMateriale() {
		return materiale;
	}

	public void setStørrelse(String størrelse) {
		this.størrelse = størrelse;
	}

	public void setFarve(String farve) {
		this.farve = farve;
	}

	public void setMateriale(String materiale) {
		this.materiale = materiale;
	}

	@Override
	public String produktBeskrivelse() {
		return String.format("%s i %s, farve: %s, materiale: %s (%s)", this.getNavn(), størrelse, farve, materiale,
				this.getProduktgruppe().toString().toLowerCase());
	}
}