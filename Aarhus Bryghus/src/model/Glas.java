package model;

import java.io.Serializable;

public class Glas extends Produkt implements Serializable {

	private int størrelse;
	private String enhed; // ml eller cl

	/**
	 * Pre: Størrelse er positiv.
	 */
	public Glas(String navn, int størrelse, String enhed) {
		super(navn);
		this.setProduktgruppe(Produktgruppe.GLAS);
		this.størrelse = størrelse;
		this.enhed = enhed;
	}

	public int getStørrelse() {
		return størrelse;
	}

	public String getEnhed() {
		return enhed;
	}

	/**
	 * Pre: Størrelse er positiv.
	 */
	public void setStørrelse(int størrelse) {
		this.størrelse = størrelse;
	}

	public void setEnhed(String enhed) {
		this.enhed = enhed;
	}

	@Override
	public String produktBeskrivelse() {
		return String.format("%s på %d %s (%s)", this.getNavn(), størrelse, enhed,
				this.getProduktgruppe().toString().toLowerCase());
	}
}