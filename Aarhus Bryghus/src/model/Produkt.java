package model;

import java.io.Serializable;

public abstract class Produkt implements Comparable<Produkt>, Serializable {

	private Produktgruppe produktgruppe;
	private String navn;

	Produkt(String navn) {
		this.navn = navn;
	}

	public Produktgruppe getProduktgruppe() {
		return produktgruppe;
	}

	public void setProduktgruppe(Produktgruppe produktgruppe) {
		this.produktgruppe = produktgruppe;
	}

	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	/**
	 * Udskriver informationer om produktet.
	 */
	public abstract String produktBeskrivelse();

	public String toString() {
		return produktBeskrivelse();
	}

	public int compareTo(Produkt produkt) {
		return this.getNavn().compareTo(produkt.getNavn());
	}
}