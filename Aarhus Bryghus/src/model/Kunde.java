package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Kunde implements Serializable {

	private String navn;
	private String telefonnummer;
	private String adresse;
	private String leveringsadresse;
	private List<Ordre> ordreliste = new ArrayList<Ordre>();

	public Kunde(String navn, String telefonnummer, String adresse, String leveringsadresse) {
		this.navn = navn;
		this.telefonnummer = telefonnummer;
		this.adresse = adresse;
		this.leveringsadresse = leveringsadresse;
	}

	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public String getTelefonnummer() {
		return telefonnummer;
	}

	public void setTelefonnummer(String telefonnummer) {
		this.telefonnummer = telefonnummer;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getLeveringsadresse() {
		return leveringsadresse;
	}

	public void setLeveringsadresse(String leveringsadresse) {
		this.leveringsadresse = leveringsadresse;
	}

	public List<Ordre> getOrdreliste() {
		return ordreliste;
	}

	public void addOrdre(Ordre ordre) {
		if (!ordreliste.contains(ordre)) {
			ordreliste.add(ordre);
		}
	}

	public String toString() {
		return navn + " (" + telefonnummer + ")";
	}
}