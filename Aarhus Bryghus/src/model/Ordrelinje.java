package model;

import java.io.Serializable;

public class Ordrelinje implements Comparable<Ordrelinje>, Serializable {

	private Listepris listepris;
	private int antal;
	private double pris;
	private boolean prisSat;
	private boolean betalt; // true betyder, at ordren er betalt

	/*
	 * Constructor benyttes i GUI. Oprettes normalt gennem Prisliste.
	 */
	public Ordrelinje(Listepris listepris, int antal, boolean betalt) {
		this.listepris = listepris;
		this.antal = antal;
		prisSat = false;
		this.betalt = betalt;
	}

	public Listepris getListepris() {
		return listepris;
	}

	public int getAntal() {
		return antal;
	}

	public void setAntal(int antal) {
		this.antal = antal;
	}

	/**
	 * Hvis prisen er blevet ændret, returneres den ændrede pris. Ellers returneres
	 * listeprisens pris gange antal.
	 */
	public double getPris() {
		if (!prisSat) {
			return listepris.getPris() * antal;
		} else {
			return pris;
		}
	}

	/**
	 * Pre: Pris er positiv eller 0.<br/>
	 * Ønskes oprindelig pris, anvendes resetPris().
	 */
	public void setPris(double pris) {
		this.pris = pris;
		prisSat = true;
	}

	/**
	 * Resetter prisen på ordrelinjen til listeprisens pris gange antal.
	 */
	public void resetPris() {
		this.pris = 0;
		prisSat = false;
	}

	/**
	 * Returnerer true, hvis ordrelinjen er betalt, og ellers returneres false.
	 */
	public boolean isBetalt() {
		return betalt;
	}

	public void setBetalt(boolean betalt) {
		this.betalt = betalt;
	}

	public String toString() {
		return String.format("%3d %-20s %7.2f", getAntal(), listepris.getProdukt(), getPris());
	}

	@Override
	public int compareTo(Ordrelinje ordrelinje) {
		return getListepris().compareTo(ordrelinje.getListepris());
	}
}