package model;

import java.io.Serializable;

public class Levering extends Produkt implements Serializable {

	private double afstand;

	/**
	 * Pre: Afstand er positiv.
	 */
	public Levering(String navn, double afstand) {
		super(navn);
		this.setProduktgruppe(Produktgruppe.LEVERING);
		this.afstand = afstand;
	}

	/**
	 * Pre: Afstand er positiv.
	 */
	public double getAfstand() {
		return afstand;
	}

	public void setAfstand(double afstand) {
		this.afstand = afstand;
	}

	@Override
	public String produktBeskrivelse() {
		return String.format("%s indenfor %5.1f km (%s)", this.getNavn(), afstand,
				this.getProduktgruppe().toString().toLowerCase());
	}
}