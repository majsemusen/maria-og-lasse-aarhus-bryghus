package model;

import java.io.Serializable;

public class Anlæg extends Produkt implements Serializable {

	public Anlæg(String navn) {
		super(navn);
		this.setProduktgruppe(Produktgruppe.ANLÆG);
	}

	@Override
	public String produktBeskrivelse() {
		return String.format("%s (%s)", this.getNavn(), this.getProduktgruppe().toString().toLowerCase());
	}
}