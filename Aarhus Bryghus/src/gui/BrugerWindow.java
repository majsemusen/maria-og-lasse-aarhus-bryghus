package gui;

import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class BrugerWindow extends Stage {

	public BrugerWindow(String title, Stage owner) {
		this.initOwner(owner);
		this.initStyle(StageStyle.UTILITY);
		this.initModality(Modality.APPLICATION_MODAL);
		this.setResizable(false);

		this.setTitle(title);
		GridPane pane = new GridPane();
		pane.setAlignment(Pos.CENTER);
		this.initContent(pane);

		Scene scene = new Scene(pane);
		this.setScene(scene);
	}

	// -------------------------------------------------------------------------

	private void initContent(GridPane pane) {
		// viser eller skjuler grid lines
		pane.setGridLinesVisible(false);

		// sætter padding for pane
		pane.setPadding(new Insets(20));
		// sætter horisontalt mellemrum mellem elementerne
		pane.setHgap(20);
		// sætter vertikalt mellemrum mellem elementerne
		pane.setVgap(10);

		TabPane tabPane = new TabPane();
		tabPane.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
		this.initTabPane(tabPane);
		pane.add(tabPane, 0, 0);
	}

	private void initTabPane(TabPane tabPane) {
		Tab tabSalg = new Tab("Salg");
		tabPane.getTabs().add(tabSalg);
		SalgPane salgPane = new SalgPane("Salg");
		tabSalg.setContent(salgPane);

		Tab tabUdlejningBegynd = new Tab("Begynd udlejning");
		tabPane.getTabs().add(tabUdlejningBegynd);
		SalgPane udlejningBegyndPane = new SalgPane("BegyndUdlejning");
		tabUdlejningBegynd.setContent(udlejningBegyndPane);

		Tab tabUdlejninger = new Tab("Udlejninger");
		tabPane.getTabs().add(tabUdlejninger);
		StatistikAfleveringPane udlejningStatistikPane = new StatistikAfleveringPane();
		tabUdlejninger.setContent(udlejningStatistikPane);

		Tab tabRundvisning = new Tab("Rundvisning");
		tabPane.getTabs().add(tabRundvisning);
		RundvisningPane rundvisningPane = new RundvisningPane();
		tabRundvisning.setContent(rundvisningPane);

		Tab tabDagensSalg = new Tab("Dagens salg");
		tabPane.getTabs().add(tabDagensSalg);
		StatistikSalgPane dagensSalgPane = new StatistikSalgPane("DagensSalg");
		tabDagensSalg.setContent(dagensSalgPane);

		Tab tabPeriodensSalg = new Tab("Totalt salg");
		tabPane.getTabs().add(tabPeriodensSalg);
		StatistikSalgPane periodensSalgPane = new StatistikSalgPane("PeriodensSalg");
		tabPeriodensSalg.setContent(periodensSalgPane);

		ChangeListener<Tab> rundvisningListener = (ov, oldTab, newTab) -> rundvisningPane.loadRundvisninger();
		tabPane.getSelectionModel().selectedItemProperty().addListener(rundvisningListener);

		ChangeListener<Tab> udlejningListener = (ov, oldTab, newTab) -> udlejningStatistikPane.loadListViews();
		tabPane.getSelectionModel().selectedItemProperty().addListener(udlejningListener);

		ChangeListener<Tab> dagensSalgListener = (ov, oldTab, newTab) -> dagensSalgPane.loadOrdrer();
		tabPane.getSelectionModel().selectedItemProperty().addListener(dagensSalgListener);
	}
}