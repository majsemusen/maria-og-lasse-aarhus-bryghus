package gui;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Iterator;

import controller.Controller;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import model.Kunde;
import model.Listepris;
import model.Ordre;
import model.Ordrelinje;
import model.OrdrelinjeRundvisning;
import model.Prisliste;
import model.Produktgruppe;

public class RundvisningPane extends GridPane {

	private final TextField txfNavn = new TextField(), txfAntal = new TextField();
	private final DatePicker datePicker = new DatePicker();
	private final Label lblOptaget = new Label();
	private final ListView<OrdrelinjeRundvisning> lvwRundvisninger = new ListView<>();
	private final ComboBox<Prisliste> cbbPrislister = new ComboBox<>();
	private final ComboBox<Listepris> cbbListepriser = new ComboBox<>();
	private final ComboBox<String> cbbTime = new ComboBox<>(), cbbMinut = new ComboBox<>();
	private final Button btnKunde = new Button("Vælg kunde"), btnReserver = new Button("Reserver rundvisning"),
			btnDelete = new Button("Slet"), btnBetal = new Button("Betal");

	private final ArrayList<Ordre> ordrer = new ArrayList<>();
	private Kunde kunde;

	public RundvisningPane() {
		this.setAlignment(Pos.CENTER);

		// viser eller skjuler grid lines
		this.setGridLinesVisible(false);

		// sætter padding for pane
		this.setPadding(new Insets(20, 0, 0, 0));
		// sætter horisontalt mellemrum mellem elementerne
		this.setHgap(20);
		// sætter vertikalt mellemrum mellem elementerne
		this.setVgap(10);

		Label lblNyRundvisning = new Label("Planlæg ny rundvisning");
		lblNyRundvisning.setFont(Font.font(20));
		this.add(lblNyRundvisning, 0, 0, 3, 1);

		VBox prislisteBox = new VBox(10);
		this.add(prislisteBox, 0, 1, 3, 1);

		prislisteBox.getChildren().addAll(cbbPrislister, cbbListepriser);

		for (Prisliste prisliste : Controller.getPrislister()) {
			if (prisliste.erProduktgruppenAnvendt(Produktgruppe.RUNDVISNING)) {
				cbbPrislister.getItems().add(prisliste);
			}
		}
		cbbPrislister.setOnMouseClicked(event -> loadPrislisterAction());
		cbbPrislister.setOnAction(event -> prislisteAction());

		Label lblNavn = new Label("Navn:");
		this.add(lblNavn, 0, 2);
		this.add(txfNavn, 1, 2, 2, 1);

		Label lblDato = new Label("Dato:");
		this.add(lblDato, 0, 3);
		this.add(datePicker, 1, 3);
		datePicker.setMaxWidth(130);
		datePicker.setEditable(false);
		datePicker.setValue(LocalDate.now().plusDays(1));
		datePicker.setOnAction(event -> tidspunktAction());

		HBox tidspunktBox = new HBox(5);
		tidspunktBox.setAlignment(Pos.CENTER_LEFT);
		this.add(tidspunktBox, 2, 3);

		Label lblKolon = new Label(":");
		tidspunktBox.getChildren().addAll(cbbTime, lblKolon, cbbMinut);
		cbbTime.getItems().addAll("00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13",
				"14", "15", "16", "17", "18", "19", "20", "21", "22", "23");
		cbbTime.getSelectionModel().select(8);
		cbbTime.setOnAction(event -> tidspunktAction());
		cbbMinut.getItems().addAll("00", "05", "10", "15", "20", "25", "30", "35", "40", "45", "50", "55");
		cbbMinut.getSelectionModel().select(0);
		cbbMinut.setOnAction(event -> tidspunktAction());

		Label lblAntal = new Label("Antal:");
		this.add(lblAntal, 0, 4);
		this.add(txfAntal, 1, 4);
		txfAntal.setMaxWidth(80);
		txfAntal.setAlignment(Pos.BOTTOM_RIGHT);

		this.add(btnKunde, 0, 5, 2, 1);
		btnKunde.setOnAction(event -> kundeAction());

		VBox reserverBox = new VBox(10);
		reserverBox.setAlignment(Pos.CENTER_RIGHT);
		this.add(reserverBox, 0, 6, 3, 1);
		reserverBox.getChildren().addAll(lblOptaget, btnReserver);
		lblOptaget.setTextFill(Color.RED);
		btnReserver.setOnAction(event -> createRundvisningAction());

		Separator separator = new Separator();
		separator.setOrientation(Orientation.VERTICAL);
		this.add(separator, 3, 0, 1, 7);

		Label lblRundvisninger = new Label("Rundvisninger planlagt");
		lblRundvisninger.setFont(Font.font(20));
		this.add(lblRundvisninger, 5, 0);

		this.add(lvwRundvisninger, 5, 1, 1, 6);
		lvwRundvisninger.setMinWidth(350);

		VBox buttonBox = new VBox(10);
		buttonBox.setAlignment(Pos.BOTTOM_CENTER);
		this.add(buttonBox, 6, 1, 1, 6);

		buttonBox.getChildren().addAll(btnDelete, btnBetal);
		btnDelete.setMinWidth(72);
		btnDelete.setOnAction(event -> deleteAction());
		btnBetal.setMinWidth(72);
		btnBetal.setFont(Font.font(20));
		btnBetal.setOnAction(event -> betalAction());
	}

	// -------------------------------------------------------------------------

	/**
	 * Opretter en rundvisning med de indtastede oplysninger.
	 */
	private void createRundvisningAction() {
		try {
			Listepris listepris = cbbListepriser.getSelectionModel().getSelectedItem();
			int antal = Integer.parseInt(txfAntal.getText().trim());
			String beskrivelse = txfNavn.getText().trim();
			LocalDate dato = datePicker.getValue();
			int time = Integer.parseInt(cbbTime.getSelectionModel().getSelectedItem());
			int minut = Integer.parseInt(cbbMinut.getSelectionModel().getSelectedItem());
			LocalTime tid = LocalTime.of(time, minut);
			LocalDateTime tidspunkt = LocalDateTime.of(dato, tid);
			if (Controller.erTidspunktLedigt(dato, tid) && listepris != null) {
				if (beskrivelse.length() > 0) {
					if (kunde != null) {
						if (antal <= 75 && ((antal >= 15 && tid.plusMinutes(90).isBefore(LocalTime.of(16, 00)))
								|| (antal >= 20 && tid.plusMinutes(90).isAfter(LocalTime.of(16, 00))))) {
							Ordre ordre = Controller.createOrdre(kunde, LocalDate.now(),
									Controller.getRabatter().get(0), 0);
							Controller.setOrdreDatoSlut(ordre, dato);
							Controller.createOrdrelinjeRundvisning(ordre, listepris, antal, false, beskrivelse,
									tidspunkt);
							txfNavn.clear();
							txfAntal.clear();
							datePicker.setValue(LocalDate.now().plusDays(1));
							cbbTime.getSelectionModel().select(8);
							cbbMinut.getSelectionModel().select(0);
							Alert alert = new Alert(AlertType.INFORMATION);
							alert.setTitle("Reservation oprettet");
							alert.setHeaderText("Reservation oprettet");
							alert.setContentText("Reservationen er nu oprettet.");
							alert.show();
							loadRundvisninger();
						} else {
							Alert alert = new Alert(AlertType.ERROR);
							alert.setTitle("Fejl");
							alert.setHeaderText("For mange eller for få personer");
							alert.setContentText(
									"Før kl. 16 på hverdage og kl. 15 på fredage er minimum 15 personer og maksimum 75.\nEfter kl. 16 er minimum 20 personer.");
							alert.show();
						}
					} else {
						Alert alert = new Alert(AlertType.ERROR);
						alert.setTitle("Fejl");
						alert.setHeaderText("Ingen kunde valgt");
						alert.setContentText("En kunde skal vælges.");
						alert.show();
					}
				} else {
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("Fejl");
					alert.setHeaderText("Fejl");
					alert.setContentText("Du skal angive en beskrivelse.");
					alert.show();
				}
			}
		} catch (NumberFormatException ex) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Fejl");
			alert.setHeaderText("Fejl");
			alert.setContentText("Antal skal angives som tal.");
			alert.show();
		}
	}

	/**
	 * Tester, om tidspunktet allerede er optaget af en anden rundvisning, om
	 * rundvisningen vil slutte efter kl. 15 en fredag, eller om tidspunktet er i en
	 * weekend.
	 */
	private void tidspunktAction() {
		lblOptaget.setText("");
		LocalDate dato = datePicker.getValue();
		int time = Integer.parseInt(cbbTime.getSelectionModel().getSelectedItem());
		int minut = Integer.parseInt(cbbMinut.getSelectionModel().getSelectedItem());
		LocalTime tid = LocalTime.of(time, minut);
		if (!Controller.erTidspunktLedigt(dato, tid)) {
			lblOptaget.setText("Rundvisningen kan ikke arrangeres der");
		}
	}

	/**
	 * Henter de eksisterende rundvisninger.
	 */
	public void loadRundvisninger() {
		lvwRundvisninger.getItems().clear();
		ordrer.clear();
		for (Ordre ordre : Controller.getOrdrer()) {
			for (Ordrelinje ol : ordre.getOrdrelinjer()) {
				if (ol instanceof OrdrelinjeRundvisning) {
					OrdrelinjeRundvisning olr = (OrdrelinjeRundvisning) ol;
					ordrer.add(ordre);
					lvwRundvisninger.getItems().add(olr);
				}
			}
		}
		lvwRundvisninger.getItems().sort(null);
	}

	/**
	 * Henter prislister med rundvisning-listepriser på.
	 */
	private void loadPrislisterAction() {
		cbbPrislister.getItems().clear();
		for (Prisliste prisliste : Controller.getPrislister()) {
			if (prisliste.erProduktgruppenAnvendt(Produktgruppe.RUNDVISNING)) {
				cbbPrislister.getItems().add(prisliste);
			}
		}
	}

	/**
	 * Viser rundvisning-listepriserne fra den valgte prisliste.
	 */
	private void prislisteAction() {
		Prisliste prisliste = cbbPrislister.getSelectionModel().getSelectedItem();
		cbbListepriser.getItems().clear();
		if (prisliste != null) {
			cbbListepriser.getItems().addAll(prisliste.getListePriserFraProduktgruppe(Produktgruppe.RUNDVISNING));
		}
	}

	/**
	 * Sletter en rundvisnings ordre og dermed rundvisningen.
	 */
	private void deleteAction() {
		Ordrelinje ol = lvwRundvisninger.getSelectionModel().getSelectedItem();
		boolean found = false;
		if (ol != null) {
			Ordre ordre;
			Iterator<Ordre> iterator = ordrer.iterator();
			while (iterator.hasNext() && !found) {
				ordre = iterator.next();
				if (ordre.getOrdrelinjer().contains(ol)) {
					iterator.remove();
					Controller.deleteOrdre(ordre);
					found = true;
				}
			}
		}
	}

	/**
	 * Åbner betalingsvinduet for at betale den valgte rundvisning.
	 */
	private void betalAction() {
		Ordrelinje ol = lvwRundvisninger.getSelectionModel().getSelectedItem();
		boolean found = false;
		if (ol != null) {
			Ordre ordre;
			Iterator<Ordre> iterator = ordrer.iterator();
			while (iterator.hasNext() && !found) {
				ordre = iterator.next();
				if (ordre.getOrdrelinjer().contains(ol)) {
					BetalingWindow betalingWindow = new BetalingWindow("Betaling", null, "Rundvisning", ordre,
							(ArrayList<Ordrelinje>) ordre.getOrdrelinjer());
					betalingWindow.showAndWait();
					found = true;
				}
			}
		}
	}

	/**
	 * Åbner et vindue med mulighed for oprettelse af et Kunde-objekt eller valg af
	 * et eksisterende Kunde-objekt.
	 */
	private void kundeAction() {
		KundeWindow kundeWindow = new KundeWindow("Kunder", null);
		kundeWindow.showAndWait();
		kunde = kundeWindow.getKunde();
		kundeWindow.clear();
	}
}