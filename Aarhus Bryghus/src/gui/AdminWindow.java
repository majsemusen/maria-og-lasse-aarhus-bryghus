package gui;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class AdminWindow extends Stage {

	public AdminWindow(String title, Stage owner) {
		this.initOwner(owner);
		this.initStyle(StageStyle.UTILITY);
		this.initModality(Modality.APPLICATION_MODAL);
		this.setMinHeight(200);
		this.setMinWidth(300);
		this.setResizable(false);

		this.setTitle(title);
		GridPane pane = new GridPane();
		pane.setAlignment(Pos.CENTER);
		this.initContent(pane);

		Scene scene = new Scene(pane);
		this.setScene(scene);

		adminFunktionWindow = new AdminFunktionWindow("Aarhus Bryghus Datasystem - administrator", owner);
		brugerWindow = new BrugerWindow("Aarhus Bryghus Datasystem", owner);
	}

	// -------------------------------------------------------------------------

	private AdminFunktionWindow adminFunktionWindow;
	private BrugerWindow brugerWindow;

	private final Button btnAdminFunktion = new Button("Administrative\nfunktioner");
	private final Button btnBruger = new Button("Almindeligt salg");

	private final int buttonWidth = 160;

	private void initContent(GridPane pane) {
		// viser eller skjuler grid lines
		pane.setGridLinesVisible(false);

		// sætter padding for pane
		pane.setPadding(new Insets(20));
		// sætter horisontalt mellemrum mellem elementerne
		pane.setHgap(20);
		// sætter vertikalt mellemrum mellem elementerne
		pane.setVgap(10);

		VBox buttonBox = new VBox(20);
		pane.add(buttonBox, 0, 0);

		buttonBox.getChildren().add(btnAdminFunktion);
		btnAdminFunktion.setPrefWidth(buttonWidth);
		btnAdminFunktion.setAlignment(Pos.CENTER);
		btnAdminFunktion.setTextAlignment(TextAlignment.valueOf("CENTER"));
		btnAdminFunktion.setOnAction(event -> adminFunktionAction());
		btnAdminFunktion.setOnKeyPressed(event -> {
			if (event.getCode().equals(KeyCode.ENTER)) {
				btnAdminFunktion.fire();
			}
		});

		buttonBox.getChildren().add(btnBruger);
		btnBruger.setPrefWidth(buttonWidth);
		btnBruger.setAlignment(Pos.CENTER);
		btnBruger.setOnAction(event -> brugerAction());
		btnBruger.setOnKeyPressed(event -> {
			if (event.getCode().equals(KeyCode.ENTER)) {
				btnBruger.fire();
			}
		});
	}

	// -------------------------------------------------------------------------

	private void adminFunktionAction() {
		adminFunktionWindow.showAndWait();
	}

	private void brugerAction() {
		brugerWindow.showAndWait();
	}
}