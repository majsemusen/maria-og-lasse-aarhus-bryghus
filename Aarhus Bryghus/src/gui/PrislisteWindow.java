package gui;

import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import controller.Controller;
import controller.SalgssituationTagetException;
import model.Listepris;
import model.Prisliste;
import model.Produkt;
import model.Produktgruppe;

public class PrislisteWindow extends Stage {

	public PrislisteWindow(String title, Prisliste prisliste) {
		this.initStyle(StageStyle.UTILITY);
		this.initModality(Modality.APPLICATION_MODAL);
		this.setResizable(false);

		this.setTitle(title);
		GridPane pane = new GridPane();
		pane.setAlignment(Pos.CENTER);
		this.initContent(pane);

		Scene scene = new Scene(pane);
		this.setScene(scene);

		start(prisliste);
	}

	// -------------------------------------------------------------------------

	private Prisliste prisliste;
	private final TextField txfNavn = new TextField(), txfPris = new TextField();
	private final Label lblSalgssituation = new Label("Salgssituation:"), lblAvailable = new Label(),
			lblPris = new Label("Pris:"),
			lblPriser = new Label("Produkter i prislisten:"), lblProdukter = new Label("Alle tilgængelige produkter:");
	private final ListView<Produkt> lvwProdukter = new ListView<>();
	private final ListView<Produkt> lvwPriser = new ListView<>();
	private final ComboBox<Produktgruppe> cbbProduktgrupper = new ComboBox<>();
	private final CheckBox chbSimpel = new CheckBox();
	private final Button btnMoveLeft = new Button("<"), btnMoveRight = new Button(">"), btnOK = new Button("OK"),
			btnCancel = new Button("Annuller");
	private final TreeMap<Produkt, Double> listepriser = new TreeMap<>();

	private void initContent(GridPane pane) {
		// viser eller skjuler grid lines
		pane.setGridLinesVisible(false);

		// sætter padding for pane
		pane.setPadding(new Insets(20));
		// sætter horisontalt mellemrum mellem elementerne
		pane.setHgap(20);
		// sætter vertikalt mellemrum mellem elementerne
		pane.setVgap(10);

		// box i toppen
		HBox navnBox = new HBox(20);
		navnBox.setAlignment(Pos.CENTER);
		pane.add(navnBox, 0, 0, 3, 1);
		navnBox.getChildren().addAll(lblSalgssituation, txfNavn, lblAvailable);

		HBox simpelBox = new HBox(20);
		simpelBox.setAlignment(Pos.CENTER);
		simpelBox.setPadding(new Insets(0, 0, 20, 0));
		pane.add(simpelBox, 0, 1, 3, 1);
		simpelBox.getChildren().add(chbSimpel);
		chbSimpel.setText("Simpel (kan bruge klip, men kan ikke bruges til udlejning og rundvisning)");
		chbSimpel.setFont(Font.font(12));

		// venstre kolonne
		pane.add(lblPriser, 0, 2);
		pane.add(lvwPriser, 0, 3, 1, 2);
		lvwPriser.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

		// midterste kolonne
		VBox middleBox = new VBox(10);
		middleBox.setAlignment(Pos.CENTER);
		pane.add(middleBox, 1, 3, 1, 2);

		middleBox.getChildren().add(btnMoveLeft);
		btnMoveLeft.setMinWidth(60);
		btnMoveLeft.setOnAction(event -> moveLeftAction());

		middleBox.getChildren().add(btnMoveRight);
		btnMoveRight.setMinWidth(60);
		btnMoveRight.setOnAction(event -> moveRightAction());

		middleBox.getChildren().add(lblPris);
		lblPris.setPadding(new Insets(20, 0, 0, 0));
		middleBox.getChildren().add(txfPris);
		txfPris.setMaxWidth(60);
		txfPris.setAlignment(Pos.CENTER_RIGHT);

		// højre kolonne
		pane.add(lblProdukter, 2, 2);
		pane.add(cbbProduktgrupper, 2, 3);
		cbbProduktgrupper.setMinWidth(250);
		cbbProduktgrupper.getItems().setAll(Produktgruppe.values());
		ChangeListener<Produktgruppe> produktGruppeListener = (ov, oldProduktgruppe, newProduktgruppe) -> this
				.cbbAction();
		cbbProduktgrupper.getSelectionModel().selectedItemProperty().addListener(produktGruppeListener);
		pane.add(lvwProdukter, 2, 4);

		lvwProdukter.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

		// knapper til højre
		VBox buttonBox = new VBox(10);
		buttonBox.setAlignment(Pos.BOTTOM_RIGHT);
		pane.add(buttonBox, 3, 4);

		buttonBox.getChildren().add(btnOK);
		btnOK.setMinWidth(77);
		btnOK.setOnAction(event -> okAction());

		buttonBox.getChildren().add(btnCancel);
		btnCancel.setMinWidth(77);
		btnCancel.setOnAction(event -> cancelAction());
	}

	// -------------------------------------------------------------------------

	/**
	 * Starter oprettelsen af ny prisliste, hvis parameteren er null, og starter
	 * ellers redigeringen.
	 */
	private void start(Prisliste prisliste) {
		if (prisliste == null) {
			lvwProdukter.getItems().setAll(listepriser.keySet());
		} else {
			txfNavn.setText(prisliste.getSalgssituation());
			for (Listepris listepris : prisliste.getListepriser()) {
				listepriser.put(listepris.getProdukt(), listepris.getPris());
			}
			lvwPriser.getItems().setAll(listepriser.keySet());
		}
		this.prisliste = prisliste;
	}

	/**
	 * Viser de tilgængelige produkter fra produktgruppen, når en produktgruppe
	 * vælges.
	 */
	private void cbbAction() {
		lvwProdukter.getItems().clear();
		Produktgruppe produktgruppe = cbbProduktgrupper.getSelectionModel().getSelectedItem();
		lvwProdukter.getItems().setAll(Controller.getProdukterFraProduktgruppe(produktgruppe));
	}

	// Flytter fra lvwProdukt til lvwPriser.
	private void moveLeftAction() {
		Produkt produkt = lvwProdukter.getSelectionModel().getSelectedItem();
		try {
			double pris = Double.parseDouble(txfPris.getText().trim());
			listepriser.put(produkt, pris);
			txfPris.clear();
			lvwProdukter.getSelectionModel().clearSelection();
		} catch (NumberFormatException ex) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Fejl");
			alert.setHeaderText("Fejl");
			alert.setContentText("Du skal angive en pris for produktet.");
			alert.show();
		}
		lvwPriser.getItems().setAll(listepriser.keySet());
	}

	// Flytter fra lvwPriser til lvwProdukt.
	private void moveRightAction() {
		Produkt produkt = lvwPriser.getSelectionModel().getSelectedItem();
		if (produkt != null) {
			listepriser.remove(produkt);
			lvwPriser.getSelectionModel().clearSelection();
			lvwPriser.getItems().setAll(listepriser.keySet());
		}
	}

	/**
	 * Opretter en ny prisliste eller redigerer den eksisterende. Sætter navn og
	 * opdaterer prislistens listepriser.
	 */
	private void okAction() {
		String salgssituation = txfNavn.getText().trim();
		boolean simpel = false;
		if (chbSimpel.isSelected()) {
			simpel = true;
		}
		if (!salgssituation.equals("")) {
			if (prisliste == null) {
				try {
					prisliste = Controller.createPrisliste(salgssituation, simpel);
					for (Map.Entry<Produkt, Double> entry : listepriser.entrySet()) {
						Produkt produkt = entry.getKey();
						double pris = entry.getValue();
						Controller.setListepris(prisliste, produkt, pris);
					}
					Iterator<Listepris> iterator = prisliste.getListepriser().iterator();
					while (iterator.hasNext()) {
						if (!listepriser.keySet().contains(iterator.next().getProdukt())) {
							iterator.remove();
						}
					}
					close();
				} catch (SalgssituationTagetException ex) {
					lblAvailable.setText("Navn er allerede taget");
					lblAvailable.setTextFill(Color.RED);
				}
			} else {
				try {
					Controller.editPrisliste(prisliste, salgssituation, simpel);
					for (Map.Entry<Produkt, Double> entry : listepriser.entrySet()) {
						Produkt produkt = entry.getKey();
						double pris = entry.getValue();
						Controller.setListepris(prisliste, produkt, pris);
					}
					Iterator<Listepris> iterator = prisliste.getListepriser().iterator();
					while (iterator.hasNext()) {
						if (!listepriser.keySet().contains(iterator.next().getProdukt())) {
							iterator.remove();
						}
					}
					close();
				} catch (SalgssituationTagetException ex) {
					lblAvailable.setText("Navn er allerede taget");
					lblAvailable.setTextFill(Color.RED);
				}
			}

		} else {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Fejl");
			alert.setHeaderText("Fejl");
			alert.setContentText("Du skal angive prislistens salgssituation.");
			alert.show();
		}
	}

	/**
	 * Lukker vinduet, hvis knappen "Cancel" trykkes på.
	 */
	private void cancelAction() {
		close();
	}
}