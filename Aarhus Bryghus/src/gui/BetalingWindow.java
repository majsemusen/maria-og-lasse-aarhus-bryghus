package gui;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import controller.Controller;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.AftaltPris;
import model.Betalingsmetode;
import model.Kunde;
import model.Listepris;
import model.Ordre;
import model.Ordrelinje;
import model.OrdrelinjeSampak;
import model.ProcentRabat;
import model.Produktgruppe;
import model.Rabat;
import model.Sampakning;

public class BetalingWindow extends Stage {

	public BetalingWindow(String title, Stage owner, String brug, Ordre ordre, ArrayList<Ordrelinje> tempOrdrelinjer) {
		this.initOwner(owner);
		this.initStyle(StageStyle.UTILITY);
		this.initModality(Modality.APPLICATION_MODAL);
		this.setMinHeight(200);
		this.setMinWidth(300);
		this.setResizable(false);

		this.setTitle(title);
		GridPane pane = new GridPane();
		pane.setAlignment(Pos.CENTER);
		this.initContent(pane);

		Scene scene = new Scene(pane);
		this.setScene(scene);

		this.ordre = ordre;
		this.tempOrdrelinjer = tempOrdrelinjer;
		this.brug = brug;
		kundeAction();
		start();
	}

	// -------------------------------------------------------------------------

	private StackPane stackPane = new StackPane(), stackHøjrePane = new StackPane();
	private GridPane betalingPane = new GridPane(), kvitteringPane = new GridPane(), klipPane = new GridPane(),
			tomPane = new GridPane(), udlejningPane = new GridPane();

	private final TextField txfTotal = new TextField(), txfRabat = new TextField(), txfKlipBrug = new TextField(),
			txfVareMedKlip = new TextField(), txfTotalKlip = new TextField(), txfUdlejAntal = new TextField(),
			txfTotalBetalesSenere = new TextField(), txfDankort = new TextField(), txfKontant = new TextField(),
			txfMobilePay = new TextField(), txfResterende = new TextField();
	private final TextArea txaOrdrelinjer = new TextArea();
	private final ListView<Ordrelinje> lvwOrdrelinjer = new ListView<>(), lvwOrdrelinjerKlip = new ListView<>(),
			lvwOrdrelinjerUdlej = new ListView<>();
	private final ComboBox<Rabat> cbbRabat = new ComboBox<Rabat>();
	private final Button btnKlipPlus = new Button("+"), btnKlipMinus = new Button("-"), btnVarePlus = new Button("+"),
			btnVareMinus = new Button("-"), btnOK = new Button("\u2713"), btnUdlejPlus = new Button("+"),
			btnUdlejMinus = new Button("-"), btnUdlejOK = new Button("\u2713"),
			btnDeleteOrdrelinje = new Button("Slet"), btnFortrydKlip = new Button("Fortryd"),
			btnFortrydBetalSenere = new Button("Fortryd"), btnDankortFuldt = new Button("Max"),
			btnKontantFuldt = new Button("Max"), btnMobilePayFuldt = new Button("Max"), btnBetal = new Button("Betal"),
			btnPrint = new Button("Print kvittering");
	private final Label lblTotalPris = new Label(), lblRabatEnhed = new Label(),
			lblPrisKlip = new Label("Fuld pris i klip\npr. stk.:"), lblBetalNu = new Label("Betal senere:"),
			lblDato = new Label(), lblUdlejedeVarer = new Label("Udlejede varer:"),
			lblTotalBetalesSenere = new Label("Betales senere:");
	private final VBox kundeBox = new VBox(10);

	private final Ordre ordre;
	private String brug;
	// tempOrdrelinjer opbevarer ordrelinjerne, som ikke er oprettet på ordren endnu
	// tempOrdrelinjerUdlej opbevarer de ordrelinjer, som ikke skal betales, fordi
	// de skal udlejes
	private final ArrayList<Ordrelinje> tempOrdrelinjer, tempOrdrelinjerUdlej = new ArrayList<>(),
			tempOrdrelinjerBetalt = new ArrayList<>();
	// tempOrdrelinjerKlip opbevarer ordrelinjer og deres pris i klip
	private final TreeMap<Ordrelinje, Integer> tempOrdrelinjerKlip = new TreeMap<>();

	private void initContent(GridPane pane) {
		// viser eller skjuler grid lines
		pane.setGridLinesVisible(false);

		// sætter padding for pane
		pane.setPadding(new Insets(20));
		// sætter horisontalt mellemrum mellem elementerne
		pane.setHgap(20);
		// sætter vertikalt mellemrum mellem elementerne
		pane.setVgap(10);

		this.setOnCloseRequest(event -> resetOnClose());

		pane.add(stackPane, 0, 0);
		stackPane.getChildren().addAll(kvitteringPane, betalingPane);
		kvitteringPane.setAlignment(Pos.CENTER);
		betalingPane.setAlignment(Pos.CENTER);

		int txfWidth = 80;

		// -------------------------------------------------------------------------

		// BETALINGPANE

		betalingPane.setGridLinesVisible(false);
		betalingPane.setPadding(new Insets(20));
		betalingPane.setHgap(20);
		betalingPane.setVgap(10);
		betalingPane.setBackground(new Background(new BackgroundFill(Color.WHITESMOKE, null, null)));

		// KOLONNE 0 + 1
		Label lblOrdrelinjer = new Label("Ordre:");
		lblOrdrelinjer.setFont(Font.font(20));
		betalingPane.add(lblOrdrelinjer, 0, 0);

		betalingPane.add(lvwOrdrelinjer, 0, 1, 2, 1);
		lvwOrdrelinjer.setMinSize(400, 442);
		ChangeListener<Ordrelinje> ordrelinjeListener = (ov, oldPrisliste, newPrisliste) -> this.ordrelinjeAction();
		lvwOrdrelinjer.getSelectionModel().selectedItemProperty().addListener(ordrelinjeListener);

//		betalingPane.add(cbbRabat, 0, 2, 2, 1);
		cbbRabat.setOnAction(event -> rabatAction());
		cbbRabat.getItems().setAll(Controller.getRabatter());
		ChangeListener<Rabat> rabatListener = (ov, oldProduktgruppe, newProduktgruppe) -> this.rabatAction();
		cbbRabat.getSelectionModel().selectedItemProperty().addListener(rabatListener);

//		betalingPane.add(txfRabat, 0, 3);
		txfRabat.setMaxWidth(txfWidth);
		txfRabat.setAlignment(Pos.CENTER_RIGHT);
		txfRabat.setDisable(true);
		ChangeListener<String> txfRabatListener = (ov, oldString, newString) -> this.totalAction();
		txfRabat.textProperty().addListener(txfRabatListener);

//		betalingPane.add(lblRabatEnhed, 1, 3);

		Label lblTotal = new Label("Total:");
		lblTotal.setPadding(new Insets(10, 0, 0, 0));
		betalingPane.add(lblTotal, 0, 4);

		betalingPane.add(txfTotal, 0, 5);
		txfTotal.setMaxWidth(txfWidth);
		txfTotal.setAlignment(Pos.CENTER_RIGHT);
		txfTotal.setEditable(false);

		Label lblKr = new Label("kr.");
		betalingPane.add(lblKr, 1, 5);

		HBox deleteOrdrelinjeBox = new HBox(10);
		deleteOrdrelinjeBox.setAlignment(Pos.TOP_RIGHT);
		betalingPane.add(deleteOrdrelinjeBox, 1, 2);
		deleteOrdrelinjeBox.getChildren().add(btnDeleteOrdrelinje);
		btnDeleteOrdrelinje.setOnAction(event -> sletOrdrelinjeAction());

		betalingPane.add(stackHøjrePane, 2, 1, 3, 3);
		stackHøjrePane.getChildren().addAll(udlejningPane, klipPane, tomPane);
		tomPane.setBackground(new Background(new BackgroundFill(Color.WHITESMOKE, null, null)));

		// -------------------------------------------------------------------------

		// KLIP

		// Klip kolonne 0
		klipPane.setGridLinesVisible(false);
		klipPane.setHgap(20);
		klipPane.setVgap(10);
		klipPane.setBackground(new Background(new BackgroundFill(Color.WHITESMOKE, null, null)));

		VBox middleBox = new VBox(5);
		middleBox.setAlignment(Pos.CENTER);
		klipPane.add(middleBox, 0, 1, 1, 2);

		middleBox.getChildren().add(lblPrisKlip);
		lblPrisKlip.setAlignment(Pos.CENTER);
		lblPrisKlip.setPadding(new Insets(0, 0, 10, 0));

		Label lblKlipBrug = new Label("Klip bruges:");
		middleBox.getChildren().add(lblKlipBrug);

		middleBox.getChildren().add(txfKlipBrug);
		txfKlipBrug.setMaxWidth(72);
		txfKlipBrug.setAlignment(Pos.CENTER_RIGHT);
		txfKlipBrug.setEditable(false);
		txfKlipBrug.setText("1");

		HBox klipBrugBox = new HBox(10);
		klipBrugBox.setAlignment(Pos.CENTER);
		klipBrugBox.setPadding(new Insets(0, 0, 10, 0));
		middleBox.getChildren().add(klipBrugBox);

		klipBrugBox.getChildren().addAll(btnKlipPlus, btnKlipMinus);
		btnKlipPlus.setMinWidth(31);
		btnKlipPlus.setOnAction(event -> valueKlipAction(btnKlipPlus));
		btnKlipMinus.setMinWidth(31);
		btnKlipMinus.setOnAction(event -> valueKlipAction(btnKlipMinus));

		Label lblVareMedKlip = new Label("Antal af varen:");
		lblVareMedKlip.setPadding(new Insets(10, 0, 0, 0));
		middleBox.getChildren().add(lblVareMedKlip);

		middleBox.getChildren().add(txfVareMedKlip);
		txfVareMedKlip.setMaxWidth(72);
		txfVareMedKlip.setAlignment(Pos.CENTER_RIGHT);
		txfVareMedKlip.setEditable(false);
		txfVareMedKlip.setText("1");

		HBox totalKlipBox = new HBox(10);
		totalKlipBox.setAlignment(Pos.CENTER);
		totalKlipBox.setPadding(new Insets(0, 0, 10, 0));
		middleBox.getChildren().add(totalKlipBox);

		totalKlipBox.getChildren().addAll(btnVarePlus, btnVareMinus);
		btnVarePlus.setMinWidth(31);
		btnVarePlus.setOnAction(event -> valueAntalAction(btnVarePlus));
		btnVareMinus.setMinWidth(31);
		btnVareMinus.setOnAction(event -> valueAntalAction(btnVareMinus));

		middleBox.getChildren().add(btnOK);
		btnOK.setMinWidth(36);
		btnOK.setOnAction(event -> okKlipAction());

		// Klip kolonne 1
		Label lblKlip = new Label("Betales med klip:");
		klipPane.add(lblKlip, 1, 1);

		klipPane.add(lvwOrdrelinjerKlip, 1, 2, 2, 1);
		lvwOrdrelinjerKlip.setMinWidth(400);

		Label lblTotalKlip = new Label("Antal klip:");
		klipPane.add(lblTotalKlip, 1, 3);

		klipPane.add(txfTotalKlip, 1, 4);
		txfTotalKlip.setMaxWidth(txfWidth);
		txfTotalKlip.setAlignment(Pos.CENTER_RIGHT);
		txfTotalKlip.setText("0");
		txfTotalKlip.setEditable(false);

		HBox fortrydKlipBox = new HBox(10);
		fortrydKlipBox.setAlignment(Pos.TOP_RIGHT);
		klipPane.add(fortrydKlipBox, 2, 3);
		fortrydKlipBox.getChildren().add(btnFortrydKlip);
		btnFortrydKlip.setOnAction(event -> fortrydKlipAction());

		// -------------------------------------------------------------------------

		// UDLEJNINGPANE

		udlejningPane.setGridLinesVisible(false);
		udlejningPane.setHgap(20);
		udlejningPane.setVgap(10);
		udlejningPane.setAlignment(Pos.BOTTOM_RIGHT);
		udlejningPane.setBackground(new Background(new BackgroundFill(Color.WHITESMOKE, null, null)));

		// Udlejning kolonne 0
		VBox middleUdlejBox = new VBox(5);
		middleUdlejBox.setAlignment(Pos.CENTER);
		udlejningPane.add(middleUdlejBox, 0, 1, 1, 2);

		middleUdlejBox.getChildren().add(lblBetalNu);
		lblBetalNu.setAlignment(Pos.CENTER);

		middleUdlejBox.getChildren().add(txfUdlejAntal);
		txfUdlejAntal.setMaxWidth(72);
		txfUdlejAntal.setAlignment(Pos.CENTER_RIGHT);
		txfUdlejAntal.setEditable(false);
		txfUdlejAntal.setText("1");

		HBox udlejBox = new HBox(10);
		udlejBox.setAlignment(Pos.CENTER);
		udlejBox.setPadding(new Insets(0, 0, 10, 0));
		middleUdlejBox.getChildren().add(udlejBox);

		udlejBox.getChildren().addAll(btnUdlejPlus, btnUdlejMinus);
		btnUdlejPlus.setMinWidth(31);
		btnUdlejPlus.setOnAction(event -> valueUdlejAction(btnUdlejPlus));
		btnUdlejMinus.setMinWidth(31);
		btnUdlejMinus.setOnAction(event -> valueUdlejAction(btnUdlejMinus));

		middleUdlejBox.getChildren().add(btnUdlejOK);
		btnUdlejOK.setMinWidth(36);
		btnUdlejOK.setOnAction(event -> okUdlejAction());

		// Udlejning kolonne 1

		udlejningPane.add(lblUdlejedeVarer, 1, 1);

		udlejningPane.add(lvwOrdrelinjerUdlej, 1, 2, 2, 1);
		lvwOrdrelinjerUdlej.setMinWidth(400);

		udlejningPane.add(lblTotalBetalesSenere, 1, 3);

		udlejningPane.add(txfTotalBetalesSenere, 1, 4);
		txfTotalBetalesSenere.setMaxWidth(txfWidth);
		txfTotalBetalesSenere.setAlignment(Pos.CENTER_RIGHT);
		txfTotalBetalesSenere.setText("0,00");
		txfTotalBetalesSenere.setEditable(false);

		HBox fortrydBetalSenereBox = new HBox(10);
		fortrydBetalSenereBox.setAlignment(Pos.TOP_RIGHT);
		udlejningPane.add(fortrydBetalSenereBox, 2, 3);
		fortrydBetalSenereBox.getChildren().add(btnFortrydBetalSenere);
		btnFortrydBetalSenere.setOnAction(event -> fortrydUdlejAction());

		// -------------------------------------------------------------------------

		// BUNDEN
		HBox betalingBox = new HBox(40);
		betalingPane.add(betalingBox, 0, 6, 3, 1);

		ChangeListener<String> txfListener = (ov, oldString, newString) -> this.resterendeBeløbAction();

		VBox dankortBox = new VBox(5);
		Label lblDankort = new Label("Dankort:");
		dankortBox.getChildren().addAll(lblDankort, txfDankort, btnDankortFuldt);
		txfDankort.setMaxWidth(txfWidth);
		txfDankort.setAlignment(Pos.CENTER_RIGHT);
		txfDankort.setText("0,00");
		txfDankort.textProperty().addListener(txfListener);
		btnDankortFuldt.setOnAction(event -> fuldtBeløbAction(btnDankortFuldt));

		VBox kontantBox = new VBox(5);
		Label lblKontant = new Label("Kontant:");
		kontantBox.getChildren().addAll(lblKontant, txfKontant, btnKontantFuldt);
		txfKontant.setMaxWidth(txfWidth);
		txfKontant.setAlignment(Pos.CENTER_RIGHT);
		txfKontant.setText("0,00");
		txfKontant.textProperty().addListener(txfListener);
		btnKontantFuldt.setOnAction(event -> fuldtBeløbAction(btnKontantFuldt));

		VBox mobilePayBox = new VBox(5);
		Label lblMobilePay = new Label("MobilePay:");
		mobilePayBox.getChildren().addAll(lblMobilePay, txfMobilePay, btnMobilePayFuldt);
		txfMobilePay.setMaxWidth(txfWidth);
		txfMobilePay.setAlignment(Pos.CENTER_RIGHT);
		txfMobilePay.setText("0,00");
		txfMobilePay.textProperty().addListener(txfListener);
		btnMobilePayFuldt.setOnAction(event -> fuldtBeløbAction(btnMobilePayFuldt));

		betalingBox.getChildren().addAll(dankortBox, kontantBox, mobilePayBox);

		VBox resterendeBox = new VBox(5);
		betalingPane.add(resterendeBox, 3, 6);
		Label lblResterende = new Label("Resterende beløb:");
		resterendeBox.getChildren().addAll(lblResterende, txfResterende);
		txfResterende.setMaxWidth(txfWidth);
		txfResterende.setAlignment(Pos.CENTER_RIGHT);
		txfResterende.setText("0,00");
		txfResterende.setEditable(false);

		HBox betalBox = new HBox(10);
		betalBox.setAlignment(Pos.BOTTOM_RIGHT);
		betalingPane.add(betalBox, 4, 6);
		betalBox.getChildren().add(btnBetal);
		btnBetal.setFont(Font.font(20));

		btnBetal.setOnAction(event -> betalAction());

		// -------------------------------------------------------------------------

		// UDSKRIFTPANE

		kvitteringPane.setGridLinesVisible(false);
		kvitteringPane.setPadding(new Insets(20));
		kvitteringPane.setHgap(20);
		kvitteringPane.setVgap(30);
		kvitteringPane.setBackground(new Background(new BackgroundFill(Color.WHITESMOKE, null, null)));

		GridPane udskriftPane = new GridPane();
		kvitteringPane.add(udskriftPane, 0, 0);

		HBox printBox = new HBox(10);
		printBox.setAlignment(Pos.BOTTOM_CENTER);
		kvitteringPane.add(printBox, 0, 1);
		printBox.getChildren().add(btnPrint);
		btnPrint.setOnAction(event -> printAction());

		// ---

		udskriftPane.setGridLinesVisible(false);
		udskriftPane.setPadding(new Insets(20));
		udskriftPane.setHgap(20);
		udskriftPane.setVgap(20);
		udskriftPane.setStyle("-fx-background-color: white");
		udskriftPane.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, null, null)));

		kundeBox.setAlignment(Pos.TOP_LEFT);
		udskriftPane.add(kundeBox, 0, 0);

		VBox abBox = new VBox(10);
		abBox.setAlignment(Pos.TOP_RIGHT);
		udskriftPane.add(abBox, 1, 0);
		Label lblAB = new Label("Aarhus Bryghus A/S");
		lblAB.setFont(Font.font(null, FontWeight.BOLD, -1));
		Label lblAdresse1 = new Label("Gunnar Clausens Vej 26");
		Label lblAdresse2 = new Label("8260 Viby J");
		Label lblCVR = new Label("\nCVR-nr 28697236");
		abBox.getChildren().addAll(lblAB, lblAdresse1, lblAdresse2, lblCVR);

		udskriftPane.add(lblDato, 0, 1);

		udskriftPane.add(txaOrdrelinjer, 0, 2, 2, 1);
		txaOrdrelinjer.setEditable(false);
		txaOrdrelinjer.setFocusTraversable(false);
		txaOrdrelinjer.setStyle(
				"-fx-background-color:transparent; -fx-text-box-border: transparent; -fx-focus-color: transparent; -fx-faint-focus-color: transparent;");

		HBox totalBox = new HBox(10);
		totalBox.setAlignment(Pos.BOTTOM_RIGHT);
		udskriftPane.add(totalBox, 1, 3);
		Label lblKvitteringTotal = new Label("Total:");
		lblKvitteringTotal.setFont(Font.font(null, FontWeight.BOLD, -1));
		totalBox.getChildren().addAll(lblKvitteringTotal, lblTotalPris);
	}

	// -------------------------------------------------------------------------

	/**
	 * Køres ved start og opsætter vinduet korrekt efter den brug, der er
	 * specificeret ved oprettelsen af vinduet.
	 */
	private void start() {
		if (brug.equals("Salg") || brug.equals("Rundvisning")) {
			betalingPane.add(cbbRabat, 0, 2, 2, 1);
			betalingPane.add(txfRabat, 0, 3);
			betalingPane.add(lblRabatEnhed, 1, 3);
			tomPane.toFront();
		} else if (brug.equals("Simpel")) {
			betalingPane.add(cbbRabat, 0, 2, 2, 1);
			betalingPane.add(txfRabat, 0, 3);
			betalingPane.add(lblRabatEnhed, 1, 3);
			disableKlip();
			klipPane.toFront();
		} else if (brug.equals("BegyndUdlejning")) {
			Iterator<Ordrelinje> iterator = tempOrdrelinjer.iterator();
			while (iterator.hasNext()) {
				Ordrelinje ordrelinje = iterator.next();
				Produktgruppe produktgruppe = ordrelinje.getListepris().getProdukt().getProduktgruppe();
				if (!produktgruppe.equals(Produktgruppe.PANT) && !produktgruppe.equals(Produktgruppe.LEVERING)) {
					tempOrdrelinjerUdlej.add(ordrelinje);
					iterator.remove();
				}
			}
			lvwOrdrelinjerUdlej.getItems().setAll(tempOrdrelinjerUdlej);
			cbbRabat.setDisable(true);
			totalUdlejning();
			udlejningPane.toFront();
		} else if (brug.equals("AfslutUdlejning")) {
			lblBetalNu.setText("Tilbageleveret:");
			lblUdlejedeVarer.setText("Varer modregnet i totalpris:");
			lblTotalBetalesSenere.setText("Modregnes:");

			Iterator<Ordrelinje> iterator = tempOrdrelinjer.iterator();
			while (iterator.hasNext()) {
				Ordrelinje ordrelinje = iterator.next();
				Produktgruppe produktgruppe = ordrelinje.getListepris().getProdukt().getProduktgruppe();
				if (produktgruppe.equals(Produktgruppe.PANT)) {
					tempOrdrelinjerUdlej.add(ordrelinje);
					iterator.remove();
				} else if (ordrelinje.isBetalt()) {
					tempOrdrelinjerBetalt.add(ordrelinje);
					iterator.remove();
				}
			}
			lvwOrdrelinjerUdlej.getItems().setAll(tempOrdrelinjerUdlej);
			btnDeleteOrdrelinje.setDisable(true);
			totalUdlejning();
			udlejningPane.toFront();
		}
		lblDato.setText("Dato: " + ordre.getDatoStart());
		lvwOrdrelinjer.getItems().setAll(tempOrdrelinjer);
		totalInitialAction();
	}

	/**
	 * Registrerer skift af rabatten i cbbRabat og sætter labels efter det. Disabler
	 * rabatfeltet, hvis IngenRabat er valgt.
	 */
	private void rabatAction() {
		Rabat tempRabat = cbbRabat.getSelectionModel().getSelectedItem();
		txfRabat.clear();
		if (tempRabat instanceof ProcentRabat) {
			lblRabatEnhed.setText("%");
			txfRabat.setDisable(false);
			Controller.setOrdreRabat(ordre, tempRabat);
			totalInitialAction();
		} else if (tempRabat instanceof AftaltPris) {
			lblRabatEnhed.setText("kr.");
			txfRabat.setDisable(false);
			Controller.setOrdreRabat(ordre, tempRabat);
			totalInitialAction();
		} else {
			lblRabatEnhed.setText("");
			txfRabat.setDisable(true);
			Controller.setOrdreRabat(ordre, tempRabat);
			totalInitialAction();
		}
	}

	/**
	 * Registrerer, når en ny ordrelinje vælges. Hvis klip kan bruges, aktiveres de
	 * relevante knapper og felter.
	 */
	private void ordrelinjeAction() {
		Ordrelinje ordrelinje = lvwOrdrelinjer.getSelectionModel().getSelectedItem();
		if (ordrelinje != null) {
			if (Controller.kanKlipBruges(ordrelinje)) {
				Listepris listepris = ordrelinje.getListepris();
				enableKlip(listepris);
			} else {
				disableKlip();
			}
		}
	}

	/**
	 * Ændrer på antallet af klip, der ønskes brugt til den valgte listepris.
	 * Antallet kan dog ikke gå over prisen for det valgte antal af listeprisen
	 * eller under 1.
	 */
	private void valueKlipAction(Button button) {
		{
			try {
				int antalKlip = Integer.parseInt(txfKlipBrug.getText().trim());
				int antalVarer = Integer.parseInt(txfVareMedKlip.getText().trim());
				Ordrelinje ordrelinje = lvwOrdrelinjer.getSelectionModel().getSelectedItem();
				if (ordrelinje != null) {
					Listepris listepris = ordrelinje.getListepris();
					if (button.equals(btnKlipPlus) && antalKlip < (Controller.prisIKlip(listepris) * antalVarer)) {
						antalKlip++;
					} else if (button.equals(btnKlipMinus) && antalKlip > 1) {
						antalKlip--;
					}
					txfKlipBrug.setText(antalKlip + "");
				}
			} catch (NumberFormatException ex) {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Fejl");
				alert.setHeaderText("Fejl");
				alert.setContentText("Antallet skal være et tal.");
				alert.show();
			}
		}
	}

	/**
	 * Ændrer på antallet af listeprisens varer, der ønskes købt med klip. Sætter
	 * også antallet af klip til det antal, der skal bruges for at købe det valgte
	 * antal af listeprisen.
	 */
	private void valueAntalAction(Button button) {
		{
			try {
				int antalVarer = Integer.parseInt(txfVareMedKlip.getText().trim());
				Ordrelinje ordrelinje = lvwOrdrelinjer.getSelectionModel().getSelectedItem();
				if (ordrelinje != null) {
					if (button.equals(btnVarePlus) && antalVarer < ordrelinje.getAntal()) {
						antalVarer++;
					} else if (button.equals(btnVareMinus) && antalVarer > 1) {
						antalVarer--;
					}
					txfVareMedKlip.setText(antalVarer + "");
					Listepris listepris = ordrelinje.getListepris();
					int antalKlip = Controller.prisIKlip(listepris) * antalVarer;
					txfKlipBrug.setText(antalKlip + "");
				}
			} catch (NumberFormatException ex) {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Fejl");
				alert.setHeaderText("Fejl");
				alert.setContentText("Antallet skal være et tal.");
				alert.show();
			}
		}
	}

	/**
	 * Accepterer antallet af listeprisens varer og det antal klip, der ønskes
	 * brugt. Flytter det valgte antal af listeprisen fra tempOrdrelinjer til
	 * tempOrdrelinjerKlip.
	 */
	private void okKlipAction() {
		Ordrelinje varerVenstreList = lvwOrdrelinjer.getSelectionModel().getSelectedItem();
		if (varerVenstreList != null) {
			int tempAntalVenstre = varerVenstreList.getAntal();
			int antalVarer = Integer.parseInt(txfVareMedKlip.getText());
			int antalKlip = Integer.parseInt(txfKlipBrug.getText().trim());
			Iterator<Ordrelinje> iterator = tempOrdrelinjerKlip.keySet().iterator();
			if (tempAntalVenstre > antalVarer) {
				boolean updated = false;
				while (iterator.hasNext() && updated == false) {
					Ordrelinje tempOrdrelinje = iterator.next();
					if (varerVenstreList.getListepris().equals(tempOrdrelinje.getListepris())) {
						int tempAntal = tempOrdrelinje.getAntal();
						Controller.setOrdrelinjeAntal(tempOrdrelinje, tempAntal + antalVarer);
						Controller.setOrdrelinjePris(tempOrdrelinje, tempOrdrelinje.getPris()
								+ Controller.resterendePrisEfterKlip(tempOrdrelinje.getListepris(), antalVarer,
										antalKlip));
						Controller.setOrdrelinjeAntal(varerVenstreList, tempAntalVenstre - antalVarer);
						tempOrdrelinjerKlip.put(tempOrdrelinje, tempOrdrelinjerKlip.get(tempOrdrelinje) + antalKlip);
						updated = true;
					}
				}
				if (updated == false) {
					Ordrelinje tempVenstre = new Ordrelinje(varerVenstreList.getListepris(), antalVarer, false);
					Controller.setOrdrelinjePris(tempVenstre,
							Controller.resterendePrisEfterKlip(tempVenstre.getListepris(), antalVarer, antalKlip));
					tempOrdrelinjerKlip.put(tempVenstre, antalKlip);
					Controller.setOrdrelinjeAntal(varerVenstreList, tempAntalVenstre - antalVarer);
				}
			} else if (tempAntalVenstre - antalVarer == 0) {
				boolean updated = false;
				while (iterator.hasNext() && updated == false) {
					Ordrelinje tempOrdrelinje = iterator.next();
					if (varerVenstreList.getListepris().equals(tempOrdrelinje.getListepris())) {
						int tempAntal = tempOrdrelinje.getAntal();
						Controller.setOrdrelinjeAntal(tempOrdrelinje, tempAntal + antalVarer);
						Controller.setOrdrelinjePris(tempOrdrelinje, tempOrdrelinje.getPris()
								+ Controller.resterendePrisEfterKlip(tempOrdrelinje.getListepris(), antalVarer,
										antalKlip));
						Controller.setOrdrelinjeAntal(varerVenstreList, tempAntalVenstre - antalVarer);
						tempOrdrelinjerKlip.put(tempOrdrelinje, tempOrdrelinjerKlip.get(tempOrdrelinje) + antalKlip);
						updated = true;
					}
				}
				if (updated == false) {
					Ordrelinje tempVenstre = new Ordrelinje(varerVenstreList.getListepris(), antalVarer, false);
					Controller.setOrdrelinjePris(tempVenstre,
							Controller.resterendePrisEfterKlip(tempVenstre.getListepris(), antalVarer, antalKlip));
					tempOrdrelinjerKlip.put(tempVenstre, antalKlip);
					tempOrdrelinjer.remove(varerVenstreList);
				}
			} else {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Fejl");
				alert.setHeaderText("Negativt resultat");
				alert.setContentText("Mængde valgt er højere end mængden af varen, der eksisterer på listen.");
				alert.show();
			}
			totalKlip();
			disableKlip();
			lvwOrdrelinjer.getItems().setAll(tempOrdrelinjer);
			lvwOrdrelinjerKlip.getItems().setAll(tempOrdrelinjerKlip.keySet());
			totalAction();
		}
	}

	/**
	 * Fortryder brugen af klip på den valgte vare og flytter den tilbage til
	 * tempOrdrelinjer fra tempOrdrelinjerKlip. Resetter listeprisens pris.
	 */
	private void fortrydKlipAction() {
		Ordrelinje oKlip = lvwOrdrelinjerKlip.getSelectionModel().getSelectedItem();
		if (oKlip != null) {
			Iterator<Ordrelinje> iterator = tempOrdrelinjer.iterator();
			boolean found = false;
			while (iterator.hasNext() && !found) {
				Ordrelinje tempOrdrelinje = iterator.next();
				if (tempOrdrelinje.getListepris().equals(oKlip.getListepris())) {
					int tempAntal = tempOrdrelinje.getAntal();
					Controller.setOrdrelinjeAntal(tempOrdrelinje, tempAntal + oKlip.getAntal());
					tempOrdrelinjerKlip.remove(oKlip);
					lvwOrdrelinjer.getItems().setAll(tempOrdrelinjer);
					lvwOrdrelinjerKlip.getItems().setAll(tempOrdrelinjerKlip.keySet());
					totalKlip();
					totalAction();
					found = true;
				}
			}
			if (!found) {
				oKlip.resetPris();
				tempOrdrelinjer.add(oKlip);
				tempOrdrelinjerKlip.remove(oKlip);
				lvwOrdrelinjer.getItems().setAll(tempOrdrelinjer);
				lvwOrdrelinjerKlip.getItems().setAll(tempOrdrelinjerKlip.keySet());
				totalKlip();
				totalAction();
			}
		}
	}

	/**
	 * Beregner det totale antal klip, der skal bruges til de varer, der er på
	 * listen tempOrdrelinjerKlip.
	 */
	private void totalKlip() {
		int totalKlip = 0;
		Iterator<Ordrelinje> iterator = tempOrdrelinjerKlip.keySet().iterator();
		while (iterator.hasNext()) {
			Ordrelinje tempOrdrelinje = iterator.next();
			totalKlip += tempOrdrelinjerKlip.get(tempOrdrelinje);
		}
		txfTotalKlip.setText(totalKlip + "");
	}

	/**
	 * Enabler de relevante knapper og felter til brug af klip.
	 */
	private void enableKlip(Listepris listepris) {
		lblPrisKlip.setText("Fuld pris i klip\npr. stk.: " + Controller.prisIKlip(listepris));
		txfKlipBrug.setText(Controller.prisIKlip(listepris) + "");
		txfVareMedKlip.setText("1");
		btnKlipPlus.setDisable(false);
		btnKlipMinus.setDisable(false);
		btnVarePlus.setDisable(false);
		btnVareMinus.setDisable(false);
		btnOK.setDisable(false);
	}

	/**
	 * Disabler de relevante knapper og felter til brug af klip.
	 */
	private void disableKlip() {
		lblPrisKlip.setText("Fuld pris i klip\npr. stk.: -");
		txfKlipBrug.setText("0");
		txfVareMedKlip.setText("0");
		btnKlipPlus.setDisable(true);
		btnKlipMinus.setDisable(true);
		btnVarePlus.setDisable(true);
		btnVareMinus.setDisable(true);
		btnOK.setDisable(true);
	}

	/**
	 * Sletter den valgte ordrelinje fra tempOrdrelinjer, og den medregnes derfor
	 * ikke mere.
	 */
	private void sletOrdrelinjeAction() {
		Ordrelinje o = lvwOrdrelinjer.getSelectionModel().getSelectedItem();
		tempOrdrelinjer.remove(o);
		lvwOrdrelinjer.getItems().setAll(tempOrdrelinjer);
		if (lvwOrdrelinjer.getItems().isEmpty()) {
			totalInitialAction();
			txfRabat.clear();
		} else {
			totalAction();
		}
	}

	/**
	 * Ændrer på antallet af listeprisens varer, der ønskes udlejet og dermed betalt
	 * senere. Antallet kan dog ikke være højere end antallet af listeprisens varer
	 * eller lavere end 1.
	 */
	private void valueUdlejAction(Button button) {
		try {
			int antalVarer = Integer.parseInt(txfUdlejAntal.getText().trim());
			Ordrelinje ordrelinje = lvwOrdrelinjer.getSelectionModel().getSelectedItem();
			if (ordrelinje != null) {
				if (button.equals(btnUdlejPlus) && antalVarer < ordrelinje.getAntal()) {
					antalVarer++;
				} else if (button.equals(btnUdlejMinus) && antalVarer > 1) {
					antalVarer--;
				}
				txfUdlejAntal.setText(antalVarer + "");
			}
		} catch (NumberFormatException ex) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Fejl");
			alert.setHeaderText("Fejl");
			alert.setContentText("Antallet skal være et tal.");
			alert.show();
		}
	}

	/**
	 * Flytter det valgte antal af listeprisen fra tempOrdrelinjer til
	 * tempOrdrelinjerUdlej.
	 */
	private void okUdlejAction() {
		Ordrelinje varerVenstreList = lvwOrdrelinjer.getSelectionModel().getSelectedItem();
		if (varerVenstreList != null) {
			int tempAntalVenstre = varerVenstreList.getAntal();
			int antalVarer = Integer.parseInt(txfUdlejAntal.getText());
			Iterator<Ordrelinje> iterator = tempOrdrelinjerUdlej.iterator();
			if (tempAntalVenstre > antalVarer) {
				boolean updated = false;
				while (iterator.hasNext() && updated == false) {
					Ordrelinje tempOrdrelinje = iterator.next();
					if (varerVenstreList.getListepris().equals(tempOrdrelinje.getListepris())) {
						int tempAntal = tempOrdrelinje.getAntal();
						Controller.setOrdrelinjeAntal(tempOrdrelinje, tempAntal + antalVarer);
						Controller.setOrdrelinjeAntal(varerVenstreList, tempAntalVenstre - antalVarer);
						updated = true;
					}
				}
				if (updated == false) {
					Ordrelinje tempVenstre = new Ordrelinje(varerVenstreList.getListepris(), antalVarer, false);
					tempOrdrelinjerUdlej.add(tempVenstre);
					Controller.setOrdrelinjeAntal(varerVenstreList, tempAntalVenstre - antalVarer);
					if (!tempVenstre.isBetalt()
							&& !tempVenstre.getListepris().getProdukt().getProduktgruppe().equals(Produktgruppe.PANT)
							&& brug.equals("AfslutUdlejning")) {
						Controller.setOrdrelinjePris(tempVenstre, 0);
					}
				}
			} else if (tempAntalVenstre - antalVarer == 0) {
				boolean updated = false;
				while (iterator.hasNext() && updated == false) {
					Ordrelinje tempOrdrelinje = iterator.next();
					if (varerVenstreList.getListepris().equals(tempOrdrelinje.getListepris())) {
						int tempAntal = tempOrdrelinje.getAntal();
						Controller.setOrdrelinjeAntal(tempOrdrelinje, tempAntal + antalVarer);
						tempOrdrelinjer.remove(varerVenstreList);
						updated = true;
					}
				}
				if (updated == false) {
					Ordrelinje tempVenstre = new Ordrelinje(varerVenstreList.getListepris(), antalVarer, false);
					tempOrdrelinjerUdlej.add(tempVenstre);
					tempOrdrelinjer.remove(varerVenstreList);
					if (!tempVenstre.isBetalt()
							&& !tempVenstre.getListepris().getProdukt().getProduktgruppe().equals(Produktgruppe.PANT)
							&& brug.equals("AfslutUdlejning")) {
						Controller.setOrdrelinjePris(tempVenstre, 0);
					}
				}
			} else {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Fejl");
				alert.setHeaderText("Negativt resultat");
				alert.setContentText("Mængde valgt er højere end mængden af varen, der eksisterer på listen.");
				alert.show();
			}
			txfUdlejAntal.setText("1");
			totalUdlejning();
			lvwOrdrelinjer.getItems().setAll(tempOrdrelinjer);
			lvwOrdrelinjerUdlej.getItems().setAll(tempOrdrelinjerUdlej);
			totalAction();
		}
	}

	/**
	 * Beregner den totale pris på den del af udlejningen, som ikke skal betales nu,
	 * hvis brugen er begynd udlejning. Beregner den totale pris på den del af
	 * udlejningen, som tilbageleveres, hvis brugen er afslut udlejning.
	 */
	private void totalUdlejning() {
		double totalUdlejning = 0;
		Iterator<Ordrelinje> iterator = tempOrdrelinjerUdlej.iterator();
		while (iterator.hasNext()) {
			Ordrelinje tempOrdrelinje = iterator.next();
			totalUdlejning += tempOrdrelinje.getPris();
		}
		txfTotalBetalesSenere.setText(String.format("%.2f", totalUdlejning));
	}

	/**
	 * Flytter den valgte listepris fra tempOrdrelinjerUdlej til tempOrdrelinjer.
	 */
	private void fortrydUdlejAction() {
		Ordrelinje oUdlej = lvwOrdrelinjerUdlej.getSelectionModel().getSelectedItem();
		if (oUdlej != null) {
			Iterator<Ordrelinje> iterator = tempOrdrelinjer.iterator();
			boolean found = false;
			while (iterator.hasNext() && !found) {
				Ordrelinje tempOrdrelinje = iterator.next();
				if (tempOrdrelinje.getListepris().equals(oUdlej.getListepris())) {
					int tempAntal = tempOrdrelinje.getAntal();
					Controller.setOrdrelinjeAntal(tempOrdrelinje, tempAntal + oUdlej.getAntal());
					tempOrdrelinjerUdlej.remove(oUdlej);
					lvwOrdrelinjer.getItems().setAll(tempOrdrelinjer);
					lvwOrdrelinjerUdlej.getItems().setAll(tempOrdrelinjerUdlej);
					totalUdlejning();
					totalAction();
					found = true;
				}
			}
			if (!found) {
				oUdlej.resetPris();
				tempOrdrelinjer.add(oUdlej);
				tempOrdrelinjerUdlej.remove(oUdlej);
				lvwOrdrelinjer.getItems().setAll(tempOrdrelinjer);
				lvwOrdrelinjerUdlej.getItems().setAll(tempOrdrelinjerUdlej);
				totalUdlejning();
				totalAction();
			}
			tempOrdrelinjerUdlej.remove(oUdlej);
			lvwOrdrelinjerUdlej.getItems().setAll(tempOrdrelinjerUdlej);
			totalUdlejning();
		}
	}

	/**
	 * Sætter værdien af det relevante felt til totalen.
	 */
	private void fuldtBeløbAction(Button button) {
		if (button.equals(btnDankortFuldt)) {
			// Dankort
			txfKontant.setText("0,00");
			txfMobilePay.setText("0,00");
			txfDankort.setText("" + txfTotal.getText());
			resterendeBeløbAction();
		} else if (button.equals(btnKontantFuldt)) {
			// Kontant
			txfDankort.setText("0,00");
			txfMobilePay.setText("0,00");
			txfKontant.setText("" + txfTotal.getText());
			resterendeBeløbAction();
		} else {
			// MobilePay
			txfDankort.setText("0,00");
			txfKontant.setText("0,00");
			txfMobilePay.setText("" + txfTotal.getText());
			resterendeBeløbAction();
		}
	}

	/**
	 * Beregner det resterende beløb efter betaling af det, der er sat i de
	 * forskellige betalingsfelter.
	 */
	private void resterendeBeløbAction() {
		try {
			double total = Double.parseDouble(txfTotal.getText().trim().replace(',', '.'));
			double dankort = Double.parseDouble(txfDankort.getText().trim().replace(',', '.'));
			double kontant = Double.parseDouble(txfKontant.getText().trim().replace(',', '.'));
			double mobilePay = Double.parseDouble(txfMobilePay.getText().trim().replace(',', '.'));
			total -= dankort + kontant + mobilePay;
			if (total >= 0) {
				txfResterende.setText(String.format("%.2f", total));
			} else if (total < 0 && brug.equals("AfslutUdlejning")) {
				txfResterende.setText(String.format("%.2f", total));
			} else if (total < 0) {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Fejl");
				alert.setHeaderText("For store beløb");
				alert.setContentText("Du kan ikke betale mere end totalen.");
				alert.show();
			}
		} catch (NumberFormatException ex) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Fejl");
			alert.setHeaderText("Fejl");
			alert.setContentText("Beløb skal angives som tal.");
			alert.show();

		}
	}

	/**
	 * Vælger den rette hjælpemetode til brugen.
	 */
	private void betalAction() {
		if (brug.equals("Salg") || brug.equals("Simpel")) {
			betalSalg();
		} else if (brug.equals("BegyndUdlejning")) {
			betalUdlejBegynd();
		} else if (brug.equals("AfslutUdlejning")) {
			betalUdlejAfslut();
		} else if (brug.equals("Rundvisning")) {
			betalRundvisning();
		}
	}

	/**
	 * Betaler et almindelig eller et simpelt salg. Sætter rabatten, opretter
	 * ordrelinjerne fra tempOrdrelinjer og tempOrdrelinjerKlip, tilføjer de brugte
	 * betalingsmetoder til ordren og sætter ordrens datoSlut til dagens dato.
	 */
	private void betalSalg() {
		try {
			if (txfRabat.getText().length() > 0) {
				Controller.setOrdreRabatMængde(ordre, Double.parseDouble(txfRabat.getText()));
			} else {
				cbbRabat.setValue(cbbRabat.getItems().get(0));
				txfRabat.setText("0");
			}

			if (Double.parseDouble(txfResterende.getText().trim().replace(",", ".")) == 0) {
				for (Ordrelinje ol : tempOrdrelinjer) {
					if (ol.getListepris().getProdukt() instanceof Sampakning) {
						OrdrelinjeSampak olSampak = (OrdrelinjeSampak) ol;
						OrdrelinjeSampak nyOl = (OrdrelinjeSampak) Controller.createOrdrelinje(ordre, ol.getListepris(),
								ol.getAntal(), true);
						Controller.setOrdrelinjeSampakBrygArray(nyOl, olSampak.getBrygArray());
						Controller.setOrdrelinjeSampakGlasArray(nyOl, olSampak.getGlasArray());
						txaOrdrelinjer.appendText("\n" + nyOl);
					} else {
						Ordrelinje nyOl = Controller.createOrdrelinje(ordre, ol.getListepris(), ol.getAntal(), true);
						txaOrdrelinjer.appendText("\n" + nyOl);
					}
				}
				for (Entry<Ordrelinje, Integer> ol : tempOrdrelinjerKlip.entrySet()) {
					Ordrelinje nyOl = Controller.createOrdrelinje(ordre, ol.getKey().getListepris(),
							ol.getKey().getPris(), ol.getKey().getAntal(), true);
					txaOrdrelinjer.appendText("\n" + nyOl);
				}

				txaOrdrelinjer.appendText(String.format("\n\nSubtotal: %.2f", ordre.getTotalUdenRabat()));

				if (ordre.getRabat() instanceof ProcentRabat) {
					txaOrdrelinjer.appendText(String.format("\nProcentrabat givet: %.2f%%", ordre.getRabatMængde()));
				} else if (ordre.getRabat() instanceof AftaltPris) {
					txaOrdrelinjer.appendText(String.format("\nAftalt beløb: %.2f", ordre.getRabatMængde()));
				}

				lblTotalPris.setText(String.format("%.2f", ordre.getTotalMedRabat()));

				for (Betalingsmetode bm : Controller.getBetalingsmetoder()) {
					if (Double.parseDouble(txfDankort.getText().trim().replace(',', '.')) > 0
							&& bm.getType().equals("Kort")) {
						Controller.addBetalingsmetode(ordre, bm,
								Double.parseDouble(txfDankort.getText().trim().replace(',', '.')));
					} else if (Double.parseDouble(txfKontant.getText().trim().replace(',', '.')) > 0
							&& bm.getType().equals("Kontant")) {
						Controller.addBetalingsmetode(ordre, bm,
								Double.parseDouble(txfKontant.getText().trim().replace(',', '.')));
					} else if (Double.parseDouble(txfMobilePay.getText().trim().replace(',', '.')) > 0
							&& bm.getType().equals("MobilePay")) {
						Controller.addBetalingsmetode(ordre, bm,
								Double.parseDouble(txfMobilePay.getText().trim().replace(',', '.')));
					} else if (Integer.parseInt(txfTotalKlip.getText()) > 0 && bm.getType().equals("Klippekort")) {
						Controller.addBetalingsmetode(ordre, bm,
								Double.parseDouble(txfTotalKlip.getText().trim().replace(',', '.')));
					}
				}

				Controller.setOrdreDatoSlut(ordre, LocalDate.now());

				kvitteringPane.toFront();

			} else {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Fejl");
				alert.setHeaderText("Manglende betaling");
				alert.setContentText("Hele beløbet skal betales.");
				alert.show();
			}
		} catch (NumberFormatException ex) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Fejl");
			alert.setHeaderText("Fejl");
			alert.setContentText("Beløb skal angives som tal.");
			alert.show();
		}
	}

	/**
	 * Betaler en rundvisning. Sætter rabatten, tilføjer de brugte betalingsmetoder
	 * til ordren og sætter ordrens datoSlut til dagens dato.
	 */
	private void betalRundvisning() {
		try {
			if (txfRabat.getText().length() > 0) {
				Controller.setOrdreRabatMængde(ordre, Double.parseDouble(txfRabat.getText()));
			} else {
				cbbRabat.setValue(cbbRabat.getItems().get(0));
				txfRabat.setText("0");
			}

			for (Ordrelinje ol : ordre.getOrdrelinjer()) {
				Controller.setOrdrelinjeBetalt(ol, true);
				txaOrdrelinjer.appendText("\n" + ol);
			}

			txaOrdrelinjer.appendText(String.format("\n\nSubtotal: %.2f", ordre.getTotalUdenRabat()));

			if (ordre.getRabat() instanceof ProcentRabat) {
				txaOrdrelinjer.appendText(String.format("\nProcentrabat givet: %.2f%%", ordre.getRabatMængde()));
			} else if (ordre.getRabat() instanceof AftaltPris) {
				txaOrdrelinjer.appendText(String.format("\nAftalt beløb: %.2f", ordre.getRabatMængde()));
			}

			for (Betalingsmetode bm : Controller.getBetalingsmetoder()) {
				if (Double.parseDouble(txfDankort.getText().trim().replace(',', '.')) > 0
						&& bm.getType().equals("Kort")) {
					Controller.addBetalingsmetode(ordre, bm,
							Double.parseDouble(txfDankort.getText().trim().replace(',', '.')));
				} else if (Double.parseDouble(txfKontant.getText().trim().replace(',', '.')) > 0
						&& bm.getType().equals("Kontant")) {
					Controller.addBetalingsmetode(ordre, bm,
							Double.parseDouble(txfKontant.getText().trim().replace(',', '.')));
				} else if (Double.parseDouble(txfMobilePay.getText().trim().replace(',', '.')) > 0
						&& bm.getType().equals("MobilePay")) {
					Controller.addBetalingsmetode(ordre, bm,
							Double.parseDouble(txfMobilePay.getText().trim().replace(',', '.')));
				} else if (Integer.parseInt(txfTotalKlip.getText()) > 0 && bm.getType().equals("Klippekort")) {
					Controller.addBetalingsmetode(ordre, bm,
							Double.parseDouble(txfTotalKlip.getText().trim().replace(',', '.')));
				}
			}

			lblTotalPris.setText(String.format("%.2f", ordre.getTotalMedRabat()));

			Controller.setOrdreDatoSlut(ordre, LocalDate.now());

			kvitteringPane.toFront();
		} catch (NumberFormatException ex) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Fejl");
			alert.setHeaderText("Fejl");
			alert.setContentText("Beløb skal angives som tal.");
			alert.show();
		}
	}

	/**
	 * Betaler en begyndt udlejning. Opretter ordrelinjerne fra tempOrdrelinjer og
	 * temOrdrelinjerUdlej, tilføjer de brugte betalingsmetoder til ordren og sætter
	 * ordrens datoSlut til dagens dato, hvis ordren ender med at blive fuldt betalt
	 * i stedet for en udlejning.
	 */
	private void betalUdlejBegynd() {
		try {
			if (Double.parseDouble(txfResterende.getText().trim().replace(",", ".")) == 0) {
				for (Ordrelinje ol : tempOrdrelinjer) {
					if (ol.getListepris().getProdukt() instanceof Sampakning) {
						OrdrelinjeSampak olSampak = (OrdrelinjeSampak) ol;
						OrdrelinjeSampak nyOl = (OrdrelinjeSampak) Controller.createOrdrelinje(ordre, ol.getListepris(),
								ol.getAntal(), true);
						Controller.setOrdrelinjeSampakBrygArray(nyOl, olSampak.getBrygArray());
						Controller.setOrdrelinjeSampakGlasArray(nyOl, olSampak.getGlasArray());
						txaOrdrelinjer.appendText("\n" + nyOl);
					} else {
						Ordrelinje nyOl = Controller.createOrdrelinje(ordre, ol.getListepris(), ol.getAntal(), true);
						txaOrdrelinjer.appendText("\n" + nyOl);
					}
				}

				txaOrdrelinjer.appendText(String.format("\n\nBetalt nu: %.2f\n", ordre.getTotalUdenRabat()));

				for (Ordrelinje ol : tempOrdrelinjerUdlej) {
					if (ol.getListepris().getProdukt() instanceof Sampakning) {
						OrdrelinjeSampak olSampak = (OrdrelinjeSampak) ol;
						OrdrelinjeSampak nyOl = (OrdrelinjeSampak) Controller.createOrdrelinje(ordre, ol.getListepris(),
								ol.getAntal(), false);
						Controller.setOrdrelinjeSampakBrygArray(nyOl, olSampak.getBrygArray());
						Controller.setOrdrelinjeSampakGlasArray(nyOl, olSampak.getGlasArray());
						txaOrdrelinjer.appendText("\n" + nyOl);
					} else {
						Ordrelinje nyOl = Controller.createOrdrelinje(ordre, ol.getListepris(), ol.getAntal(), false);
						txaOrdrelinjer.appendText("\n" + nyOl);
					}
				}

				lblTotalPris.setText(String.format("%.2f", ordre.getTotalMedRabat()));

				for (Betalingsmetode bm : Controller.getBetalingsmetoder()) {
					if (Double.parseDouble(txfDankort.getText().trim().replace(',', '.')) > 0
							&& bm.getType().equals("Kort")) {
						Controller.addBetalingsmetode(ordre, bm,
								Double.parseDouble(txfDankort.getText().trim().replace(',', '.')));
					} else if (Double.parseDouble(txfKontant.getText().trim().replace(',', '.')) > 0
							&& bm.getType().equals("Kontant")) {
						Controller.addBetalingsmetode(ordre, bm,
								Double.parseDouble(txfKontant.getText().trim().replace(',', '.')));
					} else if (Double.parseDouble(txfMobilePay.getText().trim().replace(',', '.')) > 0
							&& bm.getType().equals("MobilePay")) {
						Controller.addBetalingsmetode(ordre, bm,
								Double.parseDouble(txfMobilePay.getText().trim().replace(',', '.')));
					} else if (Integer.parseInt(txfTotalKlip.getText()) > 0 && bm.getType().equals("Klippekort")) {
						Controller.addBetalingsmetode(ordre, bm,
								Double.parseDouble(txfTotalKlip.getText().trim().replace(',', '.')));
					}
				}

				if (ordre.isBetalt()) {
					Controller.setOrdreDatoSlut(ordre, LocalDate.now());
				}

				kvitteringPane.toFront();
			} else {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Fejl");
				alert.setHeaderText("Manglende betaling");
				alert.setContentText("Hele beløbet skal betales.");
				alert.show();
			}
		} catch (NumberFormatException ex) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Fejl");
			alert.setHeaderText("Fejl");
			alert.setContentText("Beløb skal angives som tal.");
			alert.show();
		}
	}

	/**
	 * Betaler en afsluttet udlejning. Opretter ordrelinjerne fra tempOrdrelinjer og
	 * temOrdrelinjerUdlej, tilføjer de brugte betalingsmetoder til ordren og sætter
	 * ordrens datoSlut til dagens dato.
	 */
	private void betalUdlejAfslut() {
		try {
			if (txfRabat.getText().length() > 0) {
				Controller.setOrdreRabatMængde(ordre, Double.parseDouble(txfRabat.getText()));
			} else {
				cbbRabat.setValue(cbbRabat.getItems().get(0));
				txfRabat.setText("0");
			}

			if (Double.parseDouble(txfResterende.getText().trim().replace(",", ".")) == 0) {
				for (Ordrelinje ol : tempOrdrelinjer) {
					Controller.setOrdrelinjeBetalt(ol, true);
					txaOrdrelinjer.appendText("\n" + ol);
				}

				txaOrdrelinjer.appendText(String.format("\n\nBetalt nu: %.2f\n",
						Double.parseDouble(txfTotal.getText().trim().replace(',', '.'))));

				txaOrdrelinjer.appendText("\nModregnet eller tilbageleveret:");

				for (Ordrelinje ol : tempOrdrelinjerUdlej) {
					if (ol.getListepris().getProdukt() instanceof Sampakning) {
						OrdrelinjeSampak olSampak = (OrdrelinjeSampak) ol;
						OrdrelinjeSampak nyOl = (OrdrelinjeSampak) Controller.createOrdrelinje(ordre, ol.getListepris(),
								ol.getAntal(), true);
						Controller.setOrdrelinjeSampakBrygArray(nyOl, olSampak.getBrygArray());
						Controller.setOrdrelinjeSampakGlasArray(nyOl, olSampak.getGlasArray());
						Controller.setOrdrelinjePris(nyOl, 0);
						txaOrdrelinjer.appendText("\n" + nyOl);
					} else {
						Ordrelinje nyOl = Controller.createOrdrelinje(ordre, ol.getListepris(), ol.getAntal(), true);
						Controller.setOrdrelinjePris(nyOl, 0);
						txaOrdrelinjer.appendText("\n" + nyOl);
					}
				}

				txaOrdrelinjer.appendText("\nTidligere betalt:");
				for (Ordrelinje ol : tempOrdrelinjerBetalt) {
					if (ol.getListepris().getProdukt() instanceof Sampakning) {
						OrdrelinjeSampak olSampak = (OrdrelinjeSampak) ol;
						OrdrelinjeSampak nyOl = (OrdrelinjeSampak) Controller.createOrdrelinje(ordre, ol.getListepris(),
								ol.getAntal(), true);
						Controller.setOrdrelinjeSampakBrygArray(nyOl, olSampak.getBrygArray());
						Controller.setOrdrelinjeSampakGlasArray(nyOl, olSampak.getGlasArray());
						Controller.setOrdrelinjePris(nyOl, 0);
						txaOrdrelinjer.appendText("\n" + nyOl);
					} else {
						Ordrelinje nyOl = Controller.createOrdrelinje(ordre, ol.getListepris(), ol.getAntal(), true);
						Controller.setOrdrelinjePris(nyOl, 0);
						txaOrdrelinjer.appendText("\n" + nyOl);
					}
				}

				lblTotalPris.setText(String.format("%.2f", ordre.getTotalMedRabat()));

				for (Betalingsmetode bm : Controller.getBetalingsmetoder()) {
					if (Double.parseDouble(txfDankort.getText().trim().replace(',', '.')) > 0
							&& bm.getType().equals("Kort")) {
						Controller.addBetalingsmetode(ordre, bm,
								Double.parseDouble(txfDankort.getText().trim().replace(',', '.')));
					} else if (Double.parseDouble(txfKontant.getText().trim().replace(',', '.')) > 0
							&& bm.getType().equals("Kontant")) {
						Controller.addBetalingsmetode(ordre, bm,
								Double.parseDouble(txfKontant.getText().trim().replace(',', '.')));
					} else if (Double.parseDouble(txfMobilePay.getText().trim().replace(',', '.')) > 0
							&& bm.getType().equals("MobilePay")) {
						Controller.addBetalingsmetode(ordre, bm,
								Double.parseDouble(txfMobilePay.getText().trim().replace(',', '.')));
					} else if (Integer.parseInt(txfTotalKlip.getText()) > 0 && bm.getType().equals("Klippekort")) {
						Controller.addBetalingsmetode(ordre, bm,
								Double.parseDouble(txfTotalKlip.getText().trim().replace(',', '.')));
					}
				}

				if (ordre.isBetalt()) {
					Controller.setOrdreDatoSlut(ordre, LocalDate.now());
				}

				kvitteringPane.toFront();
			} else {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Fejl");
				alert.setHeaderText("Manglende betaling");
				alert.setContentText("Hele beløbet skal betales.");
				alert.show();
			}
		} catch (

		NumberFormatException ex) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Fejl");
			alert.setHeaderText("Fejl");
			alert.setContentText("Beløb skal angives som tal.");
			alert.show();
		}
	}

	/**
	 * Viser den indledende total.
	 */
	private void totalInitialAction() {
		double total = 0;
		for (Ordrelinje ordrelinje : tempOrdrelinjer) {
			total += ordrelinje.getPris();
		}
		for (Map.Entry<Ordrelinje, Integer> liste : tempOrdrelinjerKlip.entrySet()) {
			total += (liste.getKey().getPris());
		}
		if (brug.equals("AfslutUdlejning")) {
			total -= Double.parseDouble(txfTotalBetalesSenere.getText().trim().replace(',', '.'));
		}
		txfTotal.setText(String.format("%.2f", total));
		resterendeBeløbAction();
	}

	/**
	 * Viser totalen undervejs med rabatter.
	 */
	private void totalAction() {
		if (txfRabat.getText().length() != 0) {
			double total = 0;
			Rabat rabat = cbbRabat.getSelectionModel().getSelectedItem();

			for (Ordrelinje ordrelinje : tempOrdrelinjer) {
				total += ordrelinje.getPris();
			}

			for (Map.Entry<Ordrelinje, Integer> liste : tempOrdrelinjerKlip.entrySet()) {
				total += (liste.getKey().getPris());
			}

			if (brug.equals("AfslutUdlejning")) {
				total -= Double.parseDouble(txfTotalBetalesSenere.getText().trim().replace(',', '.'));
			}

			if (rabat instanceof ProcentRabat) {
				try {
					double rabatText = Double.parseDouble(txfRabat.getText());
					if (rabatText > 100) {
						txfRabat.setText("100");
						rabatText = 100;
					}
					txfTotal.setText(String.format("%.2f", (total - (total * (rabatText / 100)))));
				} catch (IllegalArgumentException iae) {
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("Fejl");
					alert.setHeaderText("Forkert datatype");
					alert.setContentText("Indtast venligst kun hele tal eller decimaltal.");
					alert.show();
				}
			} else if (rabat instanceof AftaltPris) {
				txfTotal.setText(txfRabat.getText());
			} else {
				txfTotal.setText(String.format("%.2f", total));
			}
			resterendeBeløbAction();
		} else {
			totalInitialAction();
		}
	}

	/**
	 * Sætter kundens informationer på kvitteringen.
	 */
	private void kundeAction() {
		if (ordre != null) {
			Kunde kunde = ordre.getKunde();
			boolean levering = false;
			if (kunde != null) {
				Label lblKunde = new Label("Kunde:"), lblKundeNavn = new Label(kunde.getNavn()),
						lblKundeTelefon = new Label("Tlf.: " + kunde.getTelefonnummer()),
						lblKundeAdresse = new Label(kunde.getAdresse()), lblLevering = new Label("Leveringsadresse:"),
						lblKundeLevering = new Label(kunde.getLeveringsadresse());
				kundeBox.getChildren().addAll(lblKunde, lblKundeNavn, lblKundeTelefon, lblKundeAdresse);
				for (Ordrelinje ordrelinje : tempOrdrelinjer) {
					if (ordrelinje.getListepris().getProdukt().getProduktgruppe() == Produktgruppe.LEVERING) {
						levering = true;
					}
				}
				if (levering) {
					kundeBox.getChildren().addAll(lblLevering, lblKundeLevering);
				}
			}
		}
	}

	/**
	 * Åbner et fejl-vindue med beskeden "Du har ikke en printer tilkoblet." Er kun
	 * for sjov.
	 */
	private void printAction() {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Fejl");
		alert.setHeaderText("Fejl ved udskrift");
		alert.setContentText("Du har ikke en printer tilkoblet.");
		alert.show();
	}

	/**
	 * Sætter ordrelinjerne fra tempOrdrelinjerUdlej og tempOrdrelinjerBetalt
	 * tilbage på ordrens liste af ordrelinjer i tilfælde af, at vinduet lukkes, før
	 * ordren er betalt.
	 */
	private void resetOnClose() {
		if (brug.equals("AfslutUdlejning")) {
			for (Ordrelinje olUdlej : tempOrdrelinjerUdlej) {
				boolean found = false;
				Iterator<Ordrelinje> iterator = tempOrdrelinjer.iterator();
				while (iterator.hasNext() && !found) {
					Ordrelinje olOrdre = iterator.next();
					if (olUdlej.getListepris().equals(olOrdre.getListepris())) {
						int udlejAntal = olUdlej.getAntal();
						int ordreAntal = olOrdre.getAntal();
						Controller.setOrdrelinjeAntal(olOrdre, udlejAntal + ordreAntal);
						found = true;
					}
				}
				if (!found) {
					if (olUdlej instanceof OrdrelinjeSampak) {
						OrdrelinjeSampak olSampak = (OrdrelinjeSampak) olUdlej;
						OrdrelinjeSampak nyOl = (OrdrelinjeSampak) Controller.createOrdrelinje(ordre,
								olSampak.getListepris(), olSampak.getAntal(), olSampak.isBetalt());
						Controller.setOrdrelinjeSampakBrygArray(nyOl, olSampak.getBrygArray());
						Controller.setOrdrelinjeSampakGlasArray(nyOl, olSampak.getGlasArray());
					} else {
						Controller.createOrdrelinje(ordre, olUdlej.getListepris(), olUdlej.getAntal(),
								olUdlej.isBetalt());
					}
				}
			}

			for (Ordrelinje olBetalt : tempOrdrelinjerBetalt) {
				boolean found = false;
				Iterator<Ordrelinje> iterator = tempOrdrelinjer.iterator();
				while (iterator.hasNext() && !found) {
					Ordrelinje olOrdre = iterator.next();
					if (olBetalt.getListepris().equals(olOrdre.getListepris())) {
						int betaltAntal = olBetalt.getAntal();
						int ordreAntal = olOrdre.getAntal();
						Controller.setOrdrelinjeAntal(olOrdre, betaltAntal + ordreAntal);
						found = true;
					}
				}
				if (!found) {
					if (olBetalt instanceof OrdrelinjeSampak) {
						OrdrelinjeSampak olSampak = (OrdrelinjeSampak) olBetalt;
						OrdrelinjeSampak nyOl = (OrdrelinjeSampak) Controller.createOrdrelinje(ordre,
								olSampak.getListepris(), olSampak.getAntal(), olSampak.isBetalt());
						Controller.setOrdrelinjeSampakBrygArray(nyOl, olSampak.getBrygArray());
						Controller.setOrdrelinjeSampakGlasArray(nyOl, olSampak.getGlasArray());
					} else {
						Controller.createOrdrelinje(ordre, olBetalt.getListepris(), olBetalt.getAntal(),
								olBetalt.isBetalt());
					}
				}
			}
		}
	}
}