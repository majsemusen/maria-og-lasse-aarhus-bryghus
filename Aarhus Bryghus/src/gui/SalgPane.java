package gui;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import controller.Controller;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import model.Bryg;
import model.Glas;
import model.Kunde;
import model.Listepris;
import model.Ordre;
import model.Ordrelinje;
import model.OrdrelinjeSampak;
import model.Prisliste;
import model.Produkt;
import model.Produktgruppe;
import model.Sampakning;

public class SalgPane extends GridPane {

	private final TextField txfAntal = new TextField(), txfTotal = new TextField();
	private final Label lblPrisliste = new Label();
	private final ListView<Listepris> lvwPriser = new ListView<>();
	private final ListView<Produkt> lvwBryg = new ListView<>(), lvwGlas = new ListView<>(),
			lvwValgteBryg = new ListView<>(), lvwValgteGlas = new ListView<>();
	private final ListView<Ordrelinje> lvwOrdrelinjer = new ListView<>();
	private final ComboBox<Prisliste> cbbPrislister = new ComboBox<>();
	private final Button btnDelete = new Button("Slet");
	private final Button btnPlus = new Button("+"), btnMinus = new Button("-"), btnAccept = new Button("OK"),
			btnArrow = new Button();
	private final Button btnArrowSampak = new Button(), btnBrygLeft = new Button("<"), btnBrygRight = new Button(">"),
			btnGlasLeft = new Button("<"), btnGlasRight = new Button(">"), btnSampakAccept = new Button("OK");
	private final Button btnSimpelBetal = new Button("Til betaling"), btnAlmBetal = new Button("Til betaling"),
			btnKunde = new Button("Vælg kunde");
	private final Button btnAnlæg = new Button("Anlæg"), btnBeklædning = new Button("Beklædning"),
			btnFadøl = new Button("Fadøl"), btnFlaske = new Button("Flasker"),
			btnFustage = new Button("Fustager"), btnGlas = new Button("Glas"), btnKlippekort = new Button("Klippekort"),
			btnKulsyre = new Button("Kulsyre"), btnLevering = new Button("Levering"), btnMalt = new Button("Malt"),
			btnPant = new Button("Pant"), btnRundvisning = new Button(), btnSampakning = new Button("Gaveæsker"),
			btnSnacks = new Button("Snacks"), btnSpiritus = new Button("Spiritus"), btnVand = new Button("Vand");
	private final Button[] produktButtons = { btnAnlæg, btnBeklædning, btnFadøl, btnFlaske, btnFustage,
			btnGlas, btnKlippekort, btnKulsyre, btnLevering, btnMalt, btnPant, btnRundvisning, btnSampakning, btnSnacks,
			btnSpiritus, btnVand };
	private StackPane varePane = new StackPane(), infoPane = new StackPane();
	private GridPane buttonPane = new GridPane(), prisPane = new GridPane(), sampakPane = new GridPane(),
			simpeltSalgPane = new GridPane(), almSalgPane = new GridPane();

	private Prisliste prisliste;
	private Kunde kunde;
	private String brug;
	private final ArrayList<Ordrelinje> tempOrdrelinjer = new ArrayList<>();
	private Bryg[] tempBrygArray;
	private Glas[] tempGlasArray;

	public SalgPane(String brug) {
		this.setAlignment(Pos.CENTER);

		// viser eller skjuler grid lines
		this.setGridLinesVisible(false);

		// sætter padding for pane
		this.setPadding(new Insets(20));
		// sætter horisontalt mellemrum mellem elementerne
		this.setHgap(20);
		// sætter vertikalt mellemrum mellem elementerne
		this.setVgap(10);

		int kolonner = 2;
		this.brug = brug;

		HBox changeBox = new HBox(10);
		changeBox.setAlignment(Pos.CENTER_RIGHT);
		this.add(changeBox, 0, 0, kolonner, 1);

		changeBox.getChildren().add(cbbPrislister);
		if (brug.equals("Salg")) {
			cbbPrislister.getItems().addAll(Controller.getPrislister());
		} else if (brug.equals("BegyndUdlejning")) {
			for (Prisliste prisliste : Controller.getPrislister()) {
				if (!prisliste.isSimpel()) {
					cbbPrislister.getItems().add(prisliste);
				}
			}
		}
		cbbPrislister.setOnMouseClicked(event -> loadPrislisterAction());
		cbbPrislister.setOnAction(event -> switchAction());

		HBox titleBox = new HBox(10);
		titleBox.setAlignment(Pos.CENTER);
		titleBox.setPadding(new Insets(20, 0, 20, 0));
		this.add(titleBox, 0, 1, kolonner, 1);

		titleBox.getChildren().add(lblPrisliste);
		lblPrisliste.setFont(Font.font(null, FontWeight.BOLD, 30));

		this.add(varePane, 0, 2);
		varePane.getChildren().addAll(sampakPane, prisPane, buttonPane);
		prisPane.setAlignment(Pos.CENTER);
		buttonPane.setAlignment(Pos.CENTER);

		VBox kvitteringBox = new VBox(10);
		kvitteringBox.setAlignment(Pos.CENTER);
		kvitteringBox.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, null, null)));
		this.add(kvitteringBox, 1, 2);

		kvitteringBox.getChildren().add(lvwOrdrelinjer);
		lvwOrdrelinjer.setMinWidth(400);

		HBox totalBox = new HBox(10);
		totalBox.setAlignment(Pos.CENTER_RIGHT);
		kvitteringBox.getChildren().add(totalBox);

		Label lblTotal = new Label("Total:");
		lblTotal.setFont(Font.font(null, FontWeight.BOLD, -1));
		totalBox.getChildren().addAll(lblTotal, txfTotal);
		txfTotal.setMaxWidth(80);
		txfTotal.setAlignment(Pos.CENTER_RIGHT);
		txfTotal.setEditable(false);
		txfTotal.setText("0,00");

		HBox deleteBox = new HBox(10);
		deleteBox.setAlignment(Pos.CENTER_RIGHT);
		this.add(deleteBox, 1, 3);

		deleteBox.getChildren().add(btnDelete);
		btnDelete.setOnAction(event -> deleteAction());

		this.add(infoPane, 0, 4, kolonner, 1);
		infoPane.getChildren().addAll(simpeltSalgPane, almSalgPane);
		simpeltSalgPane.setAlignment(Pos.TOP_RIGHT);
		almSalgPane.setAlignment(Pos.TOP_RIGHT);

		// -------------------------------------------------------------------------

		// BUTTONPANE
		buttonPane.setBackground(new Background(new BackgroundFill(Color.WHITESMOKE, null, null)));
		buttonPane.setGridLinesVisible(false);
		buttonPane.setHgap(30);
		buttonPane.setVgap(30);

		buttonPane.add(btnFlaske, 0, 0);
		buttonPane.add(btnFadøl, 1, 0);
		buttonPane.add(btnFustage, 2, 0);

		buttonPane.add(btnKlippekort, 0, 1);
		buttonPane.add(btnSampakning, 1, 1);
		buttonPane.add(btnSpiritus, 2, 1);

		buttonPane.add(btnAnlæg, 0, 2);
		buttonPane.add(btnGlas, 1, 2);
		buttonPane.add(btnLevering, 2, 2);

		buttonPane.add(btnKulsyre, 0, 3);
		buttonPane.add(btnMalt, 1, 3);
		buttonPane.add(btnBeklædning, 2, 3);

		HBox buttonBox = new HBox(30);
		buttonBox.setAlignment(Pos.CENTER);
		buttonPane.add(buttonBox, 0, 4, 3, 1);
		if (brug.equals("BegyndUdlejning")) {
			buttonBox.getChildren().add(btnPant);
		}
		buttonBox.getChildren().addAll(btnSnacks, btnVand);

		// Sætter alle buttons til samme bredde og sætter deres userdata til den rette
		// produktgruppe
		for (int i = 0; i < produktButtons.length; i++) {
			Produktgruppe[] produktgrupper = Produktgruppe.values();
			produktButtons[i].setUserData(produktgrupper[i]);
			produktButtons[i].setMinSize(98, 50);
			produktButtons[i].setDisable(true);
			produktButtons[i].setOnAction(event -> produktgruppeAction((Button) event.getSource()));
		}

		// -------------------------------------------------------------------------

		// PRODUKTPANE
		prisPane.setBackground(new Background(new BackgroundFill(Color.WHITESMOKE, null, null)));
		prisPane.setGridLinesVisible(false);
		prisPane.setHgap(10);
		prisPane.setVgap(10);

		VBox arrowBox = new VBox(10);
		arrowBox.setAlignment(Pos.TOP_LEFT);
		prisPane.add(arrowBox, 0, 0);

		arrowBox.getChildren().add(btnArrow);
		btnArrow.setStyle("-fx-base: rgba(0,0,0,0);");
		Image arrow = new Image("https://i.imgur.com/ECBWpdT.png");
		ImageView arrowView = new ImageView(arrow);
		arrowView.setFitHeight(50);
		arrowView.setFitWidth(50);
		btnArrow.setGraphic(arrowView);
		btnArrow.setOnAction(event -> goBackAction());

		prisPane.add(lvwPriser, 1, 0, 1, 3);
		lvwPriser.setMinWidth(400);

		VBox antalBox = new VBox(10);
		antalBox.setPadding(new Insets(150, 0, 0, 0));
		antalBox.setAlignment(Pos.BOTTOM_CENTER);
		prisPane.add(antalBox, 2, 0, 2, 1);

		Label lblAntal = new Label("Antal:");
		antalBox.getChildren().addAll(lblAntal, txfAntal);
		txfAntal.setMaxWidth(72);
		txfAntal.setAlignment(Pos.CENTER_RIGHT);
		txfAntal.setText("1");

		prisPane.add(btnPlus, 2, 1);
		btnPlus.setOnAction(event -> valueAction((Button) event.getSource()));

		prisPane.add(btnMinus, 3, 1);
		btnMinus.setMinWidth(31);
		btnMinus.setOnAction(event -> valueAction((Button) event.getSource()));

		prisPane.add(btnAccept, 4, 1);
		btnAccept.setOnAction(event -> acceptAction());

		// -------------------------------------------------------------------------

		// SAMPAKPANE
		sampakPane.setBackground(new Background(new BackgroundFill(Color.WHITESMOKE, null, null)));
		sampakPane.setGridLinesVisible(false);
		sampakPane.setHgap(10);
		sampakPane.setVgap(15);

		int listViewWidth = 230;
		int listViewHeight = 190;
		int arrowWidth = 50;

		VBox arrowSampakBox = new VBox(10);
		arrowSampakBox.setAlignment(Pos.TOP_LEFT);
		sampakPane.add(arrowSampakBox, 0, 0);

		arrowSampakBox.getChildren().add(btnArrowSampak);
		btnArrowSampak.setStyle("-fx-base: rgba(0,0,0,0);");
		Image arrowSampak = new Image("https://i.imgur.com/ECBWpdT.png");
		ImageView arrowSampakView = new ImageView(arrowSampak);
		arrowSampakView.setFitHeight(50);
		arrowSampakView.setFitWidth(50);
		btnArrowSampak.setGraphic(arrowSampakView);
		btnArrowSampak.setOnAction(event -> goBackSampakAction());

		sampakPane.add(lvwBryg, 1, 0);
		lvwBryg.setMaxSize(listViewWidth, listViewHeight);
		lvwBryg.getItems().setAll(Controller.getProdukterFraProduktgruppe(Produktgruppe.FLASKE));

		sampakPane.add(lvwGlas, 1, 1);
		lvwGlas.setMaxSize(listViewWidth, listViewHeight);
		lvwGlas.getItems().setAll(Controller.getProdukterFraProduktgruppe(Produktgruppe.GLAS));

		VBox arrowBrygBox = new VBox(10);
		arrowBrygBox.setAlignment(Pos.CENTER);
		sampakPane.add(arrowBrygBox, 2, 0);

		arrowBrygBox.getChildren().addAll(btnBrygLeft, btnBrygRight);
		btnBrygLeft.setMinWidth(arrowWidth);
		btnBrygLeft.setUserData("Bryg");
		btnBrygLeft.setOnAction(event -> moveLeftAction((Button) event.getSource()));
		btnBrygRight.setMinWidth(arrowWidth);
		btnBrygRight.setUserData("Bryg");
		btnBrygRight.setOnAction(event -> moveRightAction((Button) event.getSource()));

		VBox arrowGlasBox = new VBox(10);
		arrowGlasBox.setAlignment(Pos.CENTER);
		sampakPane.add(arrowGlasBox, 2, 1);

		arrowGlasBox.getChildren().addAll(btnGlasLeft, btnGlasRight);
		btnGlasLeft.setMinWidth(arrowWidth);
		btnGlasLeft.setUserData("Glas");
		btnGlasLeft.setOnAction(event -> moveLeftAction((Button) event.getSource()));
		btnGlasRight.setMinWidth(arrowWidth);
		btnGlasRight.setUserData("Glas");
		btnGlasRight.setOnAction(event -> moveRightAction((Button) event.getSource()));

		sampakPane.add(lvwValgteBryg, 3, 0);
		lvwValgteBryg.setMaxSize(listViewWidth, listViewHeight);

		sampakPane.add(lvwValgteGlas, 3, 1);
		lvwValgteGlas.setMaxSize(listViewWidth, listViewHeight);

		HBox sampakDoneBox = new HBox(10);
		sampakDoneBox.setAlignment(Pos.BOTTOM_RIGHT);
		sampakPane.add(sampakDoneBox, 3, 2);

		sampakDoneBox.getChildren().add(btnSampakAccept);
		btnSampakAccept.setOnAction(event -> sampakAcceptAction());

		// -------------------------------------------------------------------------

		// SIMPELT SALG
		simpeltSalgPane.setBackground(new Background(new BackgroundFill(Color.WHITESMOKE, null, null)));
		simpeltSalgPane.setGridLinesVisible(false);
		simpeltSalgPane.setPadding(new Insets(30, 0, 0, 0));
		simpeltSalgPane.setHgap(10);
		simpeltSalgPane.setVgap(10);

		HBox simpelBetalBox = new HBox(10);
		simpelBetalBox.setAlignment(Pos.BOTTOM_RIGHT);
		simpeltSalgPane.add(simpelBetalBox, 0, 0);

		simpelBetalBox.getChildren().add(btnSimpelBetal);
		btnSimpelBetal.setFont(Font.font(20));
		btnSimpelBetal.setOnAction(event -> simpelBetalAction());

		// -------------------------------------------------------------------------

		// ALMINDELIGT SALG
		almSalgPane.setBackground(new Background(new BackgroundFill(Color.WHITESMOKE, null, null)));
		almSalgPane.setGridLinesVisible(false);
		almSalgPane.setPadding(new Insets(30, 0, 0, 0));
		almSalgPane.setHgap(50);
		almSalgPane.setVgap(10);

		almSalgPane.add(btnKunde, 0, 0);
		btnKunde.setOnAction(event -> kundeAction());

		almSalgPane.add(btnAlmBetal, 1, 0);
		btnAlmBetal.setFont(Font.font(20));
		btnAlmBetal.setOnAction(event -> almBetalAction());
	}

	// -------------------------------------------------------------------------

	// METODER

	/**
	 * Benyttes til at skifte mellem forskellige prislister.
	 */
	private void switchAction() {
		Prisliste prislisteNy = cbbPrislister.getValue();
		if (prislisteNy != null) {
			if (prisliste == null) {
				switchHelp(prislisteNy);
			} else if (!prislisteNy.equals(prisliste)) {
				Alert alert = new Alert(AlertType.CONFIRMATION);
				alert.setTitle("Bekræftelse af skift");
				alert.setContentText("Ønsker du at skifte fra prislisten " + prisliste.getSalgssituation() + " til "
						+ prislisteNy.getSalgssituation() + "?");
				alert.showAndWait();
				if (alert.getResult() == ButtonType.OK) {
					switchHelp(prislisteNy);
				}
			}
		}
	}

	/**
	 * Hjælpemetode til switchAction().
	 */
	private void switchHelp(Prisliste prisliste) {
		lblPrisliste.setText(prisliste.getSalgssituation().toUpperCase());
		this.prisliste = prisliste;
		for (Button btn : produktButtons) {
			if (prisliste.erProduktgruppenAnvendt((Produktgruppe) btn.getUserData())) {
				btn.setDisable(false);
			} else {
				btn.setDisable(true);
			}
		}
		if (!prisliste.isSimpel()) {
			almSalgPane.toFront();
		} else {
			simpeltSalgPane.toFront();
		}
		tempOrdrelinjer.clear();
		lvwOrdrelinjer.getItems().setAll();
		txfAntal.setText("1");
		totalAction();
		buttonPane.toFront();
	}

	/**
	 * Henter alle prislister, hvis brugen er salg, hvis brugen er udlejning, hentes
	 * alle prislister, der ikke er simple.
	 */
	private void loadPrislisterAction() {
		cbbPrislister.getItems().clear();
		if (brug.equals("Salg")) {
			cbbPrislister.getItems().addAll(Controller.getPrislister());
		} else if (brug.equals("BegyndUdlejning")) {
			for (Prisliste prisliste : Controller.getPrislister()) {
				if (!prisliste.isSimpel()) {
					cbbPrislister.getItems().add(prisliste);
				}
			}
		}
	}

	/**
	 * Sætter alt i prislisten til at svare til priserne tilhørende den valgte
	 * produktgruppe.
	 */
	private void produktgruppeAction(Button button) {
		Produktgruppe produktgruppe = (Produktgruppe) button.getUserData();
		if (produktgruppe.equals(Produktgruppe.SAMPAKNING)) {
			lvwPriser.getItems().setAll(prisliste.getListePriserFraProduktgruppe(produktgruppe));
			txfAntal.setEditable(false);
			btnPlus.setDisable(true);
			btnMinus.setDisable(true);
			prisPane.toFront();
		} else {
			lvwPriser.getItems().setAll(prisliste.getListePriserFraProduktgruppe(produktgruppe));
			txfAntal.setEditable(true);
			btnPlus.setDisable(false);
			btnMinus.setDisable(false);
			prisPane.toFront();
		}
	}

	/**
	 * Øger txfAntal med +1 hvis knappen, der trykkes på indeholder "+". Ellers
	 * trækkes 1 fra txfAntal.
	 */
	private void valueAction(Button button) {
		{
			try {
				int antal = Integer.parseInt(txfAntal.getText().trim());
				if (button.getText().contains("+")) {
					antal++;
				} else if (antal > 1) {
					antal--;
				}
				txfAntal.setText(antal + "");
			} catch (NumberFormatException ex) {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Fejl");
				alert.setHeaderText("Fejl");
				alert.setContentText("Antallet skal være et tal.");
				alert.show();
			}
		}
	}

	/**
	 * Tilføjer valgte listepriser til tempOrdrelinjer, hvis de ikke findes, og
	 * opdaterer antallet, hvis listeprisen allerede er i tempOrdrelinjer.
	 */
	private void acceptAction() {
		try {
			int antal = Integer.parseInt(txfAntal.getText().trim());
			Listepris listepris = lvwPriser.getSelectionModel().getSelectedItem();

			if (antal > 0 && listepris != null) {
				if (listepris.getProdukt() instanceof Sampakning) {
					sampakAction(listepris);
				} else {
					boolean update = false;
					Iterator<Ordrelinje> iterator = tempOrdrelinjer.iterator();
					while (iterator.hasNext() && !update) {
						Ordrelinje tempOrdrelinje = iterator.next();
						if (tempOrdrelinje.getListepris().equals(listepris)) {
							tempOrdrelinje.setAntal(antal + tempOrdrelinje.getAntal());
							update = true;
						}
					}
					if (!update) {
						Ordrelinje tempOrdrelinje = new Ordrelinje(listepris, antal, false);
						tempOrdrelinjer.add(tempOrdrelinje);
					}
					txfAntal.setText("1");
					lvwOrdrelinjer.getItems().setAll(tempOrdrelinjer);
				}
			}
			totalAction();
		} catch (NumberFormatException ex) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Fejl");
			alert.setHeaderText("Fejl");
			alert.setContentText("Antallet skal være et tal.");
			alert.show();
		}
	}

	/**
	 * Hjælpemetode til acceptAction().
	 */
	private void sampakAction(Listepris listepris) {
		Sampakning sampak = (Sampakning) listepris.getProdukt();
		tempBrygArray = new Bryg[sampak.getBryg()];
		tempGlasArray = new Glas[sampak.getGlas()];
		sampakPane.toFront();
	}

	/**
	 * Tilføjer valgte Sampakning i form af en OrdrelinjeSampak til tempOrdrelinjer
	 * og sætter herefter OrdrelinjeSampak's to arrays.
	 */
	private void sampakAcceptAction() {
		if (filledArray(tempBrygArray) && filledArray(tempGlasArray)) {
			int antal = Integer.parseInt(txfAntal.getText().trim());
			Listepris listepris = lvwPriser.getSelectionModel().getSelectedItem();
			OrdrelinjeSampak tempOrdrelinje = new OrdrelinjeSampak(listepris, antal, false);
			Controller.setOrdrelinjeSampakBrygArray(tempOrdrelinje, tempBrygArray);
			Controller.setOrdrelinjeSampakGlasArray(tempOrdrelinje, tempGlasArray);
			tempOrdrelinjer.add(tempOrdrelinje);
			lvwValgteBryg.getItems().clear();
			lvwValgteGlas.getItems().clear();
			tempBrygArray = null;
			tempGlasArray = null;
			lvwOrdrelinjer.getItems().setAll(tempOrdrelinjer);
			totalAction();
			buttonPane.toFront();
		}
	}

	/**
	 * Hjælpemetode til sampakAcceptAction().<br/>
	 * Returnerer false, hvis der er null på minimum én plads i arrayet.
	 */
	private boolean filledArray(Produkt[] array) {
		boolean filled = true;
		if (array.length > 0) {
			for (int i = 0; i < array.length; i++) {
				if (array[i] == null) {
					return false;
				}
			}
		}
		return filled;
	}

	/**
	 * Bruges til sampakninger. Fjerner et produkt fra "gavekurven" til højre.
	 */
	private void moveLeftAction(Button button) {
		if (button.getUserData().equals("Bryg")) {
			Bryg bryg = (Bryg) lvwValgteBryg.getSelectionModel().getSelectedItem();
			int i = tempBrygArray.length - 1;
			boolean found = false;
			while (i >= 0 && !found) {
				if (tempBrygArray[i] != null && tempBrygArray[i].equals(bryg)) {
					tempBrygArray[i] = null;
					found = true;
				} else {
					i--;
				}
			}
			lvwValgteBryg.getItems().setAll(tempBrygArray);
		} else {
			Glas glas = (Glas) lvwValgteGlas.getSelectionModel().getSelectedItem();
			int i = tempGlasArray.length - 1;
			boolean found = false;
			while (i >= 0 && !found) {
				if (tempGlasArray[i] != null && tempGlasArray[i].equals(glas)) {
					tempGlasArray[i] = null;
					found = true;
				} else {
					i--;
				}
			}
			lvwValgteGlas.getItems().setAll(tempGlasArray);
		}
	}

	/**
	 * Bruges til sampakninger. Flytter et produkt fra listen til venstre til
	 * "gavekurven" til højre.
	 */
	private void moveRightAction(Button button) {
		if (button.getUserData().equals("Bryg")) {
			Bryg bryg = (Bryg) lvwBryg.getSelectionModel().getSelectedItem();
			int i = 0;
			boolean foundSpace = false;
			while (i < tempBrygArray.length && !foundSpace) {
				if (tempBrygArray[i] == null) {
					tempBrygArray[i] = bryg;
					foundSpace = true;
				} else {
					i++;
				}
			}
			lvwValgteBryg.getItems().setAll(tempBrygArray);
		} else {
			Glas glas = (Glas) lvwGlas.getSelectionModel().getSelectedItem();
			int i = 0;
			boolean foundSpace = false;
			while (i < tempGlasArray.length && !foundSpace) {
				if (tempGlasArray[i] == null) {
					tempGlasArray[i] = glas;
					foundSpace = true;
				} else {
					i++;
				}
			}
			lvwValgteGlas.getItems().setAll(tempGlasArray);
		}
	}

	/**
	 * Sletter en midlertidig ordrelinje.
	 */
	private void deleteAction() {
		Ordrelinje ordrelinje = lvwOrdrelinjer.getSelectionModel().getSelectedItem();
		tempOrdrelinjer.remove(ordrelinje);
		lvwOrdrelinjer.getItems().setAll(tempOrdrelinjer);
		totalAction();
	}

	/**
	 * Går tilbage til buttonPane, der indeholder produktknapperne.
	 */
	private void goBackAction() {
		buttonPane.toFront();
		btnFlaske.requestFocus();
		txfAntal.setText("1");
	}

	/**
	 * Bruges til sampakninger. Går tilbage til buttonPane, der indeholder
	 * produktknapperne, og tømmer desuden de to arrays med glas og bryg.
	 */
	private void goBackSampakAction() {
		lvwValgteBryg.getItems().clear();
		lvwValgteGlas.getItems().clear();
		tempBrygArray = null;
		tempGlasArray = null;
		buttonPane.toFront();
	}

	/**
	 * Viser den nuværende pris af alle valgte produkter * antal.
	 */
	private void totalAction() {
		double total = 0;
		for (Ordrelinje ordrelinje : tempOrdrelinjer) {
			total += ordrelinje.getPris();
		}
		txfTotal.setText(String.format("%.2f", total));
	}

	/**
	 * Clearer tempOrdrelinjer indeholdende information til brug af oprettelse og
	 * samtidig visning af ordrelinjer og clearer lvwOrdrelinjer.
	 */
	private void clearOrdre() {
		tempOrdrelinjer.clear();
		lvwOrdrelinjer.getItems().clear();
		totalAction();
	}

	/**
	 * Benyttes ved betaling til simple salg, f.eks. salg i fredagsbar, hvor der
	 * ikke er brug for information om kunde.
	 */
	private void simpelBetalAction() {
		try {
			if (tempOrdrelinjer.size() > 0) {
				Ordre ordre = Controller.createOrdre(LocalDate.now(), Controller.getRabatter().get(0), 0);
				BetalingWindow betalingWindow = new BetalingWindow("Betaling", null, "Simpel", ordre, tempOrdrelinjer);
				betalingWindow.showAndWait();
				clearOrdre();
			}
		} catch (IllegalArgumentException iae) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Fejl");
			alert.setHeaderText("Forkert datatype");
			alert.setContentText("Invalide input");
			alert.show();
		}
	}

	/**
	 * Metoden bruges typisk til at registrere almindelige salg, f.eks. i butikken,
	 * hvor der er brug for information om en kunde.
	 */
	private void almBetalAction() {
		try {
			if (kunde != null && tempOrdrelinjer.size() > 0) {
				Ordre ordre = Controller.createOrdre(kunde, LocalDate.now(), Controller.getRabatter().get(0), 0);
				BetalingWindow betalingWindow = new BetalingWindow("Betaling", null, brug, ordre, tempOrdrelinjer);
				betalingWindow.showAndWait();
				clearOrdre();
			} else {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Fejl");
				alert.setHeaderText("Ingen kunde valgt");
				alert.setContentText("En kunde skal vælges.");
				alert.show();
			}
		} catch (IllegalArgumentException iae) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Fejl");
			alert.setHeaderText("Forkert datatype");
			alert.setContentText("Invalide input");
			alert.show();
		}
	}

	/**
	 * Åbner et vindue med mulighed for oprettelse af et Kunde-objekt eller valg af
	 * et eksisterende Kunde-objekt.
	 */
	private void kundeAction() {
		KundeWindow kundeWindow = new KundeWindow("Kunder", null);
		kundeWindow.showAndWait();
		kunde = kundeWindow.getKunde();
		kundeWindow.clear();
	}
}