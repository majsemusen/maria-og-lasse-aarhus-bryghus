package gui;

import controller.Controller;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import model.Listepris;
import model.Prisliste;
import model.Produktgruppe;

public class PrislistePane extends GridPane {

	private final Label lblPrisliste = new Label("Prislister:"), lblProduktgrupper = new Label("Produktgrupper:"),
			lblProdukter = new Label("Produkter:");
	private final ListView<Prisliste> lvwPrislister = new ListView<>();
	private final ListView<Produktgruppe> lvwProduktgrupper = new ListView<>();
	private final ListView<Listepris> lvwListepriser = new ListView<>();
	private final Button btnCreate = new Button("Opret prisliste"), btnEdit = new Button("Rediger prisliste"),
			btnDelete = new Button("Slet prisliste");

	public PrislistePane() {
		this.setAlignment(Pos.CENTER);

		// viser eller skjuler grid lines
		this.setGridLinesVisible(false);

		// sætter padding for pane
		this.setPadding(new Insets(20));
		// sætter horisontalt mellemrum mellem elementerne
		this.setHgap(20);
		// sætter vertikalt mellemrum mellem elementerne
		this.setVgap(10);

		HBox buttonBox = new HBox(30);
		buttonBox.setAlignment(Pos.CENTER);
		buttonBox.setPadding(new Insets(0, 0, 20, 0));
		this.add(buttonBox, 0, 0, 3, 1);

		buttonBox.getChildren().add(btnCreate);
		btnCreate.setOnAction(event -> createAction());

		buttonBox.getChildren().add(btnEdit);
		btnEdit.setOnAction(event -> editAction());

		this.add(lblPrisliste, 0, 1);
		this.add(lvwPrislister, 0, 2);
		lvwPrislister.setMaxWidth(200);
		lvwPrislister.getItems().setAll(Controller.getPrislister());
		ChangeListener<Prisliste> prislisteListener = (ov, oldPrisliste, newPrisliste) -> this
				.prislisteAction();
		lvwPrislister.getSelectionModel().selectedItemProperty().addListener(prislisteListener);

		this.add(btnDelete, 0, 3);
		btnDelete.setOnAction(event -> deleteAction());

		this.add(lblProduktgrupper, 1, 1);
		this.add(lvwProduktgrupper, 1, 2);
		lvwProduktgrupper.setMaxWidth(200);
		ChangeListener<Produktgruppe> produktgruppeListener = (ov, oldProduktgruppe, newProduktgruppe) -> this
				.produktgruppeAction();
		lvwProduktgrupper.getSelectionModel().selectedItemProperty().addListener(produktgruppeListener);

		this.add(lblProdukter, 2, 1);
		this.add(lvwListepriser, 2, 2);
		lvwListepriser.setMinWidth(400);
	}

	// -------------------------------------------------------------------------

	/**
	 * Opretter en ny prisliste.
	 */
	private void createAction() {
		PrislisteWindow prislisteWindow = new PrislisteWindow("Opret prisliste", null);
		prislisteWindow.showAndWait();
		lvwPrislister.getItems().setAll(Controller.getPrislister());
	}

	/**
	 * Redigerer en eksisterende prisliste.
	 */
	private void editAction() {
		Prisliste prisliste = lvwPrislister.getSelectionModel().getSelectedItem();
		if (prisliste != null) {
			PrislisteWindow prislisteWindow = new PrislisteWindow("Rediger prisliste", prisliste);
			prislisteWindow.showAndWait();
			lvwPrislister.getItems().setAll(Controller.getPrislister());
		}
	}

	/**
	 * Sletter en eksisterende prisliste.
	 */
	private void deleteAction() {
		Prisliste prisliste = lvwPrislister.getSelectionModel().getSelectedItem();
		if (prisliste != null) {
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Bekræftelse af sletning");
			alert.setContentText("Ønsker du at slette prislisten " + prisliste.getSalgssituation() + "?");
			alert.showAndWait();
			if (alert.getResult() == ButtonType.OK) {
				Controller.deletePrisliste(prisliste);
				lvwPrislister.getItems().setAll(Controller.getPrislister());
			}
		}
	}

	/**
	 * Tester om produktgruppen er anvendt i den valgte prisliste og viser i så
	 * tilfælde produktgruppen på listen.
	 */
	private void prislisteAction() {
		lvwProduktgrupper.getItems().clear();
		Prisliste prisliste = lvwPrislister.getSelectionModel().getSelectedItem();
		if (prisliste != null) {
			for (Produktgruppe produktgruppe : Produktgruppe.values()) {
				if (prisliste.erProduktgruppenAnvendt(produktgruppe)) {
					lvwProduktgrupper.getItems().add(produktgruppe);
				}
			}
		}
	}

	/**
	 * Viser produkterne fra den valgte prislistes valgte produktgruppe.
	 */
	private void produktgruppeAction() {
		lvwListepriser.getItems().clear();
		Prisliste prisliste = lvwPrislister.getSelectionModel().getSelectedItem();
		if (prisliste != null) {
			Produktgruppe produktgruppe = lvwProduktgrupper.getSelectionModel().getSelectedItem();
			if (produktgruppe != null) {
				lvwListepriser.getItems().setAll(prisliste.getListePriserFraProduktgruppe(produktgruppe));
			}
		}
	}
}