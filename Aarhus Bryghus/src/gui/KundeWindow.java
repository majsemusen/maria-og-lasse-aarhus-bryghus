package gui;

import controller.Controller;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.Kunde;

public class KundeWindow extends Stage {

	public KundeWindow(String title, Stage owner) {
		this.initOwner(owner);
		this.initStyle(StageStyle.UTILITY);
		this.initModality(Modality.APPLICATION_MODAL);
		this.setResizable(false);

		this.setTitle(title);
		GridPane pane = new GridPane();
		pane.setAlignment(Pos.CENTER);
		this.initContent(pane);

		Scene scene = new Scene(pane);
		this.setScene(scene);
	}

	// -------------------------------------------------------------------------

	private final TextField txfNavn = new TextField(), txfTelefon = new TextField(), txfAdresse = new TextField(),
			txfLevering = new TextField();
	private final ListView<Kunde> lvwKunder = new ListView<>();
	private final Button btnCreate = new Button("Opret"), btnEdit = new Button("Rediger"),
			btnClear = new Button("Ryd felter"), btnOK = new Button("OK");

	private void initContent(GridPane pane) {
		// viser eller skjuler grid lines
		pane.setGridLinesVisible(false);

		// sætter padding for pane
		pane.setPadding(new Insets(20));
		// sætter horisontalt mellemrum mellem elementerne
		pane.setHgap(20);
		// sætter vertikalt mellemrum mellem elementerne
		pane.setVgap(10);

		Label lblEksisterende = new Label("Eksisterende kunder:");
		pane.add(lblEksisterende, 0, 0);

		pane.add(lvwKunder, 0, 1, 1, 6);
		lvwKunder.getItems().setAll(Controller.getKunder());

		HBox clearBox = new HBox(10);
		clearBox.setAlignment(Pos.BOTTOM_RIGHT);
		pane.add(clearBox, 2, 0);

		clearBox.getChildren().add(btnClear);
		btnClear.setOnAction(event -> clearAction());

		Label lblNavn = new Label("Navn:"), lblTelefon = new Label("Telefonnummer"), lblAdresse = new Label("Adresse:"),
				lblLevering = new Label("Leveringsadresse:");

		pane.add(lblNavn, 1, 1);
		pane.add(lblTelefon, 1, 2);
		pane.add(lblAdresse, 1, 3);
		pane.add(lblLevering, 1, 4);

		pane.add(txfNavn, 2, 1);
		pane.add(txfTelefon, 2, 2);
		pane.add(txfAdresse, 2, 3);
		pane.add(txfLevering, 2, 4);

		HBox buttonBox = new HBox(20);
		buttonBox.setAlignment(Pos.BOTTOM_RIGHT);
		pane.add(buttonBox, 2, 5);

		buttonBox.getChildren().addAll(btnEdit, btnCreate);
		btnEdit.setOnAction(event -> editAction());
		btnCreate.setOnAction(event -> createAction());

		HBox okBox = new HBox(20);
		okBox.setAlignment(Pos.BOTTOM_RIGHT);
		pane.add(okBox, 2, 6);

		okBox.getChildren().add(btnOK);
		btnOK.setOnAction(event -> okAction());

		ChangeListener<Kunde> kundeListener = (ov, oldProdukt, newProdukt) -> this.selectionChanged();
		lvwKunder.getSelectionModel().selectedItemProperty().addListener(kundeListener);
	}

// -------------------------------------------------------------------------

	/**
	 * Ændrer felternes tekst, hvis der er valgt en kunde.
	 */
	private void selectionChanged() {
		if (!lvwKunder.getItems().isEmpty()) {
			if (getKunde() != null) {
				Kunde k = getKunde();
				txfNavn.setText(k.getNavn());
				txfTelefon.setText(k.getTelefonnummer());
				txfAdresse.setText(k.getAdresse());
				txfLevering.setText(k.getLeveringsadresse());
			}
		}
	}

	/**
	 * Tømmer alle felterne.
	 */
	private void clearAction() {
		txfNavn.clear();
		txfTelefon.clear();
		txfAdresse.clear();
		txfLevering.clear();
	}

	/**
	 * Opretter en ny kunde.
	 */
	private void createAction() {
		String navn = txfNavn.getText();
		String telefonnummer = txfTelefon.getText();
		String adresse = txfAdresse.getText();
		String levering = txfLevering.getText();
		boolean exists = false;
		for (Kunde k : Controller.getKunder()) {
			if (k.getTelefonnummer().equals(telefonnummer)) {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Fejl");
				alert.setHeaderText("Forkert datatype");
				alert.setContentText("Invalide input");
				alert.show();
				exists = true;
				break;
			}
		}
		if (exists == false) {
			Controller.createKunde(navn, telefonnummer, adresse, levering);
		}
		lvwKunder.getItems().setAll(Controller.getKunder());
		clearAction();
	}

	/**
	 * Redigerer en eksisterende kunde.
	 */
	private void editAction() {
		if (!lvwKunder.getItems().isEmpty()) {
			if (getKunde() != null) {
				Kunde k = getKunde();
				Controller.editKunde(k, txfNavn.getText(), txfTelefon.getText(), txfAdresse.getText(),
						txfLevering.getText());
				clearAction();
			}
		}
	}

	/**
	 * Skjuler dette vindue.
	 */
	private void okAction() {
		this.hide();
	}

	/**
	 * Returnerer den valgte kunde.
	 */
	public Kunde getKunde() {
		return lvwKunder.getSelectionModel().getSelectedItem();
	}

	/**
	 * Bruges til at tømme vinduet, når kunden er hentet med getKunde().
	 */
	public void clear() {
		lvwKunder.getSelectionModel().clearSelection();
		clearAction();
	}
}