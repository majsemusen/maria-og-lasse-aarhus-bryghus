package gui;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class AdminFunktionWindow extends Stage {

	public AdminFunktionWindow(String title, Stage owner) {
		this.initOwner(owner);
		this.initStyle(StageStyle.UTILITY);
		this.initModality(Modality.APPLICATION_MODAL);
		this.setResizable(false);

		this.setTitle(title);
		GridPane pane = new GridPane();
		pane.setAlignment(Pos.CENTER);
		this.initContent(pane);

		Scene scene = new Scene(pane);
		this.setScene(scene);
	}

	// -------------------------------------------------------------------------

	private void initContent(GridPane pane) {
		// viser eller skjuler grid lines
		pane.setGridLinesVisible(false);

		// sætter padding for pane
		pane.setPadding(new Insets(20));
		// sætter horisontalt mellemrum mellem elementerne
		pane.setHgap(20);
		// sætter vertikalt mellemrum mellem elementerne
		pane.setVgap(10);

		TabPane tabPane = new TabPane();
		tabPane.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
		this.initTabPane(tabPane);
		pane.add(tabPane, 0, 0);
	}

	private void initTabPane(TabPane tabPane) {
		Tab tabPrisliste = new Tab("Prislister");
		tabPane.getTabs().add(tabPrisliste);
		PrislistePane prislistePane = new PrislistePane();
		tabPrisliste.setContent(prislistePane);

		Tab tabProdukt = new Tab("Produkter");
		tabPane.getTabs().add(tabProdukt);
		ProduktPane produktPane = new ProduktPane();
		tabProdukt.setContent(produktPane);
	}
}