package gui;

import java.util.Iterator;

import controller.Controller;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import model.Bruger;

public class MainApp extends Application {

	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) {
		stage.setTitle("Aarhus Bryghus Datasystem");
		GridPane pane = new GridPane();
		pane.setAlignment(Pos.CENTER);
		this.initContent(pane);

		Scene scene = new Scene(pane, 500, 300);
		stage.setScene(scene);
		stage.show();
		stage.setResizable(false);

		Controller.init();

		adminWindow = new AdminWindow("Aarhus Bryghus Datasystem - Admin", stage);
		brugerWindow = new BrugerWindow("Aarhus Bryghus Datasystem", stage);
	}

	@Override
	public void stop() {
		Controller.saveStorage();
	}

	// -------------------------------------------------------------------------

	private AdminWindow adminWindow;
	private BrugerWindow brugerWindow;

	private final TextField txfBrugernavn = new TextField();
	private final PasswordField passwordField = new PasswordField();
	private final Button btnLogin = new Button("Login");

	private void initContent(GridPane pane) {
		// viser eller skjuler grid lines
		pane.setGridLinesVisible(false);

		// sætter padding for pane
		pane.setPadding(new Insets(20));
		// sætter horisontalt mellemrum mellem elementerne
		pane.setHgap(20);
		// sætter vertikalt mellemrum mellem elementerne
		pane.setVgap(10);

		Image ablogo = new Image("https://i.imgur.com/M5SDn9a.png");
		ImageView imageView = new ImageView(ablogo);
		imageView.setFitHeight(100);
		imageView.setFitWidth(100);

		HBox imageBox = new HBox(10);
		imageBox.setPadding(new Insets(10, 10, 20, 10));
		imageBox.getChildren().add(imageView);

		pane.add(imageBox, 0, 0);
		imageBox.setAlignment(Pos.CENTER);

		pane.add(txfBrugernavn, 0, 1);
		txfBrugernavn.setText("Brugernavn");
		txfBrugernavn.setStyle("-fx-text-inner-color: grey;");
		txfBrugernavn.setOnMouseClicked(event -> mouseClicked((TextField) event.getSource()));
		txfBrugernavn.setOnAction(event -> loginAction());
		txfBrugernavn.setOnKeyPressed(event -> {
			if (!event.getCode().equals(KeyCode.ENTER) && !event.getCode().equals(KeyCode.TAB)) {
				keyPressed((TextField) event.getSource());
			}
		});

		pane.add(passwordField, 0, 2);
		passwordField.setText("Password");
		passwordField.setStyle("-fx-text-inner-color: grey;");
		passwordField.setOnMouseClicked(event -> mouseClicked((TextField) event.getSource()));
		passwordField.setOnAction(event -> loginAction());
		passwordField.setOnKeyPressed(event -> {
			if (!event.getCode().equals(KeyCode.ENTER) && !event.getCode().equals(KeyCode.TAB)) {
				keyPressed((TextField) event.getSource());
			}
		});

		HBox buttonBox = new HBox(25);
		pane.add(buttonBox, 0, 3);
		buttonBox.setAlignment(Pos.CENTER_RIGHT);

		buttonBox.getChildren().add(btnLogin);
		btnLogin.setPrefWidth(90);
		btnLogin.setFont(Font.font(null, FontWeight.BOLD, 15));
		btnLogin.setOnAction(event -> loginAction());
		btnLogin.setOnKeyPressed(event -> {
			if (event.getCode().equals(KeyCode.ENTER)) {
				btnLogin.fire();
			}
		});
	}

	// -------------------------------------------------------------------------

	/**
	 * Registerer museklik i et TextField.
	 */
	private void mouseClicked(TextField txf) {
		if (txf.getText().contains("Brugernavn") || txf.getText().contains("Password")) {
			txf.clear();
			txf.setStyle("-fx-text-inner-color: black;");
		}
	}

	/**
	 * Registrerer tast af knap i et TextField.
	 */
	private void keyPressed(TextField txf) {
		if (txf.getText().contains("Brugernavn") || txf.getText().contains("Password")) {
			txf.clear();
			txf.setStyle("-fx-text-inner-color: black;");
		}
	}

	/**
	 * Tjekker om det indtastede brugernavn svarer til det indtastede kodeord. Giver
	 * ellers fejl.
	 */
	private void loginAction() {
		String bruger = txfBrugernavn.getText().trim();
		String password = passwordField.getText();
		boolean found = false;
		Iterator<Bruger> iterator = Controller.getBrugere().iterator();
		while (iterator.hasNext() && !found) {
			Bruger b = iterator.next();
			if (b.getBrugernavn().equals(bruger) && b.confirmPassword(password)) {
				if (b.isAdmin()) {
					adminWindow.showAndWait();
				} else {
					brugerWindow.showAndWait();
				}
				found = true;
				txfBrugernavn.setText("Brugernavn");
				txfBrugernavn.setStyle("-fx-text-inner-color: grey;");
				passwordField.setText("Password");
				passwordField.setStyle("-fx-text-inner-color: grey;");
			}
		}
		if (!found) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Fejl");
			alert.setHeaderText("Forkert password eller brugernavn");
			alert.setContentText("Password matcher ikke brugernavnet, eller brugernavnet findes ikke..");
			alert.show();
		}
	}
}