package gui;

import java.time.LocalDate;
import java.util.ArrayList;

import controller.Controller;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import model.Ordre;
import model.Ordrelinje;
import model.Produktgruppe;

public class StatistikAfleveringPane extends GridPane {

	private final TextArea txaOrdre = new TextArea();
	private final ListView<Ordrelinje> lvwIkkeAfleveret = new ListView<>(), lvwForSentAfleveret = new ListView<>(),
			lvwAlleIkkeAfleveret = new ListView<>();
	private final Button btnBetal = new Button("Betal ordre");

	private ArrayList<Ordre> alleIkkeAfleveret = new ArrayList<>();

	private Ordre ordre;

	public StatistikAfleveringPane() {
		this.setAlignment(Pos.CENTER);

		// viser eller skjuler grid lines
		this.setGridLinesVisible(false);

		// sætter padding for pane
		this.setPadding(new Insets(20, 0, 0, 0));
		// sætter horisontalt mellemrum mellem elementerne
		this.setHgap(20);
		// sætter vertikalt mellemrum mellem elementerne
		this.setVgap(10);

		VBox ordreBox = new VBox(10);
		ordreBox.setAlignment(Pos.TOP_CENTER);
		this.add(ordreBox, 0, 0, 3, 2);

		Label lblOrdre = new Label("Ordre:");
		ordreBox.getChildren().addAll(lblOrdre, txaOrdre);
		txaOrdre.setMaxWidth(230);
		txaOrdre.setEditable(false);

		VBox betalBox = new VBox();
		betalBox.setAlignment(Pos.TOP_LEFT);
		this.add(betalBox, 2, 1);

		betalBox.getChildren().add(btnBetal);
		btnBetal.setOnAction(event -> betalAction());

		ChangeListener<Ordrelinje> ordrelinjeListener = (ov, oldOl, newOl) -> ordrelinjeAction(newOl);

		Label lblIkkeAfleveret = new Label("Nuværende udlejninger:");
		this.add(lblIkkeAfleveret, 0, 2);
		this.add(lvwIkkeAfleveret, 0, 3);
		lvwIkkeAfleveret.setMinWidth(300);
		lvwIkkeAfleveret.setOnMouseClicked(event -> clearListviewSelectionAction(event.getSource()));
		lvwIkkeAfleveret.getSelectionModel().selectedItemProperty().addListener(ordrelinjeListener);

		Label lblForSentAfleveret = new Label("For sene ikke-afleverede udlejninger:");
		this.add(lblForSentAfleveret, 1, 2);
		this.add(lvwForSentAfleveret, 1, 3);
		lvwForSentAfleveret.setMinWidth(300);
		lvwForSentAfleveret.setOnMouseClicked(event -> clearListviewSelectionAction(event.getSource()));
		lvwForSentAfleveret.getSelectionModel().selectedItemProperty().addListener(ordrelinjeListener);

		Label lblAlleIkkeAfleveret = new Label("Alle ikke-afleverede udlejninger:");
		this.add(lblAlleIkkeAfleveret, 2, 2);
		this.add(lvwAlleIkkeAfleveret, 2, 3);
		lvwAlleIkkeAfleveret.setMinWidth(300);
		lvwAlleIkkeAfleveret.setOnMouseClicked(event -> clearListviewSelectionAction(event.getSource()));
		lvwAlleIkkeAfleveret.getSelectionModel().selectedItemProperty().addListener(ordrelinjeListener);
	}

	// -------------------------------------------------------------------------

	/**
	 * Fylder listviews med ordrelinjer, fordelt efter de ordrelinjer, der højst er
	 * 14 dage gamle, de der er over 14 dage gamle, og alle ordrelinjer, der ikke er
	 * betalt.
	 */
	public void loadListViews() {
		lvwIkkeAfleveret.getItems().clear();
		lvwForSentAfleveret.getItems().clear();
		lvwAlleIkkeAfleveret.getItems().clear();
		for (Ordre ordre : Controller.getOrdrer()) {
			if (!ordre.isBetalt()) {
				if (ordre.getDatoStart().plusDays(15).isAfter(LocalDate.now())) {
					// Ordren er højst 14 dage gammel
					for (Ordrelinje ol : ordre.getOrdrelinjer()) {
						if (!ol.isBetalt() && !ol.getListepris().getProdukt().getProduktgruppe()
								.equals(Produktgruppe.RUNDVISNING)) {
							lvwIkkeAfleveret.getItems().add(ol);
							lvwAlleIkkeAfleveret.getItems().add(ol);
						}
					}
				} else {
					// Ordren er mere end 14 dage gammel
					for (Ordrelinje ol : ordre.getOrdrelinjer()) {
						if (!ol.isBetalt() && !ol.getListepris().getProdukt().getProduktgruppe()
								.equals(Produktgruppe.RUNDVISNING)) {
							lvwForSentAfleveret.getItems().add(ol);
							lvwAlleIkkeAfleveret.getItems().add(ol);
						}
					}
				}
				alleIkkeAfleveret.add(ordre);
			}
		}
	}

	/**
	 * Viser ordreinformation for den valgte ordrelinje.
	 */
	private void ordrelinjeAction(Ordrelinje ol) {
		if (ol != null) {
			for (Ordre ordre : alleIkkeAfleveret) {
				if (ordre.getOrdrelinjer().contains(ol)) {
					this.ordre = ordre;
					txaOrdre.clear();
					txaOrdre.appendText(ordre + "");
					break;
				}
			}
		}
	}

	/**
	 * Fjerner selection fra de to listviews, der ikke er trykket på.
	 */
	private void clearListviewSelectionAction(Object source) {
		if (source instanceof ListView<?>) {
			ListView<?> listview = (ListView<?>) source;
			if (listview.equals(lvwIkkeAfleveret)) {
				lvwForSentAfleveret.getSelectionModel().clearSelection();
				lvwAlleIkkeAfleveret.getSelectionModel().clearSelection();
			} else if (listview.equals(lvwForSentAfleveret)) {
				lvwIkkeAfleveret.getSelectionModel().clearSelection();
				lvwAlleIkkeAfleveret.getSelectionModel().clearSelection();
			} else if (listview.equals(lvwAlleIkkeAfleveret)) {
				lvwIkkeAfleveret.getSelectionModel().clearSelection();
				lvwForSentAfleveret.getSelectionModel().clearSelection();
			}
		}
	}

	private void betalAction() {
		if (ordre != null) {
			BetalingWindow betalingWindow = new BetalingWindow("Betaling", null, "AfslutUdlejning", ordre,
					(ArrayList<Ordrelinje>) ordre.getOrdrelinjer());
			betalingWindow.showAndWait();
			loadListViews();
		}
	}
}