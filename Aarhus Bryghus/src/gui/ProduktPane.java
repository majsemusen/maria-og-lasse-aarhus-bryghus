package gui;

import controller.Controller;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import model.Anlæg;
import model.Beklædning;
import model.Bryg;
import model.Diverse;
import model.Glas;
import model.Klippekort;
import model.Kulsyre;
import model.Levering;
import model.Malt;
import model.Pant;
import model.Produkt;
import model.Produktgruppe;
import model.Rundvisning;
import model.Sampakning;

public class ProduktPane extends GridPane {

	private final TextField txfVar1 = new TextField(), txfVar2 = new TextField(), txfVar3 = new TextField(),
			txfVar4 = new TextField();
	private final Label lblVar1 = new Label(), lblVar2 = new Label(), lblVar3 = new Label(), lblVar4 = new Label();
	private final ListView<Produkt> lvwProdukter = new ListView<>();
	private final ComboBox<Produktgruppe> cbbProduktgrupper = new ComboBox<>();
	private final Button btnCreate = new Button("Opret produkt");
	private final Button btnEdit = new Button("Rediger produkt");
	private final Button btnDelete = new Button("Slet produkt");

	public ProduktPane() {
		this.setAlignment(Pos.CENTER);

		// viser eller skjuler grid lines
		this.setGridLinesVisible(false);

		// sætter padding for pane
		this.setPadding(new Insets(20));
		// sætter horisontalt mellemrum mellem elementerne
		this.setHgap(20);
		// sætter vertikalt mellemrum mellem elementerne
		this.setVgap(10);

		Label lblProdukter = new Label("Eksisterende produkter:");
		this.add(lblProdukter, 0, 0);

		this.add(lvwProdukter, 0, 1, 1, 6);
		lvwProdukter.setMinWidth(400);
		lvwProdukter.getItems().setAll(Controller.getProdukter());
		ChangeListener<Produkt> produktListener = (ov, oldProdukt, newProdukt) -> this.selectionChanged();
		lvwProdukter.getSelectionModel().selectedItemProperty().addListener(produktListener);

		Label lblCreate = new Label("Opret nyt produkt:");
		this.add(lblCreate, 1, 0);

		this.add(cbbProduktgrupper, 2, 0);
		cbbProduktgrupper.getItems().setAll(Produktgruppe.values());
		ChangeListener<Produktgruppe> produktGruppeListener = (ov, oldProduktgruppe, newProduktgruppe) -> this
				.cbbAction();
		cbbProduktgrupper.getSelectionModel().selectedItemProperty().addListener(produktGruppeListener);

		this.add(lblVar1, 1, 1);
		this.add(txfVar1, 2, 1);
		txfVar1.setDisable(true);

		this.add(lblVar2, 1, 2);
		this.add(txfVar2, 2, 2);
		txfVar2.setDisable(true);

		this.add(lblVar3, 1, 3);
		this.add(txfVar3, 2, 3);
		txfVar3.setDisable(true);

		this.add(lblVar4, 1, 4);
		this.add(txfVar4, 2, 4);
		txfVar4.setDisable(true);

		HBox buttonBox = new HBox(20);
		this.add(buttonBox, 1, 6, 2, 1);

		buttonBox.getChildren().add(btnEdit);
		buttonBox.getChildren().add(btnCreate);
		buttonBox.getChildren().add(btnDelete);

		btnCreate.setOnAction(event -> createAction());
		btnEdit.setOnAction(event -> editAction());
		btnDelete.setOnAction(event -> deleteAction());
		cbbProduktgrupper.setValue(Produktgruppe.ANLÆG);

	}

	// -------------------------------------------------------------------------

	/**
	 * Sætter passende labelnavn, når der vælges en produktgruppe i comboboxen.
	 */
	private void cbbAction() {
		disableEditButton();
		enableCreateButton();
		clearLabels();
		clearTextFields();
		disableTextFields();
		if (cbbProduktgrupper.getValue().equals(Produktgruppe.ANLÆG)) {
			setVar1("Navn:");
		} else if (cbbProduktgrupper.getValue().equals(Produktgruppe.BEKLÆDNING)) {
			setVar1("Navn:");
			setVar2("Størrelse:");
			setVar3("Farve:");
			setVar4("Materiale:");
		} else if (cbbProduktgrupper.getValue().equals(Produktgruppe.FADØL)) {
			setVar1("Navn:");
			setVar2("Mængde:");
			setVar3("Enhed:", "cl");
			setVar4("Procent:");
			txfVar3.setDisable(true);
		} else if (cbbProduktgrupper.getValue().equals(Produktgruppe.FLASKE)) {
			setVar1("Navn:");
			setVar2("Mængde:");
			setVar3("Enhed:", "cl");
			setVar4("Procent:");
			txfVar3.setDisable(true);
		} else if (cbbProduktgrupper.getValue().equals(Produktgruppe.FUSTAGE)) {
			setVar1("Navn:");
			setVar2("Mængde:");
			setVar3("Enhed:", "L");
			setVar4("Procent:");
			txfVar3.setDisable(true);
		} else if (cbbProduktgrupper.getValue().equals(Produktgruppe.GLAS)) {
			setVar1("Navn:");
			setVar2("Størrelse:");
			setVar3("Enhed (ml/cl/l):");
		} else if (cbbProduktgrupper.getValue().equals(Produktgruppe.KLIPPEKORT)) {
			setVar1("Navn:");
			setVar2("Antal klip:");
		} else if (cbbProduktgrupper.getValue().equals(Produktgruppe.KULSYRE)) {
			setVar1("Navn:");
			setVar2("Mængde:");
		} else if (cbbProduktgrupper.getValue().equals(Produktgruppe.LEVERING)) {
			setVar1("Navn:");
			setVar2("Afstand i km:");
		} else if (cbbProduktgrupper.getValue().equals(Produktgruppe.MALT)) {
			setVar1("Navn:");
			setVar2("Mængde i kg:");
		} else if (cbbProduktgrupper.getValue().equals(Produktgruppe.PANT)) {
			setVar1("Navn:");
		} else if (cbbProduktgrupper.getValue().equals(Produktgruppe.RUNDVISNING)) {
			setVar1("Navn:");
		} else if (cbbProduktgrupper.getValue().equals(Produktgruppe.SAMPAKNING)) {
			setVar1("Navn:");
			setVar2("Antal flasker:");
			setVar3("Antal glas:");
		} else if (cbbProduktgrupper.getValue().equals(Produktgruppe.SNACKS)) {
			setVar1("Navn:");
			setVar2("Mængde:");
			setVar3("Enhed:", "g");
			txfVar3.setDisable(true);
		} else if (cbbProduktgrupper.getValue().equals(Produktgruppe.SPIRITUS)) {
			setVar1("Navn:");
			setVar2("Mængde:");
			setVar3("Enhed:", "cl");
			setVar4("Procent:");
			txfVar3.setDisable(true);
		} else if (cbbProduktgrupper.getValue().equals(Produktgruppe.VAND)) {
			setVar1("Navn:");
			setVar2("Mængde:");
			setVar3("Enhed:", "cl");
			txfVar3.setDisable(true);
		}
	}

	/**
	 * Opretter objekter ved at tage inputs fra textfields og derefter opdatere
	 * listerne.
	 */
	private void createAction() {
		Produktgruppe pg = cbbProduktgrupper.getValue();
		if (pg.equals(Produktgruppe.ANLÆG)) {
			try {
				Controller.createAnlæg(txfVar1.getText());
			} catch (IllegalArgumentException iae) {
				invalidDataAlert();
			}
		} else if (pg.equals(Produktgruppe.BEKLÆDNING)) {
			try {
				Controller.createBeklædning(txfVar1.getText(), txfVar2.getText(), txfVar3.getText(), txfVar4.getText());
			} catch (IllegalArgumentException iae) {
				invalidDataAlert();
			}
		} else if (pg.equals(Produktgruppe.FADØL)) {
			try {
				Controller.createBryg(Produktgruppe.FADØL, txfVar1.getText(), Integer.parseInt(txfVar2.getText()),
						txfVar3.getText(), Double.parseDouble(txfVar4.getText()));
			} catch (IllegalArgumentException iae) {
				invalidDataAlert();
			}
		} else if (pg.equals(Produktgruppe.FLASKE)) {
			try {
				Controller.createBryg(Produktgruppe.FLASKE, txfVar1.getText(), Integer.parseInt(txfVar2.getText()),
						txfVar3.getText(), Double.parseDouble(txfVar4.getText()));
			} catch (IllegalArgumentException iae) {
				invalidDataAlert();
			}
		} else if (pg.equals(Produktgruppe.FUSTAGE)) {
			try {
				Controller.createBryg(Produktgruppe.FUSTAGE, txfVar1.getText(), Integer.parseInt(txfVar2.getText()),
						txfVar3.getText(), Double.parseDouble(txfVar4.getText()));
			} catch (IllegalArgumentException iae) {
				invalidDataAlert();
			}
		} else if (pg.equals(Produktgruppe.GLAS)) {
			try {
				Controller.createGlas(txfVar1.getText(), Integer.parseInt(txfVar2.getText()), txfVar3.getText());
				txfVar3.setDisable(true);
			} catch (IllegalArgumentException iae) {
				invalidDataAlert();
			}
		} else if (pg.equals(Produktgruppe.KLIPPEKORT)) {
			try {
				Controller.createKlippekort(txfVar1.getText(), Integer.parseInt(txfVar2.getText()));
			} catch (IllegalArgumentException iae) {
				invalidDataAlert();
			}
		} else if (pg.equals(Produktgruppe.KULSYRE)) {
			try {
				Controller.createKulsyre(txfVar1.getText(), Integer.parseInt(txfVar2.getText()));
			} catch (IllegalArgumentException iae) {
				invalidDataAlert();
			}
		} else if (pg.equals(Produktgruppe.LEVERING)) {
			try {
				Controller.createLevering(txfVar1.getText(), Double.parseDouble(txfVar2.getText()));
			} catch (IllegalArgumentException iae) {
				invalidDataAlert();
			}
		} else if (pg.equals(Produktgruppe.MALT)) {
			try {
				Controller.createMalt(txfVar1.getText(), Integer.parseInt(txfVar2.getText()));
			} catch (IllegalArgumentException iae) {
				invalidDataAlert();
			}
		} else if (pg.equals(Produktgruppe.PANT)) {
			try {
				Controller.createPant(txfVar1.getText());
			} catch (IllegalArgumentException iae) {
				invalidDataAlert();
			}
		} else if (pg.equals(Produktgruppe.RUNDVISNING)) {
			try {
				Controller.createRundvisning(txfVar1.getText());
			} catch (IllegalArgumentException iae) {
				invalidDataAlert();
			}
		} else if (pg.equals(Produktgruppe.SAMPAKNING)) {
			try {
				Controller.createSampakning(txfVar1.getText(), Integer.parseInt(txfVar2.getText()),
						Integer.parseInt(txfVar3.getText()));
			} catch (IllegalArgumentException iae) {
				invalidDataAlert();
			}
		} else if (pg.equals(Produktgruppe.SNACKS)) {
			try {
				Controller.createDiverse(Produktgruppe.SNACKS, txfVar1.getText(), Integer.parseInt(txfVar2.getText()),
						txfVar3.getText());
			} catch (IllegalArgumentException iae) {
				invalidDataAlert();
			}
		} else if (pg.equals(Produktgruppe.SPIRITUS)) {
			try {
				Controller.createBryg(Produktgruppe.SPIRITUS, txfVar1.getText(), Integer.parseInt(txfVar2.getText()),
						txfVar3.getText(), Double.parseDouble(txfVar4.getText()));
			} catch (IllegalArgumentException iae) {
				invalidDataAlert();
			}
		} else if (pg.equals(Produktgruppe.VAND)) {
			try {
				Controller.createDiverse(Produktgruppe.VAND, txfVar1.getText(), Integer.parseInt(txfVar2.getText()),
						txfVar3.getText());
			} catch (IllegalArgumentException iae) {
				invalidDataAlert();
			}
		}
		lvwProdukter.getItems().setAll(Controller.getProdukter());
	}

	/**
	 * Opdaterer et valgt objekt via input fra textfields og opdaterer listerne.
	 */
	private void editAction() {
		int indexChange = lvwProdukter.getSelectionModel().getSelectedIndex();
		if (cbbProduktgrupper.getValue().equals(Produktgruppe.ANLÆG)) {
			Anlæg anlæg = (Anlæg) lvwProdukter.getSelectionModel().getSelectedItem();
			try {
				Controller.updateAnlæg(anlæg, txfVar1.getText());
			} catch (IllegalArgumentException iae) {
				invalidDataAlert();
			}
		} else if (cbbProduktgrupper.getValue().equals(Produktgruppe.BEKLÆDNING)) {
			Beklædning beklædning = (Beklædning) lvwProdukter.getSelectionModel().getSelectedItem();
			try {
				Controller.updateBeklædning(beklædning, txfVar1.getText(), txfVar2.getText(), txfVar3.getText(),
						txfVar4.getText());
			} catch (IllegalArgumentException iae) {
				invalidDataAlert();
			}
		} else if (cbbProduktgrupper.getValue().equals(Produktgruppe.FADØL)) {
			Bryg bryg = (Bryg) lvwProdukter.getSelectionModel().getSelectedItem();
			try {
				Controller.updateBryg(bryg, txfVar1.getText(), Integer.parseInt(txfVar2.getText()), txfVar3.getText(),
						Double.parseDouble(txfVar4.getText()));
			} catch (IllegalArgumentException iae) {
				invalidDataAlert();
			}
		} else if (cbbProduktgrupper.getValue().equals(Produktgruppe.FLASKE)) {
			Bryg bryg = (Bryg) lvwProdukter.getSelectionModel().getSelectedItem();
			try {
				Controller.updateBryg(bryg, txfVar1.getText(), Integer.parseInt(txfVar2.getText()), txfVar3.getText(),
						Double.parseDouble(txfVar4.getText()));
			} catch (IllegalArgumentException iae) {
				invalidDataAlert();
			}
		} else if (cbbProduktgrupper.getValue().equals(Produktgruppe.FUSTAGE)) {
			Bryg bryg = (Bryg) lvwProdukter.getSelectionModel().getSelectedItem();
			try {
				Controller.updateBryg(bryg, txfVar1.getText(), Integer.parseInt(txfVar2.getText()), txfVar3.getText(),
						Double.parseDouble(txfVar4.getText()));
			} catch (IllegalArgumentException iae) {
				invalidDataAlert();
			}
		} else if (cbbProduktgrupper.getValue().equals(Produktgruppe.GLAS)) {
			Glas glas = (Glas) lvwProdukter.getSelectionModel().getSelectedItem();
			try {
				Controller.updateGlas(glas, txfVar1.getText(), Integer.parseInt(txfVar2.getText()), txfVar3.getText());
			} catch (IllegalArgumentException iae) {
				invalidDataAlert();
			}
		} else if (cbbProduktgrupper.getValue().equals(Produktgruppe.KLIPPEKORT)) {
			Klippekort klippekort = (Klippekort) lvwProdukter.getSelectionModel().getSelectedItem();
			try {
				Controller.updateKlippekort(klippekort, txfVar1.getText(), Integer.parseInt(txfVar2.getText()));
			} catch (IllegalArgumentException iae) {
				invalidDataAlert();
			}
		} else if (cbbProduktgrupper.getValue().equals(Produktgruppe.KULSYRE)) {
			Kulsyre kulsyre = (Kulsyre) lvwProdukter.getSelectionModel().getSelectedItem();
			try {
				Controller.updateKulsyre(kulsyre, txfVar1.getText(), Integer.parseInt(txfVar2.getText()));
			} catch (IllegalArgumentException iae) {
				invalidDataAlert();
			}
		} else if (cbbProduktgrupper.getValue().equals(Produktgruppe.LEVERING)) {
			Levering levering = (Levering) lvwProdukter.getSelectionModel().getSelectedItem();
			try {
				Controller.updateLevering(levering, txfVar1.getText(), Integer.parseInt(txfVar2.getText()));
			} catch (IllegalArgumentException iae) {
				invalidDataAlert();
			}
		} else if (cbbProduktgrupper.getValue().equals(Produktgruppe.MALT)) {
			Malt malt = (Malt) lvwProdukter.getSelectionModel().getSelectedItem();
			try {
				Controller.updateMalt(malt, txfVar1.getText(), Integer.parseInt(txfVar2.getText()));
			} catch (IllegalArgumentException iae) {
				invalidDataAlert();
			}
		} else if (cbbProduktgrupper.getValue().equals(Produktgruppe.PANT)) {
			Pant pant = (Pant) lvwProdukter.getSelectionModel().getSelectedItem();
			try {
				Controller.updatePant(pant, txfVar1.getText());
			} catch (IllegalArgumentException iae) {
				invalidDataAlert();
			}
		} else if (cbbProduktgrupper.getValue().equals(Produktgruppe.RUNDVISNING)) {
			Rundvisning rundvisning = (Rundvisning) lvwProdukter.getSelectionModel().getSelectedItem();
			try {
				Controller.updateRundvisning(rundvisning, txfVar1.getText());
			} catch (IllegalArgumentException iae) {
				invalidDataAlert();
			}
		} else if (cbbProduktgrupper.getValue().equals(Produktgruppe.SAMPAKNING)) {
			Sampakning sampakning = (Sampakning) lvwProdukter.getSelectionModel().getSelectedItem();
			try {
				Controller.updateSampakning(sampakning, txfVar1.getText(), Integer.parseInt(txfVar2.getText()),
						Integer.parseInt(txfVar3.getText()));
			} catch (IllegalArgumentException iae) {
				invalidDataAlert();
			}
		} else if (cbbProduktgrupper.getValue().equals(Produktgruppe.SNACKS)) {
			Diverse diverse = (Diverse) lvwProdukter.getSelectionModel().getSelectedItem();
			try {
				Controller.updateDiverse(Produktgruppe.SNACKS, diverse, txfVar1.getText(),
						Integer.parseInt(txfVar2.getText()), txfVar3.getText());
			} catch (IllegalArgumentException iae) {
				invalidDataAlert();
			}
		} else if (cbbProduktgrupper.getValue().equals(Produktgruppe.SPIRITUS)) {
			Bryg bryg = (Bryg) lvwProdukter.getSelectionModel().getSelectedItem();
			try {
				Controller.updateBryg(bryg, txfVar1.getText(), Integer.parseInt(txfVar2.getText()), txfVar3.getText(),
						Double.parseDouble(txfVar4.getText()));
			} catch (IllegalArgumentException iae) {
				invalidDataAlert();
			}
		} else if (cbbProduktgrupper.getValue().equals(Produktgruppe.VAND)) {
			Diverse diverse = (Diverse) lvwProdukter.getSelectionModel().getSelectedItem();
			try {
				Controller.updateDiverse(Produktgruppe.VAND, diverse, txfVar1.getText(),
						Integer.parseInt(txfVar2.getText()), txfVar3.getText());
			} catch (IllegalArgumentException iae) {
				invalidDataAlert();
			}
		}
		lvwProdukter.getItems().setAll(Controller.getProdukter());
		lvwProdukter.getSelectionModel().select(indexChange);
	}

	/**
	 * Sletter et valgt produkt og opdaterer listerne.
	 */
	public void deleteAction() {
		if (!lvwProdukter.getItems().isEmpty()) {
			Produkt produkt = lvwProdukter.getSelectionModel().getSelectedItem();
			if (produkt != null) {
				Controller.deleteProdukt(produkt);
				lvwProdukter.getItems().setAll(Controller.getProdukter());

				if (!lvwProdukter.getItems().isEmpty()) {
					lvwProdukter.getSelectionModel().clearSelection();
				}
				disableEditButton();
			}
		}
	}

	/**
	 * Opdaterer labels og textfield, samt disabler diverse textfields ved valg af
	 * objekt i listviewet af produkter.
	 */
	private void selectionChanged() {
		if (!lvwProdukter.getItems().isEmpty()) {
			if (lvwProdukter.getSelectionModel().getSelectedItem() != null) {
				setProduktgruppeCombobox();
				enableButtons();
				enableTextFields();
				clearLabels();
				clearTextFields();
				enableTextFields();
				Produktgruppe p1 = lvwProdukter.getSelectionModel().getSelectedItem().getProduktgruppe();

				if (p1.equals((Produktgruppe.ANLÆG))) {
					Anlæg anlæg = (Anlæg) lvwProdukter.getSelectionModel().getSelectedItem();
					setVar1("Navn:", anlæg.getNavn());
					disableThreeTextFields();

				} else if (p1.equals(Produktgruppe.BEKLÆDNING)) {
					Beklædning beklædning = (Beklædning) lvwProdukter.getSelectionModel().getSelectedItem();
					setVar1("Navn:", beklædning.getNavn());
					setVar2("Størrelse:", beklædning.getStørrelse());
					setVar3("Farve:", beklædning.getFarve());
					setVar4("Materiale:", beklædning.getMateriale());

				} else if (p1.equals(Produktgruppe.FADØL)) {
					Bryg bryg = (Bryg) lvwProdukter.getSelectionModel().getSelectedItem();
					setVar1("Navn:", bryg.getNavn());
					setVar2("Mængde:", bryg.getMængde() + "");
					setVar3("Enhed (ml/cl/l):", bryg.getEnhed());
					setVar4("Procent:", bryg.getProcent() + "");
					txfVar3.setDisable(true);

				} else if (p1.equals(Produktgruppe.FLASKE)) {
					Bryg bryg = (Bryg) lvwProdukter.getSelectionModel().getSelectedItem();
					setVar1("Navn:", bryg.getNavn());
					setVar2("Mængde:", bryg.getMængde() + "");
					setVar3("Enhed (ml/cl/l):", bryg.getEnhed());
					setVar4("Procent:", bryg.getProcent() + "");
					txfVar3.setDisable(true);

				} else if (p1.equals(Produktgruppe.FUSTAGE)) {
					Bryg bryg = (Bryg) lvwProdukter.getSelectionModel().getSelectedItem();
					setVar1("Navn:", bryg.getNavn());
					setVar2("Mængde:", bryg.getMængde() + "");
					setVar3("Enhed (ml/cl/l):", bryg.getEnhed());
					setVar4("Procent:", bryg.getProcent() + "");
					txfVar3.setDisable(true);

				} else if (p1.equals(Produktgruppe.GLAS)) {
					Glas glas = (Glas) lvwProdukter.getSelectionModel().getSelectedItem();
					setVar1("Navn:", glas.getNavn());
					setVar2("Størrelse:", glas.getStørrelse() + "");
					setVar3("Enhed (ml/cl/l):", glas.getEnhed());
					disableTwoTextFields();

				} else if (p1.equals(Produktgruppe.KLIPPEKORT)) {
					Klippekort kk = (Klippekort) lvwProdukter.getSelectionModel().getSelectedItem();
					setVar1("Navn:", kk.getNavn());
					setVar2("Antal klip:", kk.getKlip() + "");
					disableTwoTextFields();
					disableEditButton();

				} else if (p1.equals(Produktgruppe.KULSYRE)) {
					Kulsyre kulsyre = (Kulsyre) lvwProdukter.getSelectionModel().getSelectedItem();
					setVar1("Navn:", kulsyre.getNavn());
					setVar2("Mængde:", kulsyre.getMængde() + "");
					disableTwoTextFields();

				} else if (p1.equals(Produktgruppe.LEVERING)) {
					Levering lv = (Levering) lvwProdukter.getSelectionModel().getSelectedItem();
					setVar1("Navn:", lv.getNavn());
					setVar2("Afstand i km:", lv.getAfstand() + "");
					disableTwoTextFields();

				} else if (p1.equals(Produktgruppe.MALT)) {
					Malt malt = (Malt) lvwProdukter.getSelectionModel().getSelectedItem();
					setVar1("Navn:", malt.getNavn());
					setVar2("Mængde i kg:", malt.getMængde() + "");
					disableTwoTextFields();

				} else if (p1.equals(Produktgruppe.PANT)) {
					Pant pant = (Pant) lvwProdukter.getSelectionModel().getSelectedItem();
					setVar1("Navn:", pant.getNavn());
					disableThreeTextFields();

				} else if (p1.equals(Produktgruppe.RUNDVISNING)) {
					Rundvisning rundvisning = (Rundvisning) lvwProdukter.getSelectionModel().getSelectedItem();
					setVar1("Navn:", rundvisning.getNavn());
					disableThreeTextFields();

				} else if (p1.equals(Produktgruppe.SAMPAKNING)) {
					Sampakning sp = (Sampakning) lvwProdukter.getSelectionModel().getSelectedItem();
					setVar1("Navn:", sp.getNavn());
					setVar2("Antal flasker:", sp.getBryg() + "");
					setVar3("Antal glas:", sp.getGlas() + "");
					disableOneTextField();

				} else if (p1.equals(Produktgruppe.SNACKS)) {
					Diverse diverse = (Diverse) lvwProdukter.getSelectionModel().getSelectedItem();
					setVar1("Navn:", diverse.getNavn());
					setVar2("Mængde:", diverse.getMængde() + "");
					setVar3("Enhed (g):", diverse.getEnhed());
					disableTwoTextFields();

				} else if (p1.equals(Produktgruppe.SPIRITUS)) {
					Bryg bryg = (Bryg) lvwProdukter.getSelectionModel().getSelectedItem();
					setVar1("Navn:", bryg.getNavn());
					setVar2("Mængde:", bryg.getMængde() + "");
					setVar3("Enhed (ml/cl/l):", bryg.getEnhed());
					setVar4("Procent:", bryg.getProcent() + "");
					txfVar3.setDisable(true);

				} else if (p1.equals(Produktgruppe.VAND)) {
					Diverse diverse = (Diverse) lvwProdukter.getSelectionModel().getSelectedItem();
					setVar1("Navn:", diverse.getNavn());
					setVar2("Mængde:", diverse.getMængde() + "");
					setVar3("Enhed (ml/cl/l):", diverse.getEnhed());
					disableTwoTextFields();
				}
			}
		}
	}

	private void setVar1(String labeltext) {
		lblVar1.setText(labeltext);
		txfVar1.setDisable(false);
	}

	private void setVar1(String labeltext, String txfText) {
		lblVar1.setText(labeltext);
		txfVar1.setDisable(false);
		txfVar1.setText(txfText);
	}

	private void setVar2(String labeltext) {
		lblVar2.setText(labeltext);
		txfVar2.setDisable(false);
	}

	private void setVar2(String labeltext, String txfText) {
		lblVar2.setText(labeltext);
		txfVar2.setDisable(false);
		txfVar2.setText(txfText);
	}

	private void setVar3(String labeltext) {
		lblVar3.setText(labeltext);
		txfVar3.setDisable(false);
	}

	private void setVar3(String labeltext, String txfText) {
		lblVar3.setText(labeltext);
		txfVar3.setDisable(false);
		txfVar3.setText(txfText);
	}

	private void setVar4(String labeltext) {
		lblVar4.setText(labeltext);
		txfVar4.setDisable(false);
	}

	private void setVar4(String labeltext, String txfText) {
		lblVar4.setText(labeltext);
		txfVar4.setDisable(false);
		txfVar4.setText(txfText);
	}

	private void enableTextFields() {
		txfVar1.setDisable(false);
		txfVar2.setDisable(false);
		txfVar3.setDisable(false);
		txfVar4.setDisable(false);
	}

	private void disableTextFields() {
		txfVar1.setDisable(true);
		txfVar2.setDisable(true);
		txfVar3.setDisable(true);
		txfVar4.setDisable(true);
	}

	private void disableThreeTextFields() {
		txfVar2.setDisable(true);
		txfVar3.setDisable(true);
		txfVar4.setDisable(true);
	}

	private void disableTwoTextFields() {
		txfVar3.setDisable(true);
		txfVar4.setDisable(true);
	}

	private void disableOneTextField() {
		txfVar4.setDisable(true);
	}

	private void clearLabels() {
		lblVar1.setText("");
		lblVar2.setText("");
		lblVar3.setText("");
		lblVar4.setText("");
	}

	private void clearTextFields() {
		txfVar1.clear();
		txfVar2.clear();
		txfVar3.clear();
		txfVar4.clear();
	}

	private void enableButtons() {
		btnCreate.setDisable(false);
		btnEdit.setDisable(false);
	}

	private void enableCreateButton() {
		btnCreate.setDisable(false);
	}

	private void disableEditButton() {
		btnEdit.setDisable(true);
	}

	private void setProduktgruppeCombobox() {
		cbbProduktgrupper.setValue(lvwProdukter.getSelectionModel().getSelectedItem().getProduktgruppe());
	}

	/**
	 * Viser en fejlmeddelelse, når der indtastes invalide data.
	 */
	private void invalidDataAlert() {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Fejl");
		alert.setHeaderText("Fejl");
		alert.setContentText("Invalide data indtastet.\nMængder indtastes kun som tal.");
		alert.show();
	}
}