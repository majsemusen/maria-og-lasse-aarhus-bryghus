package gui;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Map.Entry;

import controller.Controller;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import model.Betalingsmetode;
import model.Bryg;
import model.Glas;
import model.Listepris;
import model.Ordre;
import model.Ordrelinje;
import model.OrdrelinjeSampak;
import model.Prisliste;
import model.Produkt;
import model.Produktgruppe;

public class StatistikSalgPane extends GridPane {

	private final TextField txfDankort = new TextField(), txfKontant = new TextField(), txfMobilePay = new TextField(),
			txfKlip = new TextField(), txfTotalFørRabat = new TextField(), txfTotalEfterRabat = new TextField();
	private final TextField txfDagenTSFR = new TextField(), txfDagenTSEF = new TextField(),
			txfDagenGnsRabat = new TextField(), txfDagenGnsPris = new TextField(),
			txfDagenTotalDankort = new TextField(), txfDagenTotalKontant = new TextField(),
			txfDagenTotalMobilePay = new TextField(), txfDagenTotalKlip = new TextField();
	private final TextField txfKlipKøbt = new TextField(), txfKlipBrugt = new TextField(),
			txfPeriodenTSFR = new TextField(), txfPeriodenTSEF = new TextField(),
			txfPeriodenGnsRabat = new TextField(), txfPeriodenGnsPris = new TextField(),
			txfPeriodenTotalDankort = new TextField(), txfPeriodenTotalKontant = new TextField(),
			txfPeriodenTotalMobilePay = new TextField();
	private final TextArea txaListepriser = new TextArea();
	private final DatePicker startDato = new DatePicker(), slutDato = new DatePicker();
	private final ListView<Ordre> lvwOrdrer = new ListView<>();
	private final TableView<Ordrelinje> tblOrdrelinjer = new TableView<>();
	private final ComboBox<Prisliste> cbbPrislister = new ComboBox<>();
	private final Label lblAlleOrdrer = new Label();

	private GridPane datoPane = new GridPane(), dagensSalgPane = new GridPane(), periodensSalgPane = new GridPane();
	private ArrayList<Ordre> periodeOrdrer = new ArrayList<>();
	private final TextField[] ordreTextFields = { txfDankort, txfKontant, txfMobilePay, txfKlip, txfTotalFørRabat,
			txfTotalEfterRabat };
	private final TextField[] dagensTextFields = { txfDagenTSFR, txfDagenTSEF, txfDagenGnsRabat, txfDagenGnsPris,
			txfDagenTotalDankort, txfDagenTotalKontant, txfDagenTotalMobilePay, txfDagenTotalKlip };
	private final TextField[] periodensTextFields = { txfPeriodenTSFR, txfPeriodenTSEF, txfPeriodenGnsRabat,
			txfPeriodenGnsPris, txfPeriodenTotalDankort, txfPeriodenTotalKontant, txfPeriodenTotalMobilePay };
	private String brug;

	public StatistikSalgPane(String brug) {
		this.setAlignment(Pos.CENTER);

		// viser eller skjuler grid lines
		this.setGridLinesVisible(false);

		// sætter padding for pane
		this.setPadding(new Insets(20, 0, 0, 0));
		// sætter horisontalt mellemrum mellem elementerne
		this.setHgap(20);
		// sætter vertikalt mellemrum mellem elementerne
		this.setVgap(10);

		this.brug = brug;

		int txfWidth = 85;
		int txfWidthLarge = 120;

		datoPane.setGridLinesVisible(false);
		datoPane.setAlignment(Pos.TOP_LEFT);
		datoPane.setPadding(new Insets(0, 0, 10, 0));
		datoPane.setHgap(20);
		datoPane.setVgap(10);

		Label lblStartDato = new Label("Fra dato:");
		datoPane.add(lblStartDato, 0, 0);
		datoPane.add(startDato, 1, 0);
		startDato.setMaxWidth(130);
		startDato.setEditable(false);
		startDato.setOnAction(event -> datoAction());
		Label lblSlutDato = new Label("Fra dato:");
		datoPane.add(lblSlutDato, 0, 1);
		datoPane.add(slutDato, 1, 1);
		slutDato.setMaxWidth(130);
		slutDato.setEditable(false);
		slutDato.setOnAction(event -> datoAction());

		this.add(lblAlleOrdrer, 0, 1);
		lblAlleOrdrer.setFont(Font.font(20));

		this.add(lvwOrdrer, 0, 2);
		lvwOrdrer.setMaxWidth(230);
		ChangeListener<Ordre> ordreListener = (ov, oldOrdre, newOrdre) -> this.ordreAction();
		lvwOrdrer.getSelectionModel().selectedItemProperty().addListener(ordreListener);

		Label lblValgtOrdre = new Label("Valgt ordre:");
		lblValgtOrdre.setFont(Font.font(20));
		this.add(lblValgtOrdre, 1, 1, 2, 1);

		this.add(tblOrdrelinjer, 1, 2, 4, 1);
		tblOrdrelinjer.setMinWidth(400);
		tblOrdrelinjer.setMaxSize(410, 400);
		TableColumn<Ordrelinje, String> produktCol = new TableColumn<>("Produkt");
		produktCol.prefWidthProperty().bind(tblOrdrelinjer.widthProperty().multiply(0.6));
		produktCol.setResizable(false);
		produktCol.setCellValueFactory(
				cellData -> new ReadOnlyStringWrapper(readProdukt(cellData.getValue())));
		TableColumn<Ordrelinje, Integer> antalCol = new TableColumn<>("Antal");
		antalCol.prefWidthProperty().bind(tblOrdrelinjer.widthProperty().multiply(0.195));
		antalCol.setResizable(false);
		antalCol.setStyle("-fx-alignment: CENTER-RIGHT;");
		antalCol.setCellValueFactory(new PropertyValueFactory<>("antal"));
		TableColumn<Ordrelinje, Double> prisCol = new TableColumn<>("Pris i kr.");
		prisCol.prefWidthProperty().bind(tblOrdrelinjer.widthProperty().multiply(0.2));
		prisCol.setResizable(false);
		prisCol.setStyle("-fx-alignment: CENTER-RIGHT;");
		prisCol.setCellValueFactory(new PropertyValueFactory<>("pris"));
		tblOrdrelinjer.getColumns().setAll(produktCol, antalCol, prisCol);

		for (TextField txf : ordreTextFields) {
			txf.setMaxWidth(txfWidth);
			txf.setAlignment(Pos.CENTER_RIGHT);
			txf.setEditable(false);
		}

		Label lblDankort = new Label("Dankort:");
		this.add(lblDankort, 1, 3);
		this.add(txfDankort, 1, 4);

		Label lblKontant = new Label("Kontant:");
		this.add(lblKontant, 2, 3);
		this.add(txfKontant, 2, 4);

		Label lblMobilePay = new Label("MobilePay:");
		this.add(lblMobilePay, 3, 3);
		this.add(txfMobilePay, 3, 4);

		Label lblKlip = new Label("Klip:");
		this.add(lblKlip, 4, 3);
		this.add(txfKlip, 4, 4);

		VBox totalFørRabatBox = new VBox(10);
		totalFørRabatBox.setAlignment(Pos.CENTER);
		this.add(totalFørRabatBox, 1, 5, 2, 1);
		Label lblTotalFørRabat = new Label("Total før rabat:");
		totalFørRabatBox.getChildren().setAll(lblTotalFørRabat, txfTotalFørRabat);

		VBox totalEfterRabatBox = new VBox(10);
		totalEfterRabatBox.setAlignment(Pos.CENTER);
		this.add(totalEfterRabatBox, 3, 5, 2, 1);
		Label lblTotalEfterRabat = new Label("Total efter rabat:");
		totalEfterRabatBox.getChildren().addAll(lblTotalEfterRabat, txfTotalEfterRabat);

		StackPane stackPane = new StackPane();
		this.add(stackPane, 5, 0, 1, 7);
		stackPane.getChildren().addAll(periodensSalgPane, dagensSalgPane);

		// -------------------------------------------------------------------------

		// DAGENS SALG
		dagensSalgPane.setBackground(new Background(new BackgroundFill(Color.WHITESMOKE, null, null)));
		dagensSalgPane.setGridLinesVisible(false);
		dagensSalgPane.setAlignment(Pos.TOP_LEFT);
		dagensSalgPane.setHgap(20);
		dagensSalgPane.setVgap(10);

		Label lblTotalSalgForDagen = new Label("Total salg for dagen:");
		lblTotalSalgForDagen.setFont(Font.font(20));
		dagensSalgPane.add(lblTotalSalgForDagen, 0, 0);

		for (TextField txf : dagensTextFields) {
			txf.setMaxWidth(txfWidthLarge);
			txf.setAlignment(Pos.CENTER_RIGHT);
			txf.setEditable(false);
		}

		Label lblDagenTSFR = new Label("Total salg før rabat:");
		dagensSalgPane.add(lblDagenTSFR, 0, 1);
		dagensSalgPane.add(txfDagenTSFR, 1, 1);

		Label lblDagenTSEF = new Label("Total salg efter rabat:");
		dagensSalgPane.add(lblDagenTSEF, 0, 2);
		dagensSalgPane.add(txfDagenTSEF, 1, 2);

		Label lblDagenGnsRabat = new Label("Gennemsnitlig rabat givet pr. salg:");
		dagensSalgPane.add(lblDagenGnsRabat, 0, 3);
		dagensSalgPane.add(txfDagenGnsRabat, 1, 3);

		Label lblDagenGnsPris = new Label("Gennemsnitlig pris pr. salg:");
		dagensSalgPane.add(lblDagenGnsPris, 0, 4);
		dagensSalgPane.add(txfDagenGnsPris, 1, 4);

		Label lblDagenTotalDankort = new Label("Total betaling med Dankort:");
		dagensSalgPane.add(lblDagenTotalDankort, 0, 6);
		dagensSalgPane.add(txfDagenTotalDankort, 1, 6);

		Label lblDagenTotalKontant = new Label("Total betaling med kontanter:");
		dagensSalgPane.add(lblDagenTotalKontant, 0, 7);
		dagensSalgPane.add(txfDagenTotalKontant, 1, 7);

		Label lblDagenTotalMobilePay = new Label("Total betaling med MobilePay:");
		dagensSalgPane.add(lblDagenTotalMobilePay, 0, 8);
		dagensSalgPane.add(txfDagenTotalMobilePay, 1, 8);

		Label lblDagenTotalKlip = new Label("Total betaling med klip:");
		dagensSalgPane.add(lblDagenTotalKlip, 0, 9);
		dagensSalgPane.add(txfDagenTotalKlip, 1, 9);

		// -------------------------------------------------------------------------

		// PERIODENS SALG
		periodensSalgPane.setBackground(new Background(new BackgroundFill(Color.WHITESMOKE, null, null)));
		periodensSalgPane.setGridLinesVisible(false);
		periodensSalgPane.setAlignment(Pos.TOP_LEFT);
		periodensSalgPane.setHgap(20);
		periodensSalgPane.setVgap(10);

		HBox prislisteBox = new HBox(10);
		periodensSalgPane.add(prislisteBox, 0, 0, 4, 1);

		Label lblSalgssituation = new Label("Salgssituation:");
		lblSalgssituation.setFont(Font.font(20));

		prislisteBox.getChildren().addAll(lblSalgssituation, cbbPrislister);
		cbbPrislister.getItems().addAll(Controller.getPrislister());
		cbbPrislister.setOnMouseClicked(event -> loadPrislisterAction());
		cbbPrislister.setOnAction(event -> listeprisAction());

		Label lblAntalØl = new Label(
				"Antal flasker og fadøl solgt af alle typer øl\ni perioden fra den valgte salgssituation:");
		periodensSalgPane.add(lblAntalØl, 0, 1, 4, 1);

		periodensSalgPane.add(txaListepriser, 0, 2, 4, 1);
		txaListepriser.setMaxWidth(400);
		txaListepriser.setMinHeight(400);
		txaListepriser.setEditable(false);

		Label lblTotalSalgForPerioden = new Label("Total salg for perioden:");
		lblTotalSalgForPerioden.setFont(Font.font(20));
		periodensSalgPane.add(lblTotalSalgForPerioden, 0, 3, 4, 1);

		for (TextField txf : periodensTextFields) {
			txf.setMaxWidth(txfWidthLarge);
			txf.setAlignment(Pos.CENTER_RIGHT);
			txf.setEditable(false);
		}

		Label lblKlipKøbt = new Label("Antal klip købt i perioden:");
		periodensSalgPane.add(lblKlipKøbt, 0, 4);
		periodensSalgPane.add(txfKlipKøbt, 1, 4);
		txfKlipKøbt.setMaxWidth(70);
		txfKlipKøbt.setAlignment(Pos.CENTER_RIGHT);
		txfKlipKøbt.setEditable(false);

		Label lblKlipBrugt = new Label("Antal klip brugt i perioden:");
		periodensSalgPane.add(lblKlipBrugt, 0, 5);
		periodensSalgPane.add(txfKlipBrugt, 1, 5);
		txfKlipBrugt.setMaxWidth(70);
		txfKlipBrugt.setAlignment(Pos.CENTER_RIGHT);
		txfKlipBrugt.setEditable(false);

		Label lblPeriodenTSFR = new Label("Total salg før rabat:");
		periodensSalgPane.add(lblPeriodenTSFR, 2, 4);
		periodensSalgPane.add(txfPeriodenTSFR, 3, 4);

		Label lblPeriodenTSEF = new Label("Total salg efter rabat:");
		periodensSalgPane.add(lblPeriodenTSEF, 2, 5);
		periodensSalgPane.add(txfPeriodenTSEF, 3, 5);

		Label lblPeriodenGnsRabat = new Label("Gennemsnitlig rabat givet pr. salg:");
		periodensSalgPane.add(lblPeriodenGnsRabat, 2, 6);
		periodensSalgPane.add(txfPeriodenGnsRabat, 3, 6);

		Label lblPeriodenGnsPris = new Label("Gennemsnitlig pris pr. salg:");
		periodensSalgPane.add(lblPeriodenGnsPris, 2, 7);
		periodensSalgPane.add(txfPeriodenGnsPris, 3, 7);

		Label lblPeriodenTotalDankort = new Label("Total betaling med Dankort:");
		periodensSalgPane.add(lblPeriodenTotalDankort, 2, 9);
		periodensSalgPane.add(txfPeriodenTotalDankort, 3, 9);

		Label lblPeriodenTotalKontant = new Label("Total betaling med kontanter:");
		periodensSalgPane.add(lblPeriodenTotalKontant, 2, 10);
		periodensSalgPane.add(txfPeriodenTotalKontant, 3, 10);

		Label lblPeriodenTotalMobilePay = new Label("Total betaling med MobilePay:");
		periodensSalgPane.add(lblPeriodenTotalMobilePay, 2, 11);
		periodensSalgPane.add(txfPeriodenTotalMobilePay, 3, 11);

		// -------------------------------------------------------------------------

		start();
	}

	// -------------------------------------------------------------------------

	/**
	 * Viser statistikker for dagens salg og listen over dagens ordrer, hvis brugen
	 * er DagensSalg. Ellers vises blot et vindue med tomme felter og muligheden for
	 * at vælge datoen fra og datoen til.
	 */
	private void start() {
		if (brug.equals("DagensSalg")) {
			lblAlleOrdrer.setText("Dagens ordrer:");
			loadOrdrer();
			lvwOrdrer.getItems().clear();
			lvwOrdrer.getItems().addAll(periodeOrdrer);
			dagensSalg();
			dagensSalgPane.toFront();
		} else if (brug.equals("PeriodensSalg")) {
			this.add(datoPane, 0, 0, 2, 1);
			lblAlleOrdrer.setText("Alle periodens ordrer:");
			periodensSalgPane.toFront();
		}
	}

	/**
	 * Bruges til at loade alle dagens ordrer, når en tab med dagens salg vælges.
	 */
	public void loadOrdrer() {
		if (brug.equals("DagensSalg")) {
			periodeOrdrer = Controller.statistikOrdrer();
			lvwOrdrer.getItems().clear();
			lvwOrdrer.getItems().addAll(periodeOrdrer);
			dagensSalg();
		}
	}

	/**
	 * Viser information om den valgte ordre. Bl.a. ordrelinjerne og total med og
	 * uden rabat.
	 */
	private void ordreAction() {
		Ordre ordre = lvwOrdrer.getSelectionModel().getSelectedItem();
		if (ordre != null) {
			tblOrdrelinjer.getItems().setAll(ordre.getOrdrelinjer());
			double dankort = 0;
			double kontant = 0;
			double mobilePay = 0;
			double klip = 0;
			for (Entry<Betalingsmetode, Double> entry : ordre.getBetalingsmetoder().entrySet()) {
				if (entry.getKey().getType().equals("Kort")) {
					dankort += entry.getValue();
				} else if (entry.getKey().getType().equals("Kontant")) {
					kontant += entry.getValue();
				} else if (entry.getKey().getType().equals("MobilePay")) {
					mobilePay += entry.getValue();
				} else if (entry.getKey().getType().equals("Klippekort")) {
					klip += entry.getValue();
				}
			}
			txfDankort.setText(String.format("%.2f", dankort));
			txfKontant.setText(String.format("%.2f", kontant));
			txfMobilePay.setText(String.format("%.2f", mobilePay));
			txfKlip.setText(String.format("%.2f", klip));
			txfTotalFørRabat.setText(String.format("%.2f", ordre.getTotalUdenRabat()));
			txfTotalEfterRabat.setText(String.format("%.2f", ordre.getTotalMedRabat()));
		}
	}

	/**
	 * Bruges til at printe et produkt korrekt i tableview.
	 */
	private String readProdukt(Ordrelinje ol) {
		if (ol instanceof OrdrelinjeSampak) {
			OrdrelinjeSampak olSampak = (OrdrelinjeSampak) ol;
			String string = ol.getListepris().getProdukt().toString();
			for (Bryg bryg : olSampak.getBrygArray()) {
				string = string + String.format("\n%5s %s", "-", bryg);
			}
			for (Glas glas : olSampak.getGlasArray()) {
				string = string + String.format("\n%5s %s", "-", glas);
			}
			return string;
		} else {
			return ol.getListepris().getProdukt().toString();
		}
	}

	/**
	 * Viser dagens salg, statistikker og ordrelister.
	 */
	private void dagensSalg() {
		txfDagenTSFR.setText(String.format("%.2f", Controller.statistikTotalUdenRabat(periodeOrdrer)));
		txfDagenTSEF.setText(String.format("%.2f", Controller.statistikTotalMedRabat(periodeOrdrer)));
		txfDagenGnsRabat.setText(String.format("%.2f", Controller.statistikGnsRabat(periodeOrdrer)));
		txfDagenGnsPris.setText(String.format("%.2f", Controller.statistikGnsPris(periodeOrdrer)));
		txfDagenTotalDankort.setText(String.format("%.2f", Controller.statistikDankort(periodeOrdrer)));
		txfDagenTotalKontant.setText(String.format("%.2f", Controller.statistikKontant(periodeOrdrer)));
		txfDagenTotalMobilePay.setText(String.format("%.2f", Controller.statistikMobilePay(periodeOrdrer)));
		txfDagenTotalKlip.setText(String.format("%.0f", Controller.statistikKlipBrugt(periodeOrdrer)));
	}

	/**
	 * Viser periodens salg, statistikker og ordrelister.
	 */
	private void periodensSalg() {
		txfKlipKøbt.setText(String.format("%d", Controller.statistikKlipKøbt(periodeOrdrer)));
		txfKlipBrugt.setText(String.format("%.0f", Controller.statistikKlipBrugt(periodeOrdrer)));
		txfPeriodenTSFR.setText(String.format("%.2f", Controller.statistikTotalUdenRabat(periodeOrdrer)));
		txfPeriodenTSEF.setText(String.format("%.2f", Controller.statistikTotalMedRabat(periodeOrdrer)));
		txfPeriodenGnsRabat.setText(String.format("%.2f", Controller.statistikGnsRabat(periodeOrdrer)));
		txfPeriodenGnsPris.setText(String.format("%.2f", Controller.statistikGnsPris(periodeOrdrer)));
		txfPeriodenTotalDankort.setText(String.format("%.2f", Controller.statistikDankort(periodeOrdrer)));
		txfPeriodenTotalKontant.setText(String.format("%.2f", Controller.statistikKontant(periodeOrdrer)));
		txfPeriodenTotalMobilePay.setText(String.format("%.2f", Controller.statistikMobilePay(periodeOrdrer)));
	}

	/**
	 * Henter prislisterne.
	 */
	private void loadPrislisterAction() {
		cbbPrislister.getItems().clear();
		cbbPrislister.getItems().addAll(Controller.getPrislister());
	}

	/**
	 * Tilføjer periodens ordrer til listen periodeOrdrer.
	 */
	private void datoAction() {
		LocalDate datoStart = startDato.getValue();
		LocalDate datoSlut = slutDato.getValue();
		if (datoStart != null && datoSlut != null) {
			if (!datoSlut.isBefore(datoStart)) {
				periodeOrdrer = Controller.statistikOrdrer(datoStart, datoSlut);
				lvwOrdrer.getItems().clear();
				lvwOrdrer.getItems().addAll(periodeOrdrer);
				listeprisAction();
				periodensSalg();
			} else {
				slutDato.setValue(null);
			}
		}
	}

	/**
	 * Optæller de forskellige typer af flasker og fadøl, der er solgt i perioden.
	 */
	private void listeprisAction() {
		Prisliste prisliste = cbbPrislister.getSelectionModel().getSelectedItem();
		if (prisliste != null) {
			txaListepriser.clear();
			for (Listepris listepris : prisliste.getListepriser()) {
				int antal = 0;
				if (listepris.getProdukt().getProduktgruppe().equals(Produktgruppe.FADØL)
						|| listepris.getProdukt().getProduktgruppe().equals(Produktgruppe.FLASKE)) {
					for (Ordre ordre : periodeOrdrer) {
						for (Ordrelinje ol : ordre.getOrdrelinjer()) {
							if (ol.getListepris().equals(listepris)) {
								antal += ol.getAntal();
							} else if (ol instanceof OrdrelinjeSampak) {
								OrdrelinjeSampak olSampak = (OrdrelinjeSampak) ol;
								for (Produkt produkt : olSampak.getBrygArray()) {
									if (produkt.equals(listepris.getProdukt())) {
										antal++;
									}
								}
							}
						}
					}
				}
				if (antal > 0) {
					if (!txaListepriser.getText().equals("")) {
						txaListepriser.appendText("\n");
					}
					txaListepriser.appendText(String.format("%5d %s", antal, listepris.getProdukt()));
				}
			}
			periodensSalg();
		}
	}
}